
from flask import abort, flash, redirect, render_template, url_for, request, send_from_directory, current_app, make_response
from . import croydon


@croydon.route('/hire-uk')
def multiple_keywords_1():
    return render_template('landing/london.html', data='hire uk')
@croydon.route('/jobs-surrey')
def multiple_keywords_2():
    return render_template('landing/london.html', data='jobs surrey')
@croydon.route('/nannies')
def multiple_keywords_3():
    return render_template('landing/london.html', data='nannies')
@croydon.route('/care-nanny')
def multiple_keywords_4():
    return render_template('landing/london.html', data='care nanny')
@croydon.route('/nanny-service')
def multiple_keywords_5():
    return render_template('landing/london.html', data='jobs surrey')
