"""The Flask app, with initialization and view functions."""

import logging
import base64
import datetime
from functools import wraps
from unicodedata import normalize
from sqlalchemy import func

from datetime import datetime, timedelta
from jinja2 import Environment, FileSystemLoader
from flask import send_from_directory, current_app, make_response
from flask_sitemap import Sitemap

from flask import send_from_directory
from flask import abort, jsonify, redirect, render_template, request, url_for, flash, session, make_response
from flask.ext.login import LoginManager, current_user, login_user
from flask.ext.login import login_user, login_required, logout_user
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.security import Security, SQLAlchemyUserDatastore
from flask.ext.security.signals import user_registered
from flask.ext.security.utils import url_for_security
from flask.ext.admin import Admin, BaseView, expose, AdminIndexView
from flask.ext.admin.contrib.sqla import ModelView
#from flask.ext.misaka import Misaka
from flask_oauthlib.client import OAuth , OAuthException
from flask_mail import Mail, Message
from werkzeug import secure_filename
from wtforms.ext.appengine import db
from config import DefaultConfig
import filters
from forms import ResumeForm, CourseForm, ExtendedRegisterForm, RegisterCoachForm, ContactForm
from models import User, Resume, Course, Role, Oauth, CoachUserData, ResumeView
from common import app, db, security
#from teacheraApp.pdfs import create_pdf
from utils.base62 import dehydrate, saturate

# ***************** PAYSTACK ***************** 
from paystack import Paystack, create_transaction_ref, send_course_signup_mail

import stripe

def slug(text, encoding=None,permitted_chars='abcdefghijklmnopqrstuvwxyz0123456789-'):
    if isinstance(text, str):
        text = text.decode(encoding or 'ascii')
    clean_text = text.strip().replace(' ', '-').lower()
    while '--' in clean_text:
        clean_text = clean_text.replace('--', '-')
    ascii_text = normalize('NFKD', clean_text).encode('ascii', 'ignore')
    strict_text = map(lambda x: x if x in permitted_chars else '', ascii_text)
    return ''.join(strict_text)

app.config.from_object(DefaultConfig)
stripe.api_key = app.config.get('STRIPE_SECRET_KEY')


user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security.init_app(app, user_datastore, register_form=ExtendedRegisterForm)

def admin_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if current_user.has_role('ROLE_ADMIN') is False:
            abort(404)
        return f(*args, **kwargs)
    return decorated_function

def coach_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if current_user.has_role('ROLE_COACH') is False:
            abort(404)
        return f(*args, **kwargs)
    return decorated_function

def student_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if current_user.has_role('ROLE_STUDENT') is False:
            abort(404)
        return f(*args, **kwargs)
    return decorated_function

class MyAdminIndexView(AdminIndexView):
    @login_required
    @admin_required
    @expose('/')
    def index(self):
        return self.render('admin/index.html')

class AdminView(ModelView):
    column_searchable_list = (User.name, User.email)
    column_exclude_list = ('created', 'modified','password', 'active', 'confirmed_at','coach')
    def is_accessible(self):
        return current_user.has_role('ROLE_ADMIN')

# Flask-Admin
admin = Admin(app, name='Teachera', index_view=MyAdminIndexView())

admin.add_view(AdminView(User, db.session))
admin.add_view(AdminView(Resume, db.session))
admin.add_view(AdminView(Course, db.session))
admin.add_view(AdminView(CoachUserData, db.session))


@user_registered.connect_via(app)
def user_registered_sighandler(app, user, confirm_token):
    default_role = user_datastore.find_role("ROLE_STUDENT")
    user_datastore.add_role_to_user(user, default_role)
    db.session.commit()


@app.before_first_request
def create_user():
    db.create_all()
    if not User.query.first():
        user_datastore.create_user(username='admin', email='admin@example.com',
                             password='admin', roles=['admin'])
        db.session.commit()
# Load custom Jinja filters from the `filters` module.
filters.init_app(app)

def date_from_string(date):
    if date:
      return date if len(date)>0 else '-'
    else:
      return '-'

def base64_encode(value):
    return base64.b64encode(str(value))

app.jinja_env.filters['datefromstring'] = date_from_string
app.jinja_env.filters['b64'] = base64_encode
app.jinja_env.filters['b62'] = dehydrate
app.jinja_env.filters['slug'] = slug

#Misaka(app)
mail = Mail(app)
from flask_moment import Moment
moment = Moment(app)

# Setup logging for production.
if not app.debug:
    app.logger.setHandler(logging.StreamHandler()) # Log to stderr.
    app.logger.setLevel(logging.INFO)


@app.errorhandler(404)
def error_not_found(error):
    """Render a custom template when responding with 404 Not Found."""
    return render_template('error/not_found.html'), 404


########################OAUTH#################################################
oauth = OAuth(app)

facebook = oauth.remote_app(
    'facebook',
    consumer_key=app.config['FACEBOOK_LOGIN_APP_ID'],
    consumer_secret=app.config['FACEBOOK_LOGIN_APP_SECRET'],
    request_token_params={'scope': 'email'},
    base_url='https://graph.facebook.com',
    request_token_url=None,
    access_token_url='/oauth/access_token',
    authorize_url='https://www.facebook.com/dialog/oauth',
    access_token_method='POST',
)


@app.route('/login/fb')
def login_fb():
    callback = url_for(
        'facebook_authorized',
        next=request.args.get('next') or request.referrer or None,
        _external=True
    )
    return facebook.authorize(callback=callback)



@app.route('/login/fb/authorized')
@facebook.authorized_handler
def facebook_authorized(resp):
    if resp is None: # Authentication failure...
        flash("There was a problem with log in using Facebook: {0}".format(request.args['error_description']), 'danger')
        return render_template('landing.html')

    # Facebook session token
    session['oauth_token'] = (resp['access_token'], '')
    # Load facebook profile
    profile = facebook.get('/me?fields=id,name,first_name,last_name,link,email')
    # Try to find user and his Oauth record in db
    user = db.session.query(User).filter(User.email==profile.data['email']).first()
    oauth = db.session.query(Oauth).filter(Oauth.provider_id==profile.data['id']).\
        filter(Oauth.provider=='facebook').first()

    # User not exist? So we need to 'register' him on the site
    if user is None:
        user = User()
        user.email = profile.data['email']
        user.name = profile.data['first_name'] + u" " + profile.data['last_name']
        user.password = unicode(u"fb-id|"+profile.data['id'])   # User from OAuth have no password (we save id)
        user.active = True
        user.confirmed_at = datetime.datetime.now()
        db.session.add(user)
        db.session.commit()
        default_role = user_datastore.find_role("ROLE_STUDENT")
        user_datastore.add_role_to_user(user, default_role)
        db.session.commit()

    # Save some data from OAuth service that might be useful sometime later
    # This is also used when user was registered by e-mail and now he
    # logs in using social account for the same e-mail
    if oauth is None:
        oauth = Oauth()
        oauth.provider='facebook'
        oauth.provider_id=profile.data['id']
        oauth.email=profile.data['email']
        oauth.profile=profile.data['link']
        oauth.user=user     # Contect with user
        # There are few fields empty...
        db.session.add(oauth)
        db.session.commit()

    # Try to login new user
    lok = login_user(user)
    if lok:
        # Show green mesaage that all went fine
        flash("You have been successfully signed in using Facebook.", 'success')
        return redirect(url_for('resumes_list'))
    else:
        flash("There was a problem with your logining-in", 'warning')
        return render_template('landing.html')



# Here goes special functions need by Flask-OAuthlib
@facebook.tokengetter
def get_facebook_oauth_token():
    return session.get('oauth_token')


########################OAUTH#################################################

#######View for site map############
@app.route('/sitemap_old.xml')
def static_from_root():
    return send_from_directory(app.static_folder, request.path[1:])

@app.route('/BingSiteAuth.xml')
def static_from_root_bing():
    return send_from_directory(app.static_folder, request.path[1:])

@app.route('/sitemap')
def sitemap_html():
    return render_template('sitemap.html')

@app.route('/sitemap/business/analyst/london/list')
def sitemap_business_analyst_london():
    return render_template('sitemap_london_business_analyst.html')

@app.route('/sitemap/digital/marketing/london/list')
def sitemap_digital_marketing_london():
    return render_template('sitemap_london_digital_marketing.html')


@app.route('/sitemap/links/list')
def seo_links_citation_london():
    return render_template('seolinks.html')

@app.route('/sitemap.xml')
def xmlSitemap():
      pages=[]
      ten_days_ago=(datetime.now() - timedelta(days=10)).date().isoformat()
      # static pages
      for rule in current_app.url_map.iter_rules():
          if "GET" in rule.methods and len(rule.arguments)==0:
              pages.append(
                           [rule.rule,ten_days_ago]
                           )
      sitemap_xml = render_template('sitemap_template.xml', pages=pages)
      response= make_response(sitemap_xml)
      response.headers["Content-Type"] = "application/xml"    
    
      return response

#########Views for Resume or Profiles#######


@app.route('/dashboard/')
def resumes_list():
    """Provide HTML page listing all resumes in the database."""
    # Query: Get all Resume (Profile) objects, sorted by the resume date.
    #appts = list()
    appts = db.session.query(Resume).filter_by(user_id=current_user.id)
             #.order_by(Resume.created.asc()).first())

    #for resume in resumes:
        ##views_count_resume = db.session.query(ResumeView.id).filter(ResumeView.resume == resume).count()
        ##appts.append((resume, views_count_resume))

    courses = db.session.query(Course).filter(
                Course.users.contains(current_user)).all()

    return render_template('resume/dashboard.html', appts=appts, courses=courses)

@app.route('/attendee/profile/view/<resume_id>/')
@login_required
#@coach_required
def resume_preview(resume_id):
    """Provide HTML page with all details on a given resume.
       The url is base64 encoded so no one will try to check other resumes.
    """
    resume_id = base64.b64decode(resume_id)
    appt = db.session.query(Resume).get(resume_id)
    if appt is None:
        # Abort with Not Found.
        abort(404)
    # Count the view to user resume views
    resume_view = ResumeView(current_user, appt)
    db.session.add(resume_view)
    db.session.commit()
    # Template without edit buttons
    return render_template('resume/resume_detail_preview.html', appt=appt)


@app.route('/profiles/<int:resume_id>/')
@login_required
def resume_detail(resume_id):
    """Provide HTML page with all details on a given resume."""
    # Query: get Resume object by ID.
    appt = db.session.query(Resume).get(resume_id)
    if appt is None or appt.user_id != current_user.id:
        # Abort with Not Found.
        abort(404)
    return render_template('resume/resume_detail.html', appt=appt)


@app.route('/profiles/create/', methods=['GET', 'POST'])
#@student_required
@login_required
def resume_create():
    """Provide HTML form to create a new resume record."""
    form = ResumeForm(request.form)
    if request.method == 'POST' and form.validate():
        appt = Resume(user_id=current_user.id)
        form.populate_obj(appt)
        db.session.add(appt)
        db.session.commit()
        # Success. Send the user back to the full resumes list.
        return redirect(url_for('resumes_list'))
    # Either first load or validation error at this point.
    return render_template('resume/edit.html', form=form)



@app.route('/profiles/<int:resume_id>/edit/', methods=['GET', 'POST'])
@login_required
def resume_edit(resume_id):
    """Provide HTML form to edit a given appointment."""
    appt = db.session.query(Resume).get(resume_id)
    if appt is None:
        abort(404)
    if appt.user_id != current_user.id:
        abort(403)
    form = ResumeForm(request.form, appt)
    if request.method == 'POST' and form.validate():
        form.populate_obj(appt)
        db.session.commit()
        # Success. Send the user back to the detail view of that resume.
        return redirect(url_for('resume_detail', resume_id=appt.id))
    return render_template('resume/edit.html', form=form)


@app.route('/profiles/<int:resume_id>/delete/', methods=['GET', 'POST'])
@login_required
def resume_delete(resume_id):
    appt = db.session.query(Resume).get(resume_id)
    if appt is None:
        abort(404)
    if appt.user_id != current_user.id:
        abort(403)

    resume_views = db.session.query(ResumeView).filter(ResumeView.resume == appt)
    for record in resume_views:
        db.session.delete(record)
    db.session.commit()

    db.session.delete(appt)
    db.session.commit()
    return redirect(url_for('resumes_list'))


#########Views for Courses#######

@app.route('/private/group/courses/<course_title>/<int:course_id>/apply')
@login_required
def course_apply(course_id, course_title):
    """
    Applying for courses by applicants.

    THIS VIEW IS FOR APPLICANTS
    :param course_id: id of course to apply/join
    :return: nothing
    """
    course = db.session.query(Course).get(course_id)
    if course is None:
        abort(404)
    elif current_user.id is None:
        abort(403)
    else:
        if current_user in course.users:
            flash("You have <strong>already applied</strong> for {0}.".format(course.course_title), 'warning')
            return redirect(url_for('course_list'))
        else:
            rent_per_hour_count = course.rent_per_hour 
	    total_cost_teacher = course.cost_per_hour * course.hours_per_month
	    total_cost_rent = course.rent_per_hour * course.hours_per_month
            first_cost = total_cost_teacher + total_cost_rent
            all_cost = first_cost/course.min_students
            amount = 0
            amount1 = all_cost * 1.2
            amount2 = all_cost * 1.4
            amount3 = all_cost * 1.1 
            if course.min_students == len(course.users):
                amount = amount1
            elif course.min_students > len(course.users):
                amount = amount2
            elif course.min_students < len(course.users):
                amount = amount3
            return render_template('course/payment.html', course=course, amount=amount,
                                   key=app.config.get('STRIPE_PUBLISHABLE_KEY'))



@app.route('/courses/<course_title>/<int:course_id>/charge', methods=['POST'])
@login_required
def st_charge(course_id, course_title):
    course = db.session.query(Course).get(course_id)
    rent_per_hour_count = course.rent_per_hour / course.min_students
    total_cost_teacher = course.cost_per_hour * course.hours_per_month
    total_cost_rent = course.rent_per_hour * course.hours_per_month
    first_cost = total_cost_teacher + total_cost_rent
    all_cost = first_cost/course.min_students
    amount = 0
    #amount1 = course.cost_per_hour * 0.5 + course.cost_per_hour * course.hours_per_month / course.min_students + rent_per_hour_count
    #amount2 = course.cost_per_hour * 0.4 + course.cost_per_hour * course.hours_per_month / course.min_students + rent_per_hour_count
    #amount3 = course.cost_per_hour * 0.6 + course.cost_per_hour * course.hours_per_month  / course.min_students + rent_per_hour_count
    amount1 = all_cost * 1.2
    amount2 = all_cost * 1.4
    amount3 = all_cost * 1.1 
    if course.min_students == len(course.users):
        amount = amount1
    elif course.min_students > len(course.users):
        amount = amount2
    elif course.min_students < len(course.users):
        amount = amount3
    amount = amount * 100  # calculate the amount

    customer = stripe.Customer.create(
         email=current_user.email,
         card=request.form['stripeToken']
     )
    st_charge = stripe.Charge.create (
        customer=customer.id,
        amount=int(amount),
        currency='GBP',
        description='GBP {amount} Payment for {course_name}'.format(amount=amount, course_name=course.course_title)
    )
    if st_charge.to_dict()['status'] == "succeeded":
        course.users.append(current_user)
        db.session.add(course)
        db.session.commit()
        flash("You have <strong>successfully applied</strong> for {0}.".format(course.course_title), 'success')
        send_course_signup_mail(current_user)  
    else:
        flash("Some error occured. Please try again", 'warning')
    return redirect(url_for('course_list'))


    #try:
	#message = Message(subject="Class signup successfully!",
                             #sender='postmaster@teachera.co.uk',
                             #reply_to='support@teachera.co.uk',
                            #recipients=[current_user.email])
    	#body = "Hello:\t{0}\nYou have signed up to a class successfully."\
                         #"\nCongratulations on successfully applying to join the class. Please check with the course organizer or the teacher for more details\n"\
                         #"\n\n"\
                         #"Regards,\n"\
                         #"Teachera.org team"
    	#message.body= body.format(current_user.name)
    	#mail.send(message)
    	#except:
        	#pass
    #return redirect(url_for('course_list'))



@app.route('/private/group/courses/<course_title>/<int:course_id>/charge', methods=['POST'])
@login_required
def charge(course_id, course_title):
    course = db.session.query(Course).get(course_id)
    rent_per_hour_count = course.rent_per_hour / course.min_students
    total_cost_teacher = course.cost_per_hour * course.hours_per_month
    total_cost_rent = course.rent_per_hour * course.hours_per_month
    first_cost = total_cost_teacher + total_cost_rent
    all_cost = first_cost/course.min_students
    amount = 0
    #amount1 = course.cost_per_hour * 0.5 + course.cost_per_hour * course.hours_per_month / course.min_students + rent_per_hour_count
    #amount2 = course.cost_per_hour * 0.4 + course.cost_per_hour * course.hours_per_month / course.min_students + rent_per_hour_count
    #amount3 = course.cost_per_hour * 0.6 + course.cost_per_hour * course.hours_per_month  / course.min_students + rent_per_hour_count
    amount1 = all_cost * 1.2
    amount2 = all_cost * 1.4
    amount3 = all_cost * 1.1 
    if course.min_students == len(course.users):
        amount = amount1
    elif course.min_students > len(course.users):
        amount = amount2
    elif course.min_students < len(course.users):
        amount = amount3
    amount = amount * 100  # calculate the amount

    # =================  UNCOMMENT THIS LINE TO ENABLE PAYSTACK :--: IMPLEMENT CONDITION FOR REDIRECTION ============
    # Set conditional payment channel based on user location
    # if user.location == "Nigeria": # OR any other suitable condition you decide to use
    # If condition is true, return payWithPaystack()

    ############## UNCOMMENT THE LINE BELOW ##################################
    return payWithPaystack(amount, current_user.email, course_id)

    customer = stripe.Customer.create(
         email=current_user.email,
         card=request.form['stripeToken']
     )
    charge = stripe.Charge.create (
        customer=customer.id,
        amount=int(amount),
        currency='GBP',
        description='GBP {amount} Payment for {course_name}'.format(amount=amount, course_name=course.course_title)
    )
    if charge.to_dict()['status'] == "succeeded":
        course.users.append(current_user)
        db.session.add(course)
        db.session.commit()
        flash("You have <strong>successfully applied</strong> for {0}.".format(course.course_title), 'success')
        send_course_signup_mail(current_user)  # Edited by James
    else:
        flash("Some error occured. Please try again", 'warning')
    return redirect(url_for('course_list'))
    # try:
    #     message = Message(subject="Class signup successfully!",
    #                         sender='support@teachera.eu',
    #                         reply_to='support@teachera.eu',
    #                        recipients=[current_user.email])
    #     body = "Hello:\t{0}\nYou have signed up to a class successfully."\
    #                     "\nYou need to make an paymemt for this course. Contact the organizers or the teacher for more details\n"\
    #                     "\n\n"\
    #                     "Regards,\n"\
    #                     "Teachera.org team"  
    #     message.body= body.format(current_user.name)      
    #     mail.send(message)
    # except:
    #     pass
    # return redirect(url_for('course_list'))




# **************** PAY WITH PAYSTACK ***********************
def payWithPaystack(amount, user_email, course_id):
    """
    Initiate payment processing using abstracted Paystack API class, on successful connection, returns
    Paystack authorization url (which can be loaded using javascript on the user end, or redirected to) 
    to provide user card details input interface
    
    Params:
        amount: amount in naira to be paid 
        user_email: payers email address
    return: none
    """
    response_dict    =       {}
    reference        =       create_transaction_ref("payment", 10)
    callback_url     =       "www.teachera.org" + url_for('verify_pay', trx_ref=reference, course_id=course_id) 
    ps               =       Paystack()
    response_dict    =       ps.initiate_transaction(reference, amount, user_email, callback_url)
    if response_dict['status'] == "success":
        return redirect(response_dict['auth_url'])
    else:
        try:
            if response_dict['error_response']  ==  "Duplicate Transaction Reference":
                new_ref            =     create_transaction_ref("payment", 10)
                callback_url       =     "www.teachera.org" + url_for('verify_pay', trx_ref=new_ref, course_id=course_id)
                print "re-attempting payment with reference number %s" %(new_ref)
                response_dict      =      ps.initiate_transaction(new_ref, amount, user_email, callback_url)
                return redirect(response_dict['auth_url'])
        except Exception as e:
            print "Encountered error: %s" %(e)
    return redirect(response_dict['auth_url'])



# Verify paystack payment
@app.route('/paystack_api/verify_pay/<trx_ref>/<course_id>')
def verify_pay(trx_ref, course_id):
    course = db.session.query(Course).get(course_id)
    ps = Paystack()
    pay_status    =  ps.verify_transaction(trx_ref)
    if pay_status == "success":
        course.users.append(current_user)
        db.session.add(course)
        db.session.commit()
        flash("You have <strong>successfully applied</strong> for {0}.".format(course.course_title), 'success')
        #  SEND MAIL TO USER 
        send_course_signup_mail(current_user)
    else:
        flash("Some error occured. Please try again", 'warning')
    return redirect(url_for('course_list'))


        
@app.route('/courses/<int:course_id>/<course_title>/<city>')
def course_details(course_id , course_title , city):
    """Provide HTML page with all details on a given course.

    THIS VIEW IS FOR APPLICANTS
    """
    # Query: get Course object by ID.
    appt = db.session.query(Course).get(course_id)
    if current_user.is_anonymous:
        resume_exists = False
        anonymous = True
    else:
        resume_exists = bool(db.session.query(Resume).filter(Resume.user_id==current_user.id).count()> 0)
        anonymous = False
    return render_template('course/details.html', appt=appt,
                           have_resume=resume_exists, anonym=anonymous)

@app.route('/join-now/<b62id>/<course_title>')
def course_apply_now(b62id, course_title, city):
    course_id = saturate(b62id)
    return redirect(url_for('course_details', course_id=course_id, city=city, course_title=course_title))

# Coach views # Teachers #Tutors #Instructors

@app.route('/coach/signup/', methods=['GET', 'POST'])
def security_coach_register():
    return redirect(url_for_security('register', next=url_for('coach_register')))




@app.route('/coach/activate/', methods=['GET', 'POST'])
@login_required
def coach_register():
    coach_details = None
    try:
        coach_details = db.session.query(CoachUserData
                        ).filter_by(user_id=current_user.id).all()[0]
    except IndexError:
        pass

    if coach_details:
        return redirect(url_for('course_create'))

    form = RegisterCoachForm(request.form)
    if request.method == 'POST':# and form.validate():
        appt = CoachUserData(user_id=current_user.id)
        #if not appt.last_name:
            #appt.last_name = ""
        form.populate_obj(appt)
        db.session.add(appt)

        coach_role = user_datastore.find_role("ROLE_COACH")
        user_datastore.add_role_to_user(current_user, coach_role)
        db.session.commit()

        # Success. Send to the postion list
        flash("Welcome to the dashboard for tutors or teachers and coaches.", 'success')
        return redirect(url_for('course_create'))
    # Either first load or validation error at this point.
    return render_template('course/edit_coach.html', form=form)



@app.route('/courses/')
@login_required
#@coach_required
def course_list():
    """Provide HTML page listing all courses in the database.

    THIS VIEW IS FOR COMPANIES
    """
    # Query: Get all Course objects, sorted by the position date.
    if current_user and current_user.has_role('ROLE_COACH') == False:
        appts = (db.session.query(Course).
                 order_by(Course.course_start_date.asc()).all())
        return render_template('course/allcourse.html', appts=appts)

    else:
        appts = (db.session.query(Course)
             .filter_by(user_id=current_user.id)
             .order_by(Course.course_start_date.asc()).all())

    return render_template('course/index.html', appts=appts)

@app.route('/courses/create/', methods=['GET', 'POST'])
@login_required
#@coach_required
def course_create():
    """Provide HTML form to create a new courses record.

    THIS VIEW IS FOR COACHES
    """
    try:
        coach_details = db.session.query(CoachUserData
                        ).filter_by(user_id=current_user.id).all()[0]
    except IndexError:
        return redirect(url_for('coach_register'))

    if coach_details is None:
        return redirect(url_for('coach_register'))

    form = CourseForm(request.form)
    if coach_details is not None:
        form.coach_name.data = coach_details.coach_name
        #form.coach_website.data = coach_details.website

    if request.method == 'POST' and form.validate():
        appt = Course(user_id=current_user.id)
        form.populate_obj(appt)
        db.session.add(appt)
        db.session.commit()
        # Success. Send the user back to the full resumes list.
        return redirect(url_for('course_list'))
    # Either first load or validation error at this point.
    return render_template('course/edit.html', form=form)

@app.route('/coach/courses/<int:course_id>/edit/', methods=['GET', 'POST'])
@login_required
#@coach_required
def course_edit(course_id):
    """Provide HTML form to edit a given course.

    THIS VIEW IS FOR COACHES
    """
    appt = db.session.query(Course).get(course_id)
    if appt is None:
        abort(404)
    if appt.user_id != current_user.id and (not current_user.has_role('ROLE_ADMIN')):
        abort(403)
    form = CourseForm(request.form, appt)
    if request.method == 'POST' and form.validate():
        form.populate_obj(appt)
        #del form.created
        db.session.commit()
        # Success. Send the user back to the detail view of that resume.
        return redirect(url_for('course_details', course_id=appt.id, course_title=appt.course_title, city=appt.city))
    return render_template('course/edit.html', form=form)

@app.route('/coach/courses/<int:course_id>/delete/', methods=['GET', 'POST'])
@login_required
@coach_required
def course_delete(course_id):
    """Delete a record

    THIS VIEW IS FOR COACHES
    """
    appt = db.session.query(Course).get(course_id)

    if appt is None:
        # Abort with simple response indicating course not found.
        flash("Wrong Course id.", 'danger')
        return redirect(url_for('course_list'))
    if appt.user_id != current_user.id and (not current_user.has_role('ROLE_ADMIN')):
        # Abort with simple response indicating forbidden.
        flash("You can't remove this course.", 'danger')
        return redirect(url_for('course_list'))
    db.session.delete(appt)
    db.session.commit()
    flash("Course was removed.", 'success')
    return redirect(url_for('course_list'))
    # return jsonify({'status': 'OK'})

@app.route('/coach/courses/<int:course_id>/attendees/')
@login_required
#@coach_required
def course_list_applicants(course_id):

    course = db.session.query(Course).get(course_id)
    if course is None:
        abort(404)
    elif current_user.id is None:
        abort(403)
    #elif course.user_id != current_user.id and (not current_user.has_role('ROLE_ADMIN')):
        #abort(403)
    else:
        applicants_resumes = {}
        applicants = course.users
        for applicant in applicants:
            resumes = db.session.query(Resume).filter(Resume.user_id==applicant.id).all()
            if len(resumes) > 0:
                # encoding each id of resume
                resumes = [base64.b64encode(str(resume.id)) for resume in resumes ]
                applicants_resumes[applicant.id] = resumes
            else:
                applicants_resumes[applicant.id] = None
        return render_template('course/applicants.html', course_id=course_id,
                               applicants=applicants, resumes=applicants_resumes)


@app.route('/coach/courses/<int:course_id>/applicants/send-message/', methods=['GET', 'POST'])
@login_required
@admin_required
def course_applicants_send_email(course_id):
    """
     View for conntacitng all aplicants of postion by e-mail.

    :param course_id: id of postion that applicants will be contacted
    :return: None
    """
    if current_user.id is None:
        abort(403)
    else:
        form = ContactForm(request.form)
        if request.method == 'POST' and form.validate():
            course = db.session.query(Course).get(course_id)
            if course is None:
                abort(404)
            emails = [u.email for u in course.users]
            message = Message(subject=form.subject.data,
                            sender='info@teachera.co.uk',
                           reply_to='info@teachera.co.uk',
                           recipients=['info@teachera.co.uk'],
                           bcc=emails,
                           body=form.text.data)
            mail.send(message)
            flash("Message was send.", 'succes')
            return redirect(url_for('course_list_applicants', course_id=course_id))
        return render_template('course/message_send_form.html', form=form)

###Public Views


def landing_page():
    if current_user.is_authenticated:
        if current_user.coach:
            return redirect(url_for('coach_register'))
        else:
            return redirect(url_for('resumes_list'))
    else:
        return render_template('landing.html')

@app.route('/')
@app.route('/students')
def landing_page_students():

    if current_user.is_authenticated:
        if current_user.coach:
            return redirect(url_for('coach_register'))
        else:
            return redirect(url_for('resumes_list'))
    else:
        appts = (db.session.query(Course).order_by(Course.course_start_date.asc()).all())
        
        return render_template('landing_page_students.html', appts=appts)


@app.route('/coach')
@app.route('/tutors/')
@app.route('/teachers')
@app.route('/teachers/tutors/coach')
def landing_page_teachers():
    if current_user.is_authenticated:
        if current_user.coach:
            return redirect(url_for('coach_register'))
        else:
            return redirect(url_for('resumes_list'))
    else:
        return render_template('landing_teachers.html')

@app.route('/about')
def about_us():
    return render_template('public/about.html')
@app.route('/faq')
def faq():
    return render_template('public/faq.html')

#@app.route('/policy')
def data_policy():
    return render_template('public/policy.html')

@app.route('/contact', methods=['GET', 'POST'])
def contact_form():
    form = ContactForm(request.form)
    if request.method == 'POST' and form.validate():
        #SEND E-MAIL
        message = Message(subject=form.subject.data,
                        sender='support@teachera.co.uk',
                       reply_to=current_user.email,
                       recipients=['support@teachera.co.uk'],
                       body=form.text.data)
        mail.send(message)

        # Success. Send to the postion list
        flash("Your message was send.", 'succes')
        return redirect(url_for('resumes_list'))

    # Either first load or validation error at this point.
    return render_template('public/contact_form.html', form=form)

@app.route('/list/courses/')
def courses_list():
    
    appts = (db.session.query(Course).
             order_by(Course.course_start_date.asc()).all())
    return render_template('course/allcourse.html', appts=appts)




@app.route('/some-endpoint', methods=['POST'])
def share_email():
    share_text = "Your friend {0} on http://teachera.org want to recommend you this open course: {1}.\n"\
                  "Register, and view it here: {2}."\
                  "\n\n"\
                  "Regards,\n"\
                  "Teachera.org team"

    formated_text = share_text.format(current_user.name, request.form['title'], request.form['url'])
    message = Message(subject="Teachera.org - private classes recomendations!",
                       sender='info@teachera.co.uk',
                       reply_to=current_user.email,
                       recipients=[request.form['email']],
                       body=formated_text)
    mail.send(message)




    print request.__dict__
    print request.form
    return jsonify(status='success')



#####Admin created to test a few stuffs ####


@app.route('/admin/course/create/', methods=['GET', 'POST'])
@login_required
def admin_course_create():
    """Provide HTML form to create a new resume record."""
    form = CourseForm(request.form)
    if request.method == 'POST' and form.validate():
        appt = Course(user_id=current_user.id)
        form.populate_obj(appt)
        db.session.add(appt)
        db.session.commit()
        # Success. Send the user back to the full resumes list.
        return redirect(url_for('course_list'))
    # Either first load or validation error at this point.
    return render_template('course/edit.html', form=form)


@app.route('/admin/resumes/create/', methods=['GET', 'POST'])
#@student_required
@login_required
def resumes_create():
    """Provide HTML form to create a new resume record."""
    form = ResumeForm(request.form)
    if request.method == 'POST' and form.validate():
        appt = Resume(user_id=current_user.id)
        form.populate_obj(appt)
        db.session.add(appt)
        db.session.commit()
        # Success. Send the user back to the full resumes list.
        return redirect(url_for('resumes_list'))
    # Either first load or validation error at this point.
    return render_template('resume/admin_edit.html', form=form)



####Admin coach activate try#####


@app.route('/admin/coach/activate/', methods=['GET', 'POST'])
#@student_required
#@login_required
def admin_coach_activate():
    """Provide HTML form to create a new resume record."""
    form = RegisterCoachForm(request.form)
    if request.method == 'POST' and form.validate():
        appt = CoachUserData(user_id=current_user.id)
        form.populate_obj(appt)
        db.session.add(appt)
        coach_role = user_datastore.find_role("ROLE_COACH")
        user_datastore.add_role_to_user(current_user, coach_role)
        db.session.commit()
        # Success. Send the user back to the full resumes list.
        return redirect(url_for('course_create'))
    # Either first load or validation error at this point.
    return render_template('course/edit_coach.html', form=form)

###############SEO URLS ############

@app.route('/learn-to-code-c++-Barcelona')
def tech_keyword_learn_1():
    return render_template('landing.html', title="learn how to code" , keyword="c++" , location="in Barcelona")
@app.route('/learn-to-code-c#-Barcelona')
def tech_keyword_learn_2():
    return render_template('landing.html', title="learn how to code" , keyword="c#" , location="in Barcelona")
@app.route('/learn-to-code-Ruby-Barcelona')
def tech_keyword_learn_3():
    return render_template('landing.html', title="learn how to code" , keyword="Ruby" , location="in Barcelona")
@app.route('/learn-to-code-Python-Barcelona')
def tech_keyword_learn_4():
    return render_template('landing.html', title="learn how to code" , keyword="Python" , location="in Barcelona")
@app.route('/learn-to-code-HTML5-Barcelona')
def tech_keyword_learn_5():
    return render_template('landing.html', title="learn how to code" , keyword="HTML5" , location="in Barcelona")
@app.route('/learn-to-code-Java-Barcelona')
def tech_keyword_learn_6():
    return render_template('landing.html', title="learn how to code" , keyword="Java" , location="in Barcelona")
@app.route('/learn-to-code-Perl-Barcelona')
def tech_keyword_learn_7():
    return render_template('landing.html', title="learn how to code" , keyword="Perl" , location="in Barcelona")
@app.route('/learn-to-code-Php-Barcelona')
def tech_keyword_learn_8():
    return render_template('landing.html', title="learn how to code" , keyword="Php" , location="in Barcelona")
    
@app.route('/learn-to-code-JavaScript-Barcelona')
def tech_keyword_learn_9():
    return render_template('landing.html', title="learn how to code" , keyword="JavaScript" , location="in Barcelona")
    
###########Guitar Keywords ########

@app.route('/learn-to-play-guitar-Barcelona')
def guitar_keyword_learn_1():
    return render_template('landing.html', title="learn to play" , keyword="guitar" , location="in Barcelona")
@app.route('/learn-how-to-play-guitar-Barcelona')
def guitar_keyword_learn_2():
    return render_template('landing.html', title="learn how to play" , keyword="guitar" , location="in Barcelona")
    
@app.route('/learn-to-play-the-guitar-Barcelona')
def guitar_keyword_learn_3():
    return render_template('landing.html', title="learn to play the" , keyword="guitar" , location="in Barcelona")
    
@app.route('/bass-guitar-lessons-for-beginners-barcelona')
def guitar_keyword_learn_4():
    return render_template('landing.html', title="beginner bass lessons " , keyword="guitar" , location="in Barcelona")
    
@app.route('/how-to-play-electric-bass-guitar-barcelona')
def guitar_keyword_learn_5():
    return render_template('landing.html', title="how to play" , keyword="electric bass guitar" , location="in Barcelona")

@app.route('/english-guitar-lessons-barcelona')
def guitar_keyword_learn_6():
    return render_template('landing.html', title="english" , keyword="guitar lessons" , location="in Barcelona")
###############Learn English #####
@app.route('/learn-english-for-business-communication-Barcelona')
def english_keyword_learn_1():
    return render_template('landing.html', title="learn " , keyword="english for business communication" , location="in Barcelona")
@app.route('/teach-english-in-Barcelona')
def english_keyword_learn_2():
    return render_template('landing.html', title="learn " , keyword="english" , location="in Barcelona")
@app.route('/english-teacher-in-Barcelona')
def english_keyword_learn_3():
    return render_template('landing.html', title="english " , keyword="teacher" , location="in Barcelona")
@app.route('/teach-english-in-barcelona-without-tefl')
def english_keyword_learn_4():
    return render_template('landing.html', title="english " , keyword="without tefl" , location="in Barcelona")
@app.route('/learn-new-languages-in-barcelona')
def languages_keyword_learn_2():
    return render_template('landing.html', title="learn " , keyword="new languages" , location="in Barcelona")
    
@app.route('/language-exchange-free-in-Barcelona')
def language_keyword_learn_3():
    return render_template('landing.html', title="language" , keyword="exchange" , location="in Barcelona")
    
@app.route('/global-tutor-Barcelona')
def tutor_keyword_barcelona_1():
    return render_template('landing.html', title="global " , keyword="tutor" , location="in Barcelona")
##### Salsa Classes Keywords ###

@app.route('/salsa-classes-in-barcelona')
def classes_keyword_salsa_1():
    return render_template('landing.html', title="salsa " , keyword="classes" , location="in Barcelona")

##### Yoga Classes Keywords ###
@app.route('/yoga-classes-in-barcelona')
def classes_keyword_yoga_1():
    return render_template('landing.html', title="yoga " , keyword="classes" , location="in Barcelona")

##### Programmming Level 2 Keywords ###
@app.route('/python-barcelona')
def programming_keyword_barcelona_1():
    return render_template('landing.html', title="python " , keyword="classes" , location="in Barcelona")
@app.route('/ruby-barcelona')
def programming_keyword_barcelona_2():
    return render_template('landing.html', title="ruby " , keyword="classes" , location="in Barcelona")
@app.route('/html5-barcelona')
def programming_keyword_barcelona_3():
    return render_template('landing.html', title="html5 " , keyword="classes" , location="in Barcelona")
@app.route('/java-barcelona')
def programming_keyword_barcelona_4():
    return render_template('landing.html', title="java " , keyword="classes" , location="in Barcelona")
@app.route('/django-barcelona')
def programming_keyword_barcelona_5():
    return render_template('landing.html', title="django " , keyword="classes" , location="in Barcelona")
@app.route('/javascript-barcelona')
def programming_keyword_barcelona_6():
    return render_template('landing.html', title="javascript " , keyword="classes" , location="in Barcelona")
@app.route('/learn-to-code-barcelona')
def programming_keyword_barcelona_7():
    return render_template('landing.html', title="learn to" , keyword="code" , location="Barcelona")
@app.route('/learn-to-code-c-sharp-barcelona')
def programming_keyword_barcelona_8():
    return render_template('landing.html', title="learn to code" , keyword="c sharp" , location="Barcelona")
@app.route('/learn-to-code-c++-barcelona')
def programming_keyword_barcelona_9():
    return render_template('landing.html', title="learn to code" , keyword="c++" , location="Barcelona")
@app.route('/learn-programming-in-barcelona')
def programming_keyword_barcelona_10():
    return render_template('landing.html', title="learn" , keyword="programming" , location="Barcelona")
@app.route('/learn-software-development-in-barcelona')
def programming_keyword_barcelona_11():
    return render_template('landing.html', title="learn" , keyword="software development" , location="Barcelona")
@app.route('/programming-classes-in-barcelona')
def programming_keyword_barcelona_12():
    return render_template('landing.html', title="programming" , keyword="classes" , location="Barcelona")
@app.route('/programming-courses-in-barcelona')
def programming_keyword_barcelona_13():
    return render_template('landing.html', title="programming" , keyword="courses" , location="Barcelona")
@app.route('/intensive-programming-classes-in-barcelona')
def programming_keyword_barcelona_14():
    return render_template('landing.html', title="intensive programming" , keyword="classes" , location="Barcelona")
@app.route('/cooking-course-barcelona')
def cooking_keyword_barcelona_1():
    return render_template('landing.html', title="cooking" , keyword="course" , location="Barcelona")
@app.route('/photography-course-barcelona')
def photography_keyword_barcelona_1():
    return render_template('landing.html', title="photography" , keyword="course" , location="Barcelona")
@app.route('/art-course-barcelona')
def art_keyword_barcelona_1():
    return render_template('landing.html', title="art" , keyword="course" , location="Barcelona")
@app.route('/group-exercise-classes-barcelona')
def exercise_keyword_barcelona_1():
    return render_template('landing.html', title="group" , keyword="exercise classes" , location="Barcelona")
@app.route('/i-want-to-learn-spanish')
def landing_students_1():
    return render_template('landing_students.html', title="i want to learn" , keyword="spanish" , location="Barcelona")
@app.route('/where-to-learn-computer-programming')
def landing_students_2():
    return render_template('landing_students.html', title="where can I learn" , keyword="computer programming" , location="Barcelona")
@app.route('/easy-way-to-learn-computer-programming')
def landing_students_3():
    return render_template('landing_students.html', title="easy way to learn" , keyword="computer programming" , location="Barcelona")
@app.route('/best-way-to-learn-python-2016')
def landing_students_4():
    return render_template('landing_students.html', title="best way to learn" , keyword="python 2016" , location="Barcelona")
@app.route('/private-courses-after-graduation')
def landing_students_5():
    return render_template('landing_students.html', title="private" , keyword="courses after graduation" , location="Barcelona")
@app.route('/private-courses-after-graduation')
def landing_students_6():
    return render_template('landing_students.html', title="private" , keyword="courses after graduation" , location="Barcelona")
@app.route('/professional-courses-in-commerce-after-graduation')
def landing_students_7():
    return render_template('landing_students.html', title="professional" , keyword="courses in commerce after graduation" , location="Barcelona")
@app.route('/diploma-courses-after-graduation-in-commerce')
def landing_students_8():
    return render_template('landing_students.html', title="diploma" , keyword="courses after graduation in commerce" , location="Barcelona")
@app.route('/new-courses-for-commerce-students-after-graduation')
def landing_students_9():
    return render_template('landing_students.html', title="new" , keyword="courses for commerce students after graduation" , location="Barcelona")
@app.route('/teach-english-abroad-without-tefl-or-degree')
def landing_students_10():
    return render_template('landing_students.html', title="teach abroad without tefl or degree in" , keyword="english" , location="Barcelona")

#####################USA CITIES KEYWORDS START###########
@app.route('/free-computer-classes-nyc')
def landing_usa_free1():
    return render_template('landing_usa.html', data="free computer classes nyc ")
@app.route('/free-computer-classes-near-me')
def landing_usa_free2():
    return render_template('landing_usa.html', data="free-computer classes near me ")
@app.route('/free-computer-classes-in-chicago')
def landing_usa_free3():
    return render_template('landing_usa.html', data="free computer classes in chicago ")
@app.route('/free-computer-classes-in-philadelphia')
def landing_usa_free4():
    return render_template('landing_usa.html', data="free computer classes in philadelphia ")
@app.route('/free-computer-classes-in-houston')
def landing_usa_free5():
    return render_template('landing_usa.html', data="free computer classes in houston")
@app.route('/free-computer-classes-in-dc')
def landing_usa_free6():
    return render_template('landing_usa.html', data="free computer classes in dc")
@app.route('/free-computer-classes-for-adults-in-nyc')
def landing_usa_free7():
    return render_template('landing_usa.html', data="free computer classes for adults in nyc")
@app.route('/free-computer-classes-san-diego')
def landing_usa_free8():
    return render_template('landing_usa.html', data="free computer classes san diego")
@app.route('/free-computer-classes-nj')
def landing_usa_free9():
    return render_template('landing_usa.html', data="free computer classes nj")

@app.route('/free-computer-classes-in-las-vegas')
def landing_usa_free10():
    return render_template('landing_usa.html', data="free computer classes in las vegas")
@app.route('/free-computer-classes-in-nyc')
def landing_usa_free11():
    return render_template('landing_usa.html', data="free computer classes in nyc")
@app.route('/free-computer-classes-buffalo-ny')
def landing_usa_free12():
    return render_template('landing_usa.html', data="free computer classes buffalo ny")
@app.route('/free-computer-classes-los-angeles')
def landing_usa_free13():
    return render_template('landing_usa.html', data="free computer classes los angeles")
@app.route('/free-computer-classes-in-columbus-ohio')
def landing_usa_free14():
    return render_template('landing_usa.html', data="free computer classes in columbus ohio")
@app.route('/free-computer-classes-charlotte-nc')
def landing_usa_free15():
    return render_template('landing_usa.html', data="free computer classes charlotte nc")
@app.route('/free-computer-classes-philadelphia')
def landing_usa_free16():
    return render_template('landing_usa.html', data="free computer classes philadelphia")
@app.route('/free-computer-classes-seattle')
def landing_usa_free17():
    return render_template('landing_usa.html', data="free computer classes seattle")
@app.route('/free-computer-classes-minneapolis')
def landing_usa_free18():
    return render_template('landing_usa.html', data="free computer classes minneapolis")
@app.route('/free-computer-classes-austin')
def landing_usa_free19():
    return render_template('landing_usa.html', data="free computer classes austin")

@app.route('/free-computer-classes-in-dallas-tx')
def landing_usa_free20():
    return render_template('landing_usa.html', data="free computer classes in dallas tx")
@app.route('/free-computer-classes-in-ri')
def landing_usa_free21():
    return render_template('landing_usa.html', data="free computer classes in ri")
@app.route('/free-computer-classes-in-san-antonio-tx')
def landing_usa_free22():
    return render_template('landing_usa.html', data="free computer classes in san antonio tx")
@app.route('/free-computer-classes-new-orleans')
def landing_usa_free23():
    return render_template('landing_usa.html', data="free computer classes new orleans")
@app.route('/free-computer-classes-nashville-tn')
def landing_usa_free24():
    return render_template('landing_usa.html', data="free computer classes nashville tn")
@app.route('/free-computer-classes-boston')
def landing_usa_free25():
    return render_template('landing_usa.html', data="free computer classes boston")
@app.route('/free-computer-classes-denver')
def landing_usa_free26():
    return render_template('landing_usa.html', data="free computer classes denver")
@app.route('/free-computer-classes-brooklyn')
def landing_usa_free27():
    return render_template('landing_usa.html', data="free computer classes brooklyn")
@app.route('/free-computer-classes-miami')
def landing_usa_free28():
    return render_template('landing_usa.html', data="free computer classes miami")
@app.route('/free-computer-classes-houston')
def landing_usa_free29():
    return render_template('landing_usa.html', data="free computer classes houston")

@app.route('/free-computer-classes-bronx')
def landing_usa_free30():
    return render_template('landing_usa.html', data="free computer classes bronx")
@app.route('/free-computer-classes-birmingham-al')
def landing_usa_free31():
    return render_template('landing_usa.html', data="free computer classes birmingham al")
@app.route('/free-computer-classes-san-francisco')
def landing_usa_free32():
    return render_template('landing_usa.html', data="free computer classes san francisco")
@app.route('/free-computer-classes-in-atlanta-ga')
def landing_usa_free33():
    return render_template('landing_usa.html', data="free computer classes in atlanta ga")
@app.route('/free-computer-classes-phoenix-az')
def landing_usa_free34():
    return render_template('landing_usa.html', data="free computer classes phoenix az")
@app.route('/free-computer-classes-richmond-va')
def landing_usa_free35():
    return render_template('landing_usa.html', data="free computer classes richmond va")
@app.route('/free-computer-classes-mn')
def landing_usa_free36():
    return render_template('landing_usa.html', data="free computer classes mn")
@app.route('/free-computer-classes-san-antonio-tx')
def landing_usa_free37():
    return render_template('landing_usa.html', data="free computer classes san antonio tx")
@app.route('/free-computer-classes-sacramento')
def landing_usa_free38():
    return render_template('landing_usa.html', data="free computer classes sacramento")
@app.route('/free-computer-classes-austin-tx')
def landing_usa_free39():
    return render_template('landing_usa.html', data="free computer classes austin tx")

@app.route('/free-computer-classes-rochester-ny')
def landing_usa_free40():
    return render_template('landing_usa.html', data="free computer classes rochester ny")
@app.route('/free-computer-classes-in-charlotte-nc')
def landing_usa_free41():
    return render_template('landing_usa.html', data="free computer classes in charlotte nc")
@app.route('/free-computer-classes-tucson')
def landing_usa_free42():
    return render_template('landing_usa.html', data="free computer classes tucson")
@app.route('/free-computer-classes-pittsburgh')
def landing_usa_free43():
    return render_template('landing_usa.html', data="free computer classes pittsburgh")
@app.route('/free-computer-classes-in-detroit')
def landing_usa_free44():
    return render_template('landing_usa.html', data="free computer classes in detroit")
@app.route('/free-computer-classes-kansas-city')
def landing_usa_free45():
    return render_template('landing_usa.html', data="free computer classes kansas city")
@app.route('/free-computer-classes-detroit')
def landing_usa_free46():
    return render_template('landing_usa.html', data="free computer classes detroit")
@app.route('/free-computer-classes-on-the-internet')
def landing_usa_free47():
    return render_template('landing_usa.html', data=" ")
@app.route('/teach-english-abroad-without-tefl-or-degree')
def landing_usa_free48():
    return render_template('landing_usa.html', data="free computer classes on the internet")
@app.route('/free-computer-classes-mesa-az')
def landing_usa_free49():
    return render_template('landing_usa.html', data="free computer classes mesa az")

@app.route('/free-computer-classes-greenville-sc')
def landing_usa_free50():
    return render_template('landing_usa.html', data="free computer classes greenville sc")
@app.route('/free-computer-classes-washington-dc')
def landing_usa_free51():
    return render_template('landing_usa.html', data="free computer classes washington dc")
@app.route('/free-computer-classes-louisville-ky')
def landing_usa_free52():
    return render_template('landing_usa.html', data="free computer classes louisville ky")
@app.route('/free-computer-classes-in-knoxville-tn')
def landing_usa_free53():
    return render_template('landing_usa.html', data="free computer classes in knoxville tn")
@app.route('/free-computer-classes-colorado-springs')
def landing_usa_free54():
    return render_template('landing_usa.html', data="free computer classes colorado springs")
@app.route('/free-computer-classes-memphis-tn')
def landing_usa_free55():
    return render_template('landing_usa.html', data="free computer classes memphis tn")
@app.route('/free-computer-classes-phoenix')
def landing_usa_free56():
    return render_template('landing_usa.html', data="free computer classes phoenix")
@app.route('/free-computer-classes-boston-ma')
def landing_usa_free57():
    return render_template('landing_usa.html', data="free computer classes boston ma")
@app.route('/free-computer-classes-st-louis-mo')
def landing_usa_free58():
    return render_template('landing_usa.html', data="free computer classes st louis mo")
@app.route('/free-computer-classes-atlanta-ga')
def landing_usa_free59():
    return render_template('landing_usa.html', data="free computer classes atlanta ga")

@app.route('/free-computer-classes-baton-rouge')
def landing_usa_free60():
    return render_template('landing_usa.html', data="free computer classes baton rouge")
@app.route('/free-computer-classes-in-houston-texas')
def landing_usa_free61():
    return render_template('landing_usa.html', data="free computer classes in houston texas")
@app.route('/free-computer-classes-san-antonio')
def landing_usa_free62():
    return render_template('landing_usa.html', data="free computer classes san antonio")
@app.route('/free-computer-classes-houston-tx')
def landing_usa_free63():
    return render_template('landing_usa.html', data="free computer classes houston tx")
@app.route('/free-computer-classes-dallas-tx')
def landing_usa_free64():
    return render_template('landing_usa.html', data=" ")
@app.route('/free-computer-classes-portland-oregon')
def landing_usa_free65():
    return render_template('landing_usa.html', data="free computer classes portland oregon")
@app.route('/free-computer-classes-queens-ny')
def landing_usa_free66():
    return render_template('landing_usa.html', data="free computer classes queens ny")
@app.route('/free-computer-classes-cleveland-ohio')
def landing_usa_free67():
    return render_template('landing_usa.html', data="free computer classes cleveland ohio")
@app.route('/free-computer-classes-bronx-ny')
def landing_usa_free68():
    return render_template('landing_usa.html', data="free computer classes bronx ny")
@app.route('/free-computer-classes-tampa')
def landing_usa_free69():
    return render_template('landing_usa.html', data="free computer classes tampa")

@app.route('/free-computer-classes-albany-ny')
def landing_usa_free70():
    return render_template('landing_usa.html', data="free computer classes albany ny")
@app.route('/free-computer-classes-tampa-fl')
def landing_usa_free71():
    return render_template('landing_usa.html', data="free computer classes tampa fl")
@app.route('/free-computer-classes-brooklyn-ny')
def landing_usa_free72():
    return render_template('landing_usa.html', data="free computer classes brooklyn ny")
@app.route('/free-computer-classes-dc')
def landing_usa_free73():
    return render_template('landing_usa.html', data="free computer classes dc")
@app.route('/free-computer-classes-knoxville-tne')
def landing_usa_free74():
    return render_template('landing_usa.html', data="free computer classes knoxville tn")
@app.route('/free-computer-classes-raleigh-nc')
def landing_usa_free75():
    return render_template('landing_usa.html', data="free computer classes raleigh nc")
@app.route('/free-computer-classes-staten-island-ny')
def landing_usa_free76():
    return render_template('landing_usa.html', data="free computer classes staten island ny")
@app.route('/free-computer-classes-anchorage-ak')
def landing_usa_free77():
    return render_template('landing_usa.html', data="free computer classes anchorage ak")
@app.route('/free-computer-classes-courses')
def landing_usa_free78():
    return render_template('landing_usa.html', data="free computer classes courses")
@app.route('/free-computer-classes-sacramento-ca')
def landing_usa_free79():
    return render_template('landing_usa.html', data="free computer classes sacramento ca")

@app.route('/free-computer-classes-chicago-il')
def landing_usa_free80():
    return render_template('landing_usa.html', data="free computer classes chicago il")
@app.route('/free-computer-classes-miami-fl')
def landing_usa_free81():
    return render_template('landing_usa.html', data="free computer classes miami fl")
@app.route('/free-computer-classes-bakersfield-ca')
def landing_usa_free82():
    return render_template('landing_usa.html', data="free computer classes bakersfield ca")
@app.route('/free-computer-classes-baltimore-md')
def landing_usa_free83():
    return render_template('landing_usa.html', data="free computer classes baltimore md")
@app.route('/free-computer-classes-new-york')
def landing_usa_free84():
    return render_template('landing_usa.html', data="free computer classes new york")
@app.route('/free-computer-classes-las-vegas-nv')
def landing_usa_free85():
    return render_template('landing_usa.html', data="free computer classes las vegas nv")
@app.route('/free-computer-classes-san-diego-ca')
def landing_usa_free86():
    return render_template('landing_usa.html', data="free computer classes san diego ca")
@app.route('/free-computer-classes-austin-texas')
def landing_usa_free87():
    return render_template('landing_usa.html', data="free computer classes austin texas")
@app.route('/free-computer-classes-milwaukee')
def landing_usa_free88():
    return render_template('landing_usa.html', data="free computer classes milwaukee")
@app.route('/free-computer-classes-reno-nv')
def landing_usa_free89():
    return render_template('landing_usa.html', data="free computer classes reno nv")

@app.route('/free-computer-classes-oklahoma-city')
def landing_usa_free90():
    return render_template('landing_usa.html', data="free computer classes oklahoma city")
@app.route('/free-computer-classes-erie-pa')
def landing_usa_free91():
    return render_template('landing_usa.html', data="free computer classes erie pa")
@app.route('/free-computer-classes-for-adults-in-chicago')
def landing_usa_free92():
    return render_template('landing_usa.html', data="free computer classes for adults in chicago")
###############################################################################Computer Programming Classes #############
@app.route('/computer-programming-classes-nyc')
def landing_usa_computer_programming1():
    return render_template('landing_usa.html', data="computer programming classes nyc ")
@app.route('/computer-programming-classes-near-me')
def landing_usa_computer_programming2():
    return render_template('landing_usa.html', data="computer-programming classes near me ")
@app.route('/computer-programming-classes-in-chicago')
def landing_usa_computer_programming3():
    return render_template('landing_usa.html', data="computer programming classes in chicago ")
@app.route('/computer-programming-classes-in-philadelphia')
def landing_usa_computer_programming4():
    return render_template('landing_usa.html', data="computer programming classes in philadelphia ")
@app.route('/computer-programming-classes-in-houston')
def landing_usa_computer_programming5():
    return render_template('landing_usa.html', data="computer programming classes in houston")
@app.route('/computer-programming-classes-in-dc')
def landing_usa_computer_programming6():
    return render_template('landing_usa.html', data="computer programming classes in dc")
@app.route('/computer-programming-classes-for-adults-in-nyc')
def landing_usa_computer_programming7():
    return render_template('landing_usa.html', data="computer programming classes for adults in nyc")
@app.route('/computer-programming-classes-san-diego')
def landing_usa_computer_programming8():
    return render_template('landing_usa.html', data="computer programming classes san diego")
@app.route('/computer-programming-classes-nj')
def landing_usa_computer_programming9():
    return render_template('landing_usa.html', data="computer programming classes nj")

@app.route('/computer-programming-classes-in-las-vegas')
def landing_usa_computer_programming10():
    return render_template('landing_usa.html', data="computer programming classes in las vegas")
@app.route('/computer-programming-classes-in-nyc')
def landing_usa_computer_programming11():
    return render_template('landing_usa.html', data="computer programming classes in nyc")
@app.route('/computer-programming-classes-buffalo-ny')
def landing_usa_computer_programming12():
    return render_template('landing_usa.html', data="computer programming classes buffalo ny")
@app.route('/computer-programming-classes-los-angeles')
def landing_usa_computer_programming13():
    return render_template('landing_usa.html', data="computer programming classes los angeles")
@app.route('/computer-programming-classes-in-columbus-ohio')
def landing_usa_computer_programming14():
    return render_template('landing_usa.html', data="computer programming classes in columbus ohio")
@app.route('/computer-programming-classes-charlotte-nc')
def landing_usa_computer_programming15():
    return render_template('landing_usa.html', data="computer programming classes charlotte nc")
@app.route('/computer-programming-classes-philadelphia')
def landing_usa_computer_programming16():
    return render_template('landing_usa.html', data="computer programming classes philadelphia")
@app.route('/computer-programming-classes-seattle')
def landing_usa_computer_programming17():
    return render_template('landing_usa.html', data="computer programming classes seattle")
@app.route('/computer-programming-classes-minneapolis')
def landing_usa_computer_programming18():
    return render_template('landing_usa.html', data="computer programming classes minneapolis")
@app.route('/computer-programming-classes-austin')
def landing_usa_computer_programming19():
    return render_template('landing_usa.html', data="computer programming classes austin")

@app.route('/computer-programming-classes-in-dallas-tx')
def landing_usa_computer_programming20():
    return render_template('landing_usa.html', data="computer programming classes in dallas tx")
@app.route('/computer-programming-classes-in-ri')
def landing_usa_computer_programming21():
    return render_template('landing_usa.html', data="computer programming classes in ri")
@app.route('/computer-programming-classes-in-san-antonio-tx')
def landing_usa_computer_programming22():
    return render_template('landing_usa.html', data="computer programming classes in san antonio tx")
@app.route('/computer-programming-classes-new-orleans')
def landing_usa_computer_programming23():
    return render_template('landing_usa.html', data="computer programming classes new orleans")
@app.route('/computer-programming-classes-nashville-tn')
def landing_usa_computer_programming24():
    return render_template('landing_usa.html', data="computer programming classes nashville tn")
@app.route('/computer-programming-classes-boston')
def landing_usa_computer_programming25():
    return render_template('landing_usa.html', data="computer programming classes boston")
@app.route('/computer-programming-classes-denver')
def landing_usa_computer_programming26():
    return render_template('landing_usa.html', data="computer programming classes denver")
@app.route('/computer-programming-classes-brooklyn')
def landing_usa_computer_programming27():
    return render_template('landing_usa.html', data="computer programming classes brooklyn")
@app.route('/computer-programming-classes-miami')
def landing_usa_computer_programming28():
    return render_template('landing_usa.html', data="computer programming classes miami")
@app.route('/computer-programming-classes-houston')
def landing_usa_computer_programming29():
    return render_template('landing_usa.html', data="computer programming classes houston")

@app.route('/computer-programming-classes-bronx')
def landing_usa_computer_programming30():
    return render_template('landing_usa.html', data="computer programming classes bronx")
@app.route('/computer-programming-classes-birmingham-al')
def landing_usa_computer_programming31():
    return render_template('landing_usa.html', data="computer programming classes birmingham al")
@app.route('/computer-programming-classes-san-francisco')
def landing_usa_computer_programming32():
    return render_template('landing_usa.html', data="computer programming classes san francisco")
@app.route('/computer-programming-classes-in-atlanta-ga')
def landing_usa_computer_programming33():
    return render_template('landing_usa.html', data="computer programming classes in atlanta ga")
@app.route('/computer-programming-classes-phoenix-az')
def landing_usa_computer_programming34():
    return render_template('landing_usa.html', data="computer programming classes phoenix az")
@app.route('/computer-programming-classes-richmond-va')
def landing_usa_computer_programming35():
    return render_template('landing_usa.html', data="computer programming classes richmond va")
@app.route('/computer-programming-classes-mn')
def landing_usa_computer_programming36():
    return render_template('landing_usa.html', data="computer programming classes mn")
@app.route('/computer-programming-classes-san-antonio-tx')
def landing_usa_computer_programming37():
    return render_template('landing_usa.html', data="computer programming classes san antonio tx")
@app.route('/computer-programming-classes-sacramento')
def landing_usa_computer_programming38():
    return render_template('landing_usa.html', data="computer programming classes sacramento")
@app.route('/computer-programming-classes-austin-tx')
def landing_usa_computer_programming39():
    return render_template('landing_usa.html', data="computer programming classes austin tx")

@app.route('/computer-programming-classes-rochester-ny')
def landing_usa_computer_programming40():
    return render_template('landing_usa.html', data="computer programming classes rochester ny")
@app.route('/computer-programming-classes-in-charlotte-nc')
def landing_usa_computer_programming41():
    return render_template('landing_usa.html', data="computer programming classes in charlotte nc")
@app.route('/computer-programming-classes-tucson')
def landing_usa_computer_programming42():
    return render_template('landing_usa.html', data="computer programming classes tucson")
@app.route('/computer-programming-classes-pittsburgh')
def landing_usa_computer_programming43():
    return render_template('landing_usa.html', data="computer programming classes pittsburgh")
@app.route('/computer-programming-classes-in-detroit')
def landing_usa_computer_programming44():
    return render_template('landing_usa.html', data="computer programming classes in detroit")
@app.route('/computer-programming-classes-kansas-city')
def landing_usa_computer_programming45():
    return render_template('landing_usa.html', data="computer programming classes kansas city")
@app.route('/computer-programming-classes-detroit')
def landing_usa_computer_programming46():
    return render_template('landing_usa.html', data="computer programming classes detroit")
@app.route('/computer-programming-classes-on-the-internet')
def landing_usa_computer_programming47():
    return render_template('landing_usa.html', data=" ")
@app.route('/teach-english-abroad-without-tefl-or-degree')
def landing_usa_computer_programming48():
    return render_template('landing_usa.html', data="computer programming classes on the internet")
@app.route('/computer-programming-classes-mesa-az')
def landing_usa_computer_programming49():
    return render_template('landing_usa.html', data="computer programming classes mesa az")

@app.route('/computer-programming-classes-greenville-sc')
def landing_usa_computer_programming50():
    return render_template('landing_usa.html', data="computer programming classes greenville sc")
@app.route('/computer-programming-classes-washington-dc')
def landing_usa_computer_programming51():
    return render_template('landing_usa.html', data="computer programming classes washington dc")
@app.route('/computer-programming-classes-louisville-ky')
def landing_usa_computer_programming52():
    return render_template('landing_usa.html', data="computer programming classes louisville ky")
@app.route('/computer-programming-classes-in-knoxville-tn')
def landing_usa_computer_programming53():
    return render_template('landing_usa.html', data="computer programming classes in knoxville tn")
@app.route('/computer-programming-classes-colorado-springs')
def landing_usa_computer_programming54():
    return render_template('landing_usa.html', data="computer programming classes colorado springs")
@app.route('/computer-programming-classes-memphis-tn')
def landing_usa_computer_programming55():
    return render_template('landing_usa.html', data="computer programming classes memphis tn")
@app.route('/computer-programming-classes-phoenix')
def landing_usa_computer_programming56():
    return render_template('landing_usa.html', data="computer programming classes phoenix")
@app.route('/computer-programming-classes-boston-ma')
def landing_usa_computer_programming57():
    return render_template('landing_usa.html', data="computer programming classes boston ma")
@app.route('/computer-programming-classes-st-louis-mo')
def landing_usa_computer_programming58():
    return render_template('landing_usa.html', data="computer programming classes st louis mo")
@app.route('/computer-programming-classes-atlanta-ga')
def landing_usa_computer_programming59():
    return render_template('landing_usa.html', data="computer programming classes atlanta ga")

@app.route('/computer-programming-classes-baton-rouge')
def landing_usa_computer_programming60():
    return render_template('landing_usa.html', data="computer programming classes baton rouge")
@app.route('/computer-programming-classes-in-houston-texas')
def landing_usa_computer_programming61():
    return render_template('landing_usa.html', data="computer programming classes in houston texas")
@app.route('/computer-programming-classes-san-antonio')
def landing_usa_computer_programming62():
    return render_template('landing_usa.html', data="computer programming classes san antonio")
@app.route('/computer-programming-classes-houston-tx')
def landing_usa_computer_programming63():
    return render_template('landing_usa.html', data="computer programming classes houston tx")
@app.route('/computer-programming-classes-dallas-tx')
def landing_usa_computer_programming64():
    return render_template('landing_usa.html', data=" ")
@app.route('/computer-programming-classes-portland-oregon')
def landing_usa_computer_programming65():
    return render_template('landing_usa.html', data="computer programming classes portland oregon")
@app.route('/computer-programming-classes-queens-ny')
def landing_usa_computer_programming66():
    return render_template('landing_usa.html', data="computer programming classes queens ny")
@app.route('/computer-programming-classes-cleveland-ohio')
def landing_usa_computer_programming67():
    return render_template('landing_usa.html', data="computer programming classes cleveland ohio")
@app.route('/computer-programming-classes-bronx-ny')
def landing_usa_computer_programming68():
    return render_template('landing_usa.html', data="computer programming classes bronx ny")
@app.route('/computer-programming-classes-tampa')
def landing_usa_computer_programming69():
    return render_template('landing_usa.html', data="computer programming classes tampa")

@app.route('/computer-programming-classes-albany-ny')
def landing_usa_computer_programming70():
    return render_template('landing_usa.html', data="computer programming classes albany ny")
@app.route('/computer-programming-classes-tampa-fl')
def landing_usa_computer_programming71():
    return render_template('landing_usa.html', data="computer programming classes tampa fl")
@app.route('/computer-programming-classes-brooklyn-ny')
def landing_usa_computer_programming72():
    return render_template('landing_usa.html', data="computer programming classes brooklyn ny")
@app.route('/computer-programming-classes-dc')
def landing_usa_computer_programming73():
    return render_template('landing_usa.html', data="computer programming classes dc")
@app.route('/computer-programming-classes-knoxville-tne')
def landing_usa_computer_programming74():
    return render_template('landing_usa.html', data="computer programming classes knoxville tn")
@app.route('/computer-programming-classes-raleigh-nc')
def landing_usa_computer_programming75():
    return render_template('landing_usa.html', data="computer programming classes raleigh nc")
@app.route('/computer-programming-classes-staten-island-ny')
def landing_usa_computer_programming76():
    return render_template('landing_usa.html', data="computer programming classes staten island ny")
@app.route('/computer-programming-classes-anchorage-ak')
def landing_usa_computer_programming77():
    return render_template('landing_usa.html', data="computer programming classes anchorage ak")
@app.route('/computer-programming-classes-courses')
def landing_usa_computer_programming78():
    return render_template('landing_usa.html', data="computer programming classes courses")
@app.route('/computer-programming-classes-sacramento-ca')
def landing_usa_computer_programming79():
    return render_template('landing_usa.html', data="computer programming classes sacramento ca")

@app.route('/computer-programming-classes-chicago-il')
def landing_usa_computer_programming80():
    return render_template('landing_usa.html', data="computer programming classes chicago il")
@app.route('/computer-programming-classes-miami-fl')
def landing_usa_computer_programming81():
    return render_template('landing_usa.html', data="computer programming classes miami fl")
@app.route('/computer-programming-classes-bakersfield-ca')
def landing_usa_computer_programming82():
    return render_template('landing_usa.html', data="computer programming classes bakersfield ca")
@app.route('/computer-programming-classes-baltimore-md')
def landing_usa_computer_programming83():
    return render_template('landing_usa.html', data="computer programming classes baltimore md")
@app.route('/computer-programming-classes-new-york')
def landing_usa_computer_programming84():
    return render_template('landing_usa.html', data="computer programming classes new york")
@app.route('/computer-programming-classes-las-vegas-nv')
def landing_usa_computer_programming85():
    return render_template('landing_usa.html', data="computer programming classes las vegas nv")
@app.route('/computer-programming-classes-san-diego-ca')
def landing_usa_computer_programming86():
    return render_template('landing_usa.html', data="computer programming classes san diego ca")
@app.route('/computer-programming-classes-austin-texas')
def landing_usa_computer_programming87():
    return render_template('landing_usa.html', data="computer programming classes austin texas")
@app.route('/computer-programming-classes-milwaukee')
def landing_usa_computer_programming88():
    return render_template('landing_usa.html', data="computer programming classes milwaukee")
@app.route('/computer-programming-classes-reno-nv')
def landing_usa_computer_programming89():
    return render_template('landing_usa.html', data="computer programming classes reno nv")

@app.route('/computer-programming-classes-oklahoma-city')
def landing_usa_computer_programming90():
    return render_template('landing_usa.html', data="computer programming classes oklahoma city")
@app.route('/computer-programming-classes-erie-pa')
def landing_usa_computer_programming91():
    return render_template('landing_usa.html', data="computer programming classes erie pa")
@app.route('/computer-programming-classes-for-adults-in-chicago')
def landing_usa_computer_programming92():
    return render_template('landing_usa.html', data="computer programming classes for adults in chicago")
####################################################################Computer Science Classes###############
@app.route('/computer-science-classes-nyc')
def landing_usa_computer_science1():
    return render_template('landing_usa.html', data="computer science classes nyc ")
@app.route('/computer-science-classes-near-me')
def landing_usa_computer_science2():
    return render_template('landing_usa.html', data="computer-science classes near me ")
@app.route('/computer-science-classes-in-chicago')
def landing_usa_computer_science3():
    return render_template('landing_usa.html', data="computer science classes in chicago ")
@app.route('/computer-science-classes-in-philadelphia')
def landing_usa_computer_science4():
    return render_template('landing_usa.html', data="computer science classes in philadelphia ")
@app.route('/computer-science-classes-in-houston')
def landing_usa_computer_science5():
    return render_template('landing_usa.html', data="computer science classes in houston")
@app.route('/computer-science-classes-in-dc')
def landing_usa_computer_science6():
    return render_template('landing_usa.html', data="computer science classes in dc")
@app.route('/computer-science-classes-for-adults-in-nyc')
def landing_usa_computer_science7():
    return render_template('landing_usa.html', data="computer science classes for adults in nyc")
@app.route('/computer-science-classes-san-diego')
def landing_usa_computer_science8():
    return render_template('landing_usa.html', data="computer science classes san diego")
@app.route('/computer-science-classes-nj')
def landing_usa_computer_science9():
    return render_template('landing_usa.html', data="computer science classes nj")

@app.route('/computer-science-classes-in-las-vegas')
def landing_usa_computer_science10():
    return render_template('landing_usa.html', data="computer science classes in las vegas")
@app.route('/computer-science-classes-in-nyc')
def landing_usa_computer_science11():
    return render_template('landing_usa.html', data="computer science classes in nyc")
@app.route('/computer-science-classes-buffalo-ny')
def landing_usa_computer_science12():
    return render_template('landing_usa.html', data="computer science classes buffalo ny")
@app.route('/computer-science-classes-los-angeles')
def landing_usa_computer_science13():
    return render_template('landing_usa.html', data="computer science classes los angeles")
@app.route('/computer-science-classes-in-columbus-ohio')
def landing_usa_computer_science14():
    return render_template('landing_usa.html', data="computer science classes in columbus ohio")
@app.route('/computer-science-classes-charlotte-nc')
def landing_usa_computer_science15():
    return render_template('landing_usa.html', data="computer science classes charlotte nc")
@app.route('/computer-science-classes-philadelphia')
def landing_usa_computer_science16():
    return render_template('landing_usa.html', data="computer science classes philadelphia")
@app.route('/computer-science-classes-seattle')
def landing_usa_computer_science17():
    return render_template('landing_usa.html', data="computer science classes seattle")
@app.route('/computer-science-classes-minneapolis')
def landing_usa_computer_science18():
    return render_template('landing_usa.html', data="computer science classes minneapolis")
@app.route('/computer-science-classes-austin')
def landing_usa_computer_science19():
    return render_template('landing_usa.html', data="computer science classes austin")

@app.route('/computer-science-classes-in-dallas-tx')
def landing_usa_computer_science20():
    return render_template('landing_usa.html', data="computer science classes in dallas tx")
@app.route('/computer-science-classes-in-ri')
def landing_usa_computer_science21():
    return render_template('landing_usa.html', data="computer science classes in ri")
@app.route('/computer-science-classes-in-san-antonio-tx')
def landing_usa_computer_science22():
    return render_template('landing_usa.html', data="computer science classes in san antonio tx")
@app.route('/computer-science-classes-new-orleans')
def landing_usa_computer_science23():
    return render_template('landing_usa.html', data="computer science classes new orleans")
@app.route('/computer-science-classes-nashville-tn')
def landing_usa_computer_science24():
    return render_template('landing_usa.html', data="computer science classes nashville tn")
@app.route('/computer-science-classes-boston')
def landing_usa_computer_science25():
    return render_template('landing_usa.html', data="computer science classes boston")
@app.route('/computer-science-classes-denver')
def landing_usa_computer_science26():
    return render_template('landing_usa.html', data="computer science classes denver")
@app.route('/computer-science-classes-brooklyn')
def landing_usa_computer_science27():
    return render_template('landing_usa.html', data="computer science classes brooklyn")
@app.route('/computer-science-classes-miami')
def landing_usa_computer_science28():
    return render_template('landing_usa.html', data="computer science classes miami")
@app.route('/computer-science-classes-houston')
def landing_usa_computer_science29():
    return render_template('landing_usa.html', data="computer science classes houston")

@app.route('/computer-science-classes-bronx')
def landing_usa_computer_science30():
    return render_template('landing_usa.html', data="computer science classes bronx")
@app.route('/computer-science-classes-birmingham-al')
def landing_usa_computer_science31():
    return render_template('landing_usa.html', data="computer science classes birmingham al")
@app.route('/computer-science-classes-san-francisco')
def landing_usa_computer_science32():
    return render_template('landing_usa.html', data="computer science classes san francisco")
@app.route('/computer-science-classes-in-atlanta-ga')
def landing_usa_computer_science33():
    return render_template('landing_usa.html', data="computer science classes in atlanta ga")
@app.route('/computer-science-classes-phoenix-az')
def landing_usa_computer_science34():
    return render_template('landing_usa.html', data="computer science classes phoenix az")
@app.route('/computer-science-classes-richmond-va')
def landing_usa_computer_science35():
    return render_template('landing_usa.html', data="computer science classes richmond va")
@app.route('/computer-science-classes-mn')
def landing_usa_computer_science36():
    return render_template('landing_usa.html', data="computer science classes mn")
@app.route('/computer-science-classes-san-antonio-tx')
def landing_usa_computer_science37():
    return render_template('landing_usa.html', data="computer science classes san antonio tx")
@app.route('/computer-science-classes-sacramento')
def landing_usa_computer_science38():
    return render_template('landing_usa.html', data="computer science classes sacramento")
@app.route('/computer-science-classes-austin-tx')
def landing_usa_computer_science39():
    return render_template('landing_usa.html', data="computer science classes austin tx")

@app.route('/computer-science-classes-rochester-ny')
def landing_usa_computer_science40():
    return render_template('landing_usa.html', data="computer science classes rochester ny")
@app.route('/computer-science-classes-in-charlotte-nc')
def landing_usa_computer_science41():
    return render_template('landing_usa.html', data="computer science classes in charlotte nc")
@app.route('/computer-science-classes-tucson')
def landing_usa_computer_science42():
    return render_template('landing_usa.html', data="computer science classes tucson")
@app.route('/computer-science-classes-pittsburgh')
def landing_usa_computer_science43():
    return render_template('landing_usa.html', data="computer science classes pittsburgh")
@app.route('/computer-science-classes-in-detroit')
def landing_usa_computer_science44():
    return render_template('landing_usa.html', data="computer science classes in detroit")
@app.route('/computer-science-classes-kansas-city')
def landing_usa_computer_science45():
    return render_template('landing_usa.html', data="computer science classes kansas city")
@app.route('/computer-science-classes-detroit')
def landing_usa_computer_science46():
    return render_template('landing_usa.html', data="computer science classes detroit")
@app.route('/computer-science-classes-on-the-internet')
def landing_usa_computer_science47():
    return render_template('landing_usa.html', data=" ")
@app.route('/teach-english-abroad-without-tefl-or-degree')
def landing_usa_computer_science48():
    return render_template('landing_usa.html', data="computer science classes on the internet")
@app.route('/computer-science-classes-mesa-az')
def landing_usa_computer_science49():
    return render_template('landing_usa.html', data="computer science classes mesa az")

@app.route('/computer-science-classes-greenville-sc')
def landing_usa_computer_science50():
    return render_template('landing_usa.html', data="computer science classes greenville sc")
@app.route('/computer-science-classes-washington-dc')
def landing_usa_computer_science51():
    return render_template('landing_usa.html', data="computer science classes washington dc")
@app.route('/computer-science-classes-louisville-ky')
def landing_usa_computer_science52():
    return render_template('landing_usa.html', data="computer science classes louisville ky")
@app.route('/computer-science-classes-in-knoxville-tn')
def landing_usa_computer_science53():
    return render_template('landing_usa.html', data="computer science classes in knoxville tn")
@app.route('/computer-science-classes-colorado-springs')
def landing_usa_computer_science54():
    return render_template('landing_usa.html', data="computer science classes colorado springs")
@app.route('/computer-science-classes-memphis-tn')
def landing_usa_computer_science55():
    return render_template('landing_usa.html', data="computer science classes memphis tn")
@app.route('/computer-science-classes-phoenix')
def landing_usa_computer_science56():
    return render_template('landing_usa.html', data="computer science classes phoenix")
@app.route('/computer-science-classes-boston-ma')
def landing_usa_computer_science57():
    return render_template('landing_usa.html', data="computer science classes boston ma")
@app.route('/computer-science-classes-st-louis-mo')
def landing_usa_computer_science58():
    return render_template('landing_usa.html', data="computer science classes st louis mo")
@app.route('/computer-science-classes-atlanta-ga')
def landing_usa_computer_science59():
    return render_template('landing_usa.html', data="computer science classes atlanta ga")

@app.route('/computer-science-classes-baton-rouge')
def landing_usa_computer_science60():
    return render_template('landing_usa.html', data="computer science classes baton rouge")
@app.route('/computer-science-classes-in-houston-texas')
def landing_usa_computer_science61():
    return render_template('landing_usa.html', data="computer science classes in houston texas")
@app.route('/computer-science-classes-san-antonio')
def landing_usa_computer_science62():
    return render_template('landing_usa.html', data="computer science classes san antonio")
@app.route('/computer-science-classes-houston-tx')
def landing_usa_computer_science63():
    return render_template('landing_usa.html', data="computer science classes houston tx")
@app.route('/computer-science-classes-dallas-tx')
def landing_usa_computer_science64():
    return render_template('landing_usa.html', data=" ")
@app.route('/computer-science-classes-portland-oregon')
def landing_usa_computer_science65():
    return render_template('landing_usa.html', data="computer science classes portland oregon")
@app.route('/computer-science-classes-queens-ny')
def landing_usa_computer_science66():
    return render_template('landing_usa.html', data="computer science classes queens ny")
@app.route('/computer-science-classes-cleveland-ohio')
def landing_usa_computer_science67():
    return render_template('landing_usa.html', data="computer science classes cleveland ohio")
@app.route('/computer-science-classes-bronx-ny')
def landing_usa_computer_science68():
    return render_template('landing_usa.html', data="computer science classes bronx ny")
@app.route('/computer-science-classes-tampa')
def landing_usa_computer_science69():
    return render_template('landing_usa.html', data="computer science classes tampa")

@app.route('/computer-science-classes-albany-ny')
def landing_usa_computer_science70():
    return render_template('landing_usa.html', data="computer science classes albany ny")
@app.route('/computer-science-classes-tampa-fl')
def landing_usa_computer_science71():
    return render_template('landing_usa.html', data="computer science classes tampa fl")
@app.route('/computer-science-classes-brooklyn-ny')
def landing_usa_computer_science72():
    return render_template('landing_usa.html', data="computer science classes brooklyn ny")
@app.route('/computer-science-classes-dc')
def landing_usa_computer_science73():
    return render_template('landing_usa.html', data="computer science classes dc")
@app.route('/computer-science-classes-knoxville-tne')
def landing_usa_computer_science74():
    return render_template('landing_usa.html', data="computer science classes knoxville tn")
@app.route('/computer-science-classes-raleigh-nc')
def landing_usa_computer_science75():
    return render_template('landing_usa.html', data="computer science classes raleigh nc")
@app.route('/computer-science-classes-staten-island-ny')
def landing_usa_computer_science76():
    return render_template('landing_usa.html', data="computer science classes staten island ny")
@app.route('/computer-science-classes-anchorage-ak')
def landing_usa_computer_science77():
    return render_template('landing_usa.html', data="computer science classes anchorage ak")
@app.route('/computer-science-classes-courses')
def landing_usa_computer_science78():
    return render_template('landing_usa.html', data="computer science classes courses")
@app.route('/computer-science-classes-sacramento-ca')
def landing_usa_computer_science79():
    return render_template('landing_usa.html', data="computer science classes sacramento ca")

@app.route('/computer-science-classes-chicago-il')
def landing_usa_computer_science80():
    return render_template('landing_usa.html', data="computer science classes chicago il")
@app.route('/computer-science-classes-miami-fl')
def landing_usa_computer_science81():
    return render_template('landing_usa.html', data="computer science classes miami fl")
@app.route('/computer-science-classes-bakersfield-ca')
def landing_usa_computer_science82():
    return render_template('landing_usa.html', data="computer science classes bakersfield ca")
@app.route('/computer-science-classes-baltimore-md')
def landing_usa_computer_science83():
    return render_template('landing_usa.html', data="computer science classes baltimore md")
@app.route('/computer-science-classes-new-york')
def landing_usa_computer_science84():
    return render_template('landing_usa.html', data="computer science classes new york")
@app.route('/computer-science-classes-las-vegas-nv')
def landing_usa_computer_science85():
    return render_template('landing_usa.html', data="computer science classes las vegas nv")
@app.route('/computer-science-classes-san-diego-ca')
def landing_usa_computer_science86():
    return render_template('landing_usa.html', data="computer science classes san diego ca")
@app.route('/computer-science-classes-austin-texas')
def landing_usa_computer_science87():
    return render_template('landing_usa.html', data="computer science classes austin texas")
@app.route('/computer-science-classes-milwaukee')
def landing_usa_computer_science88():
    return render_template('landing_usa.html', data="computer science classes milwaukee")
@app.route('/computer-science-classes-reno-nv')
def landing_usa_computer_science89():
    return render_template('landing_usa.html', data="computer science classes reno nv")

@app.route('/computer-science-classes-oklahoma-city')
def landing_usa_computer_science90():
    return render_template('landing_usa.html', data="computer science classes oklahoma city")
@app.route('/computer-science-classes-erie-pa')
def landing_usa_computer_science91():
    return render_template('landing_usa.html', data="computer science classes erie pa")
@app.route('/computer-science-classes-for-adults-in-chicago')
def landing_usa_computer_science92():
    return render_template('landing_usa.html', data="computer science classes for adults in chicago")
#############################################Coding Bootcamps USA#############
@app.route('/coding-boot-camp-nyc')
def landing_usa_coding_boot1():
    return render_template('landing_usa.html', data="coding boot camp nyc ")
@app.route('/coding-boot-camp-near-me')
def landing_usa_coding_boot2():
    return render_template('landing_usa.html', data="coding-boot camp near me ")
@app.route('/coding-boot-camp-in-chicago')
def landing_usa_coding_boot3():
    return render_template('landing_usa.html', data="coding boot camp in chicago ")
@app.route('/coding-boot-camp-in-philadelphia')
def landing_usa_coding_boot4():
    return render_template('landing_usa.html', data="coding boot camp in philadelphia ")
@app.route('/coding-boot-camp-in-houston')
def landing_usa_coding_boot5():
    return render_template('landing_usa.html', data="coding boot camp in houston")
@app.route('/coding-boot-camp-in-dc')
def landing_usa_coding_boot6():
    return render_template('landing_usa.html', data="coding boot camp in dc")
@app.route('/coding-boot-camp-for-adults-in-nyc')
def landing_usa_coding_boot7():
    return render_template('landing_usa.html', data="coding boot camp for adults in nyc")
@app.route('/coding-boot-camp-san-diego')
def landing_usa_coding_boot8():
    return render_template('landing_usa.html', data="coding boot camp san diego")
@app.route('/coding-boot-camp-nj')
def landing_usa_coding_boot9():
    return render_template('landing_usa.html', data="coding boot camp nj")

@app.route('/coding-boot-camp-in-las-vegas')
def landing_usa_coding_boot10():
    return render_template('landing_usa.html', data="coding boot camp in las vegas")
@app.route('/coding-boot-camp-in-nyc')
def landing_usa_coding_boot11():
    return render_template('landing_usa.html', data="coding boot camp in nyc")
@app.route('/coding-boot-camp-buffalo-ny')
def landing_usa_coding_boot12():
    return render_template('landing_usa.html', data="coding boot camp buffalo ny")
@app.route('/coding-boot-camp-los-angeles')
def landing_usa_coding_boot13():
    return render_template('landing_usa.html', data="coding boot camp los angeles")
@app.route('/coding-boot-camp-in-columbus-ohio')
def landing_usa_coding_boot14():
    return render_template('landing_usa.html', data="coding boot camp in columbus ohio")
@app.route('/coding-boot-camp-charlotte-nc')
def landing_usa_coding_boot15():
    return render_template('landing_usa.html', data="coding boot camp charlotte nc")
@app.route('/coding-boot-camp-philadelphia')
def landing_usa_coding_boot16():
    return render_template('landing_usa.html', data="coding boot camp philadelphia")
@app.route('/coding-boot-camp-seattle')
def landing_usa_coding_boot17():
    return render_template('landing_usa.html', data="coding boot camp seattle")
@app.route('/coding-boot-camp-minneapolis')
def landing_usa_coding_boot18():
    return render_template('landing_usa.html', data="coding boot camp minneapolis")
@app.route('/coding-boot-camp-austin')
def landing_usa_coding_boot19():
    return render_template('landing_usa.html', data="coding boot camp austin")

@app.route('/coding-boot-camp-in-dallas-tx')
def landing_usa_coding_boot20():
    return render_template('landing_usa.html', data="coding boot camp in dallas tx")
@app.route('/coding-boot-camp-in-ri')
def landing_usa_coding_boot21():
    return render_template('landing_usa.html', data="coding boot camp in ri")
@app.route('/coding-boot-camp-in-san-antonio-tx')
def landing_usa_coding_boot22():
    return render_template('landing_usa.html', data="coding boot camp in san antonio tx")
@app.route('/coding-boot-camp-new-orleans')
def landing_usa_coding_boot23():
    return render_template('landing_usa.html', data="coding boot camp new orleans")
@app.route('/coding-boot-camp-nashville-tn')
def landing_usa_coding_boot24():
    return render_template('landing_usa.html', data="coding boot camp nashville tn")
@app.route('/coding-boot-camp-boston')
def landing_usa_coding_boot25():
    return render_template('landing_usa.html', data="coding boot camp boston")
@app.route('/coding-boot-camp-denver')
def landing_usa_coding_boot26():
    return render_template('landing_usa.html', data="coding boot camp denver")
@app.route('/coding-boot-camp-brooklyn')
def landing_usa_coding_boot27():
    return render_template('landing_usa.html', data="coding boot camp brooklyn")
@app.route('/coding-boot-camp-miami')
def landing_usa_coding_boot28():
    return render_template('landing_usa.html', data="coding boot camp miami")
@app.route('/coding-boot-camp-houston')
def landing_usa_coding_boot29():
    return render_template('landing_usa.html', data="coding boot camp houston")

@app.route('/coding-boot-camp-bronx')
def landing_usa_coding_boot30():
    return render_template('landing_usa.html', data="coding boot camp bronx")
@app.route('/coding-boot-camp-birmingham-al')
def landing_usa_coding_boot31():
    return render_template('landing_usa.html', data="coding boot camp birmingham al")
@app.route('/coding-boot-camp-san-francisco')
def landing_usa_coding_boot32():
    return render_template('landing_usa.html', data="coding boot camp san francisco")
@app.route('/coding-boot-camp-in-atlanta-ga')
def landing_usa_coding_boot33():
    return render_template('landing_usa.html', data="coding boot camp in atlanta ga")
@app.route('/coding-boot-camp-phoenix-az')
def landing_usa_coding_boot34():
    return render_template('landing_usa.html', data="coding boot camp phoenix az")
@app.route('/coding-boot-camp-richmond-va')
def landing_usa_coding_boot35():
    return render_template('landing_usa.html', data="coding boot camp richmond va")
@app.route('/coding-boot-camp-mn')
def landing_usa_coding_boot36():
    return render_template('landing_usa.html', data="coding boot camp mn")
@app.route('/coding-boot-camp-san-antonio-tx')
def landing_usa_coding_boot37():
    return render_template('landing_usa.html', data="coding boot camp san antonio tx")
@app.route('/coding-boot-camp-sacramento')
def landing_usa_coding_boot38():
    return render_template('landing_usa.html', data="coding boot camp sacramento")
@app.route('/coding-boot-camp-austin-tx')
def landing_usa_coding_boot39():
    return render_template('landing_usa.html', data="coding boot camp austin tx")

@app.route('/coding-boot-camp-rochester-ny')
def landing_usa_coding_boot40():
    return render_template('landing_usa.html', data="coding boot camp rochester ny")
@app.route('/coding-boot-camp-in-charlotte-nc')
def landing_usa_coding_boot41():
    return render_template('landing_usa.html', data="coding boot camp in charlotte nc")
@app.route('/coding-boot-camp-tucson')
def landing_usa_coding_boot42():
    return render_template('landing_usa.html', data="coding boot camp tucson")
@app.route('/coding-boot-camp-pittsburgh')
def landing_usa_coding_boot43():
    return render_template('landing_usa.html', data="coding boot camp pittsburgh")
@app.route('/coding-boot-camp-in-detroit')
def landing_usa_coding_boot44():
    return render_template('landing_usa.html', data="coding boot camp in detroit")
@app.route('/coding-boot-camp-kansas-city')
def landing_usa_coding_boot45():
    return render_template('landing_usa.html', data="coding boot camp kansas city")
@app.route('/coding-boot-camp-detroit')
def landing_usa_coding_boot46():
    return render_template('landing_usa.html', data="coding boot camp detroit")
@app.route('/coding-boot-camp-on-the-internet')
def landing_usa_coding_boot47():
    return render_template('landing_usa.html', data=" ")
@app.route('/teach-english-abroad-without-tefl-or-degree')
def landing_usa_coding_boot48():
    return render_template('landing_usa.html', data="coding boot camp on the internet")
@app.route('/coding-boot-camp-mesa-az')
def landing_usa_coding_boot49():
    return render_template('landing_usa.html', data="coding boot camp mesa az")

@app.route('/coding-boot-camp-greenville-sc')
def landing_usa_coding_boot50():
    return render_template('landing_usa.html', data="coding boot camp greenville sc")
@app.route('/coding-boot-camp-washington-dc')
def landing_usa_coding_boot51():
    return render_template('landing_usa.html', data="coding boot camp washington dc")
@app.route('/coding-boot-camp-louisville-ky')
def landing_usa_coding_boot52():
    return render_template('landing_usa.html', data="coding boot camp louisville ky")
@app.route('/coding-boot-camp-in-knoxville-tn')
def landing_usa_coding_boot53():
    return render_template('landing_usa.html', data="coding boot camp in knoxville tn")
@app.route('/coding-boot-camp-colorado-springs')
def landing_usa_coding_boot54():
    return render_template('landing_usa.html', data="coding boot camp colorado springs")
@app.route('/coding-boot-camp-memphis-tn')
def landing_usa_coding_boot55():
    return render_template('landing_usa.html', data="coding boot camp memphis tn")
@app.route('/coding-boot-camp-phoenix')
def landing_usa_coding_boot56():
    return render_template('landing_usa.html', data="coding boot camp phoenix")
@app.route('/coding-boot-camp-boston-ma')
def landing_usa_coding_boot57():
    return render_template('landing_usa.html', data="coding boot camp boston ma")
@app.route('/coding-boot-camp-st-louis-mo')
def landing_usa_coding_boot58():
    return render_template('landing_usa.html', data="coding boot camp st louis mo")
@app.route('/coding-boot-camp-atlanta-ga')
def landing_usa_coding_boot59():
    return render_template('landing_usa.html', data="coding boot camp atlanta ga")

@app.route('/coding-boot-camp-baton-rouge')
def landing_usa_coding_boot60():
    return render_template('landing_usa.html', data="coding boot camp baton rouge")
@app.route('/coding-boot-camp-in-houston-texas')
def landing_usa_coding_boot61():
    return render_template('landing_usa.html', data="coding boot camp in houston texas")
@app.route('/coding-boot-camp-san-antonio')
def landing_usa_coding_boot62():
    return render_template('landing_usa.html', data="coding boot camp san antonio")
@app.route('/coding-boot-camp-houston-tx')
def landing_usa_coding_boot63():
    return render_template('landing_usa.html', data="coding boot camp houston tx")
@app.route('/coding-boot-camp-dallas-tx')
def landing_usa_coding_boot64():
    return render_template('landing_usa.html', data=" ")
@app.route('/coding-boot-camp-portland-oregon')
def landing_usa_coding_boot65():
    return render_template('landing_usa.html', data="coding boot camp portland oregon")
@app.route('/coding-boot-camp-queens-ny')
def landing_usa_coding_boot66():
    return render_template('landing_usa.html', data="coding boot camp queens ny")
@app.route('/coding-boot-camp-cleveland-ohio')
def landing_usa_coding_boot67():
    return render_template('landing_usa.html', data="coding boot camp cleveland ohio")
@app.route('/coding-boot-camp-bronx-ny')
def landing_usa_coding_boot68():
    return render_template('landing_usa.html', data="coding boot camp bronx ny")
@app.route('/coding-boot-camp-tampa')
def landing_usa_coding_boot69():
    return render_template('landing_usa.html', data="coding boot camp tampa")

@app.route('/coding-boot-camp-albany-ny')
def landing_usa_coding_boot70():
    return render_template('landing_usa.html', data="coding boot camp albany ny")
@app.route('/coding-boot-camp-tampa-fl')
def landing_usa_coding_boot71():
    return render_template('landing_usa.html', data="coding boot camp tampa fl")
@app.route('/coding-boot-camp-brooklyn-ny')
def landing_usa_coding_boot72():
    return render_template('landing_usa.html', data="coding boot camp brooklyn ny")
@app.route('/coding-boot-camp-dc')
def landing_usa_coding_boot73():
    return render_template('landing_usa.html', data="coding boot camp dc")
@app.route('/coding-boot-camp-knoxville-tne')
def landing_usa_coding_boot74():
    return render_template('landing_usa.html', data="coding boot camp knoxville tn")
@app.route('/coding-boot-camp-raleigh-nc')
def landing_usa_coding_boot75():
    return render_template('landing_usa.html', data="coding boot camp raleigh nc")
@app.route('/coding-boot-camp-staten-island-ny')
def landing_usa_coding_boot76():
    return render_template('landing_usa.html', data="coding boot camp staten island ny")
@app.route('/coding-boot-camp-anchorage-ak')
def landing_usa_coding_boot77():
    return render_template('landing_usa.html', data="coding boot camp anchorage ak")
@app.route('/coding-boot-camp-courses')
def landing_usa_coding_boot78():
    return render_template('landing_usa.html', data="coding boot camp courses")
@app.route('/coding-boot-camp-sacramento-ca')
def landing_usa_coding_boot79():
    return render_template('landing_usa.html', data="coding boot camp sacramento ca")

@app.route('/coding-boot-camp-chicago-il')
def landing_usa_coding_boot80():
    return render_template('landing_usa.html', data="coding boot camp chicago il")
@app.route('/coding-boot-camp-miami-fl')
def landing_usa_coding_boot81():
    return render_template('landing_usa.html', data="coding boot camp miami fl")
@app.route('/coding-boot-camp-bakersfield-ca')
def landing_usa_coding_boot82():
    return render_template('landing_usa.html', data="coding boot camp bakersfield ca")
@app.route('/coding-boot-camp-baltimore-md')
def landing_usa_coding_boot83():
    return render_template('landing_usa.html', data="coding boot camp baltimore md")
@app.route('/coding-boot-camp-new-york')
def landing_usa_coding_boot84():
    return render_template('landing_usa.html', data="coding boot camp new york")
@app.route('/coding-boot-camp-las-vegas-nv')
def landing_usa_coding_boot85():
    return render_template('landing_usa.html', data="coding boot camp las vegas nv")
@app.route('/coding-boot-camp-san-diego-ca')
def landing_usa_coding_boot86():
    return render_template('landing_usa.html', data="coding boot camp san diego ca")
@app.route('/coding-boot-camp-austin-texas')
def landing_usa_coding_boot87():
    return render_template('landing_usa.html', data="coding boot camp austin texas")
@app.route('/coding-boot-camp-milwaukee')
def landing_usa_coding_boot88():
    return render_template('landing_usa.html', data="coding boot camp milwaukee")
@app.route('/coding-boot-camp-reno-nv')
def landing_usa_coding_boot89():
    return render_template('landing_usa.html', data="coding boot camp reno nv")

@app.route('/coding-boot-camp-oklahoma-city')
def landing_usa_coding_boot90():
    return render_template('landing_usa.html', data="coding boot camp oklahoma city")
@app.route('/coding-boot-camp-erie-pa')
def landing_usa_coding_boot91():
    return render_template('landing_usa.html', data="coding boot camp erie pa")
@app.route('/coding-boot-camp-for-adults-in-chicago')
def landing_usa_coding_boot92():
    return render_template('landing_usa.html', data="coding boot camp for adults in chicago")

#####################pmp bootcamp #####################
@app.route('/pmp-boot-camp-nyc')
def landing_usa_pmp_boot1():
    return render_template('landing_usa.html', data="pmp boot camp nyc ")
@app.route('/pmp-boot-camp-near-me')
def landing_usa_pmp_boot2():
    return render_template('landing_usa.html', data="pmp-boot camp near me ")
@app.route('/pmp-boot-camp-in-chicago')
def landing_usa_pmp_boot3():
    return render_template('landing_usa.html', data="pmp boot camp in chicago ")
@app.route('/pmp-boot-camp-in-philadelphia')
def landing_usa_pmp_boot4():
    return render_template('landing_usa.html', data="pmp boot camp in philadelphia ")
@app.route('/pmp-boot-camp-in-houston')
def landing_usa_pmp_boot5():
    return render_template('landing_usa.html', data="pmp boot camp in houston")
@app.route('/pmp-boot-camp-in-dc')
def landing_usa_pmp_boot6():
    return render_template('landing_usa.html', data="pmp boot camp in dc")
@app.route('/pmp-boot-camp-for-adults-in-nyc')
def landing_usa_pmp_boot7():
    return render_template('landing_usa.html', data="pmp boot camp for adults in nyc")
@app.route('/pmp-boot-camp-san-diego')
def landing_usa_pmp_boot8():
    return render_template('landing_usa.html', data="pmp boot camp san diego")
@app.route('/pmp-boot-camp-nj')
def landing_usa_pmp_boot9():
    return render_template('landing_usa.html', data="pmp boot camp nj")

@app.route('/pmp-boot-camp-in-las-vegas')
def landing_usa_pmp_boot10():
    return render_template('landing_usa.html', data="pmp boot camp in las vegas")
@app.route('/pmp-boot-camp-in-nyc')
def landing_usa_pmp_boot11():
    return render_template('landing_usa.html', data="pmp boot camp in nyc")
@app.route('/pmp-boot-camp-buffalo-ny')
def landing_usa_pmp_boot12():
    return render_template('landing_usa.html', data="pmp boot camp buffalo ny")
@app.route('/pmp-boot-camp-los-angeles')
def landing_usa_pmp_boot13():
    return render_template('landing_usa.html', data="pmp boot camp los angeles")
@app.route('/pmp-boot-camp-in-columbus-ohio')
def landing_usa_pmp_boot14():
    return render_template('landing_usa.html', data="pmp boot camp in columbus ohio")
@app.route('/pmp-boot-camp-charlotte-nc')
def landing_usa_pmp_boot15():
    return render_template('landing_usa.html', data="pmp boot camp charlotte nc")
@app.route('/pmp-boot-camp-philadelphia')
def landing_usa_pmp_boot16():
    return render_template('landing_usa.html', data="pmp boot camp philadelphia")
@app.route('/pmp-boot-camp-seattle')
def landing_usa_pmp_boot17():
    return render_template('landing_usa.html', data="pmp boot camp seattle")
@app.route('/pmp-boot-camp-minneapolis')
def landing_usa_pmp_boot18():
    return render_template('landing_usa.html', data="pmp boot camp minneapolis")
@app.route('/pmp-boot-camp-austin')
def landing_usa_pmp_boot19():
    return render_template('landing_usa.html', data="pmp boot camp austin")

@app.route('/pmp-boot-camp-in-dallas-tx')
def landing_usa_pmp_boot20():
    return render_template('landing_usa.html', data="pmp boot camp in dallas tx")
@app.route('/pmp-boot-camp-in-ri')
def landing_usa_pmp_boot21():
    return render_template('landing_usa.html', data="pmp boot camp in ri")
@app.route('/pmp-boot-camp-in-san-antonio-tx')
def landing_usa_pmp_boot22():
    return render_template('landing_usa.html', data="pmp boot camp in san antonio tx")
@app.route('/pmp-boot-camp-new-orleans')
def landing_usa_pmp_boot23():
    return render_template('landing_usa.html', data="pmp boot camp new orleans")
@app.route('/pmp-boot-camp-nashville-tn')
def landing_usa_pmp_boot24():
    return render_template('landing_usa.html', data="pmp boot camp nashville tn")
@app.route('/pmp-boot-camp-boston')
def landing_usa_pmp_boot25():
    return render_template('landing_usa.html', data="pmp boot camp boston")
@app.route('/pmp-boot-camp-denver')
def landing_usa_pmp_boot26():
    return render_template('landing_usa.html', data="pmp boot camp denver")
@app.route('/pmp-boot-camp-brooklyn')
def landing_usa_pmp_boot27():
    return render_template('landing_usa.html', data="pmp boot camp brooklyn")
@app.route('/pmp-boot-camp-miami')
def landing_usa_pmp_boot28():
    return render_template('landing_usa.html', data="pmp boot camp miami")
@app.route('/pmp-boot-camp-houston')
def landing_usa_pmp_boot29():
    return render_template('landing_usa.html', data="pmp boot camp houston")

@app.route('/pmp-boot-camp-bronx')
def landing_usa_pmp_boot30():
    return render_template('landing_usa.html', data="pmp boot camp bronx")
@app.route('/pmp-boot-camp-birmingham-al')
def landing_usa_pmp_boot31():
    return render_template('landing_usa.html', data="pmp boot camp birmingham al")
@app.route('/pmp-boot-camp-san-francisco')
def landing_usa_pmp_boot32():
    return render_template('landing_usa.html', data="pmp boot camp san francisco")
@app.route('/pmp-boot-camp-in-atlanta-ga')
def landing_usa_pmp_boot33():
    return render_template('landing_usa.html', data="pmp boot camp in atlanta ga")
@app.route('/pmp-boot-camp-phoenix-az')
def landing_usa_pmp_boot34():
    return render_template('landing_usa.html', data="pmp boot camp phoenix az")
@app.route('/pmp-boot-camp-richmond-va')
def landing_usa_pmp_boot35():
    return render_template('landing_usa.html', data="pmp boot camp richmond va")
@app.route('/pmp-boot-camp-mn')
def landing_usa_pmp_boot36():
    return render_template('landing_usa.html', data="pmp boot camp mn")
@app.route('/pmp-boot-camp-san-antonio-tx')
def landing_usa_pmp_boot37():
    return render_template('landing_usa.html', data="pmp boot camp san antonio tx")
@app.route('/pmp-boot-camp-sacramento')
def landing_usa_pmp_boot38():
    return render_template('landing_usa.html', data="pmp boot camp sacramento")
@app.route('/pmp-boot-camp-austin-tx')
def landing_usa_pmp_boot39():
    return render_template('landing_usa.html', data="pmp boot camp austin tx")

@app.route('/pmp-boot-camp-rochester-ny')
def landing_usa_pmp_boot40():
    return render_template('landing_usa.html', data="pmp boot camp rochester ny")
@app.route('/pmp-boot-camp-in-charlotte-nc')
def landing_usa_pmp_boot41():
    return render_template('landing_usa.html', data="pmp boot camp in charlotte nc")
@app.route('/pmp-boot-camp-tucson')
def landing_usa_pmp_boot42():
    return render_template('landing_usa.html', data="pmp boot camp tucson")
@app.route('/pmp-boot-camp-pittsburgh')
def landing_usa_pmp_boot43():
    return render_template('landing_usa.html', data="pmp boot camp pittsburgh")
@app.route('/pmp-boot-camp-in-detroit')
def landing_usa_pmp_boot44():
    return render_template('landing_usa.html', data="pmp boot camp in detroit")
@app.route('/pmp-boot-camp-kansas-city')
def landing_usa_pmp_boot45():
    return render_template('landing_usa.html', data="pmp boot camp kansas city")
@app.route('/pmp-boot-camp-detroit')
def landing_usa_pmp_boot46():
    return render_template('landing_usa.html', data="pmp boot camp detroit")
@app.route('/pmp-boot-camp-on-the-internet')
def landing_usa_pmp_boot47():
    return render_template('landing_usa.html', data=" ")
@app.route('/teach-english-abroad-without-tefl-or-degree')
def landing_usa_pmp_boot48():
    return render_template('landing_usa.html', data="pmp boot camp on the internet")
@app.route('/pmp-boot-camp-mesa-az')
def landing_usa_pmp_boot49():
    return render_template('landing_usa.html', data="pmp boot camp mesa az")

@app.route('/pmp-boot-camp-greenville-sc')
def landing_usa_pmp_boot50():
    return render_template('landing_usa.html', data="pmp boot camp greenville sc")
@app.route('/pmp-boot-camp-washington-dc')
def landing_usa_pmp_boot51():
    return render_template('landing_usa.html', data="pmp boot camp washington dc")
@app.route('/pmp-boot-camp-louisville-ky')
def landing_usa_pmp_boot52():
    return render_template('landing_usa.html', data="pmp boot camp louisville ky")
@app.route('/pmp-boot-camp-in-knoxville-tn')
def landing_usa_pmp_boot53():
    return render_template('landing_usa.html', data="pmp boot camp in knoxville tn")
@app.route('/pmp-boot-camp-colorado-springs')
def landing_usa_pmp_boot54():
    return render_template('landing_usa.html', data="pmp boot camp colorado springs")
@app.route('/pmp-boot-camp-memphis-tn')
def landing_usa_pmp_boot55():
    return render_template('landing_usa.html', data="pmp boot camp memphis tn")
@app.route('/pmp-boot-camp-phoenix')
def landing_usa_pmp_boot56():
    return render_template('landing_usa.html', data="pmp boot camp phoenix")
@app.route('/pmp-boot-camp-boston-ma')
def landing_usa_pmp_boot57():
    return render_template('landing_usa.html', data="pmp boot camp boston ma")
@app.route('/pmp-boot-camp-st-louis-mo')
def landing_usa_pmp_boot58():
    return render_template('landing_usa.html', data="pmp boot camp st louis mo")
@app.route('/pmp-boot-camp-atlanta-ga')
def landing_usa_pmp_boot59():
    return render_template('landing_usa.html', data="pmp boot camp atlanta ga")

@app.route('/pmp-boot-camp-baton-rouge')
def landing_usa_pmp_boot60():
    return render_template('landing_usa.html', data="pmp boot camp baton rouge")
@app.route('/pmp-boot-camp-in-houston-texas')
def landing_usa_pmp_boot61():
    return render_template('landing_usa.html', data="pmp boot camp in houston texas")
@app.route('/pmp-boot-camp-san-antonio')
def landing_usa_pmp_boot62():
    return render_template('landing_usa.html', data="pmp boot camp san antonio")
@app.route('/pmp-boot-camp-houston-tx')
def landing_usa_pmp_boot63():
    return render_template('landing_usa.html', data="pmp boot camp houston tx")
@app.route('/pmp-boot-camp-dallas-tx')
def landing_usa_pmp_boot64():
    return render_template('landing_usa.html', data=" ")
@app.route('/pmp-boot-camp-portland-oregon')
def landing_usa_pmp_boot65():
    return render_template('landing_usa.html', data="pmp boot camp portland oregon")
@app.route('/pmp-boot-camp-queens-ny')
def landing_usa_pmp_boot66():
    return render_template('landing_usa.html', data="pmp boot camp queens ny")
@app.route('/pmp-boot-camp-cleveland-ohio')
def landing_usa_pmp_boot67():
    return render_template('landing_usa.html', data="pmp boot camp cleveland ohio")
@app.route('/pmp-boot-camp-bronx-ny')
def landing_usa_pmp_boot68():
    return render_template('landing_usa.html', data="pmp boot camp bronx ny")
@app.route('/pmp-boot-camp-tampa')
def landing_usa_pmp_boot69():
    return render_template('landing_usa.html', data="pmp boot camp tampa")

@app.route('/pmp-boot-camp-albany-ny')
def landing_usa_pmp_boot70():
    return render_template('landing_usa.html', data="pmp boot camp albany ny")
@app.route('/pmp-boot-camp-tampa-fl')
def landing_usa_pmp_boot71():
    return render_template('landing_usa.html', data="pmp boot camp tampa fl")
@app.route('/pmp-boot-camp-brooklyn-ny')
def landing_usa_pmp_boot72():
    return render_template('landing_usa.html', data="pmp boot camp brooklyn ny")
@app.route('/pmp-boot-camp-dc')
def landing_usa_pmp_boot73():
    return render_template('landing_usa.html', data="pmp boot camp dc")
@app.route('/pmp-boot-camp-knoxville-tne')
def landing_usa_pmp_boot74():
    return render_template('landing_usa.html', data="pmp boot camp knoxville tn")
@app.route('/pmp-boot-camp-raleigh-nc')
def landing_usa_pmp_boot75():
    return render_template('landing_usa.html', data="pmp boot camp raleigh nc")
@app.route('/pmp-boot-camp-staten-island-ny')
def landing_usa_pmp_boot76():
    return render_template('landing_usa.html', data="pmp boot camp staten island ny")
@app.route('/pmp-boot-camp-anchorage-ak')
def landing_usa_pmp_boot77():
    return render_template('landing_usa.html', data="pmp boot camp anchorage ak")
@app.route('/pmp-boot-camp-courses')
def landing_usa_pmp_boot78():
    return render_template('landing_usa.html', data="pmp boot camp courses")
@app.route('/pmp-boot-camp-sacramento-ca')
def landing_usa_pmp_boot79():
    return render_template('landing_usa.html', data="pmp boot camp sacramento ca")

@app.route('/pmp-boot-camp-chicago-il')
def landing_usa_pmp_boot80():
    return render_template('landing_usa.html', data="pmp boot camp chicago il")
@app.route('/pmp-boot-camp-miami-fl')
def landing_usa_pmp_boot81():
    return render_template('landing_usa.html', data="pmp boot camp miami fl")
@app.route('/pmp-boot-camp-bakersfield-ca')
def landing_usa_pmp_boot82():
    return render_template('landing_usa.html', data="pmp boot camp bakersfield ca")
@app.route('/pmp-boot-camp-baltimore-md')
def landing_usa_pmp_boot83():
    return render_template('landing_usa.html', data="pmp boot camp baltimore md")
@app.route('/pmp-boot-camp-new-york')
def landing_usa_pmp_boot84():
    return render_template('landing_usa.html', data="pmp boot camp new york")
@app.route('/pmp-boot-camp-las-vegas-nv')
def landing_usa_pmp_boot85():
    return render_template('landing_usa.html', data="pmp boot camp las vegas nv")
@app.route('/pmp-boot-camp-san-diego-ca')
def landing_usa_pmp_boot86():
    return render_template('landing_usa.html', data="pmp boot camp san diego ca")
@app.route('/pmp-boot-camp-austin-texas')
def landing_usa_pmp_boot87():
    return render_template('landing_usa.html', data="pmp boot camp austin texas")
@app.route('/pmp-boot-camp-milwaukee')
def landing_usa_pmp_boot88():
    return render_template('landing_usa.html', data="pmp boot camp milwaukee")
@app.route('/pmp-boot-camp-reno-nv')
def landing_usa_pmp_boot89():
    return render_template('landing_usa.html', data="pmp boot camp reno nv")

@app.route('/pmp-boot-camp-oklahoma-city')
def landing_usa_pmp_boot90():
    return render_template('landing_usa.html', data="pmp boot camp oklahoma city")
@app.route('/pmp-boot-camp-erie-pa')
def landing_usa_pmp_boot91():
    return render_template('landing_usa.html', data="pmp boot camp erie pa")
@app.route('/pmp-boot-camp-for-adults-in-chicago')
def landing_usa_pmp_boot92():
    return render_template('landing_usa.html', data="pmp boot camp for adults in chicago")


#################################################################Whole foods cooking classes ###############
@app.route('/whole-foods-cooking-classes-nyc')
def landing_usa_whole_foods_cooking1():
    return render_template('landing_usa.html', data="whole foods cooking classes nyc ")
@app.route('/whole-foods-cooking-classes-near-me')
def landing_usa_whole_foods_cooking2():
    return render_template('landing_usa.html', data="whole-foods-cooking classes near me ")
@app.route('/whole-foods-cooking-classes-in-chicago')
def landing_usa_whole_foods_cooking3():
    return render_template('landing_usa.html', data="whole foods cooking classes in chicago ")
@app.route('/whole-foods-cooking-classes-in-philadelphia')
def landing_usa_whole_foods_cooking4():
    return render_template('landing_usa.html', data="whole foods cooking classes in philadelphia ")
@app.route('/whole-foods-cooking-classes-in-houston')
def landing_usa_whole_foods_cooking5():
    return render_template('landing_usa.html', data="whole foods cooking classes in houston")
@app.route('/whole-foods-cooking-classes-in-dc')
def landing_usa_whole_foods_cooking6():
    return render_template('landing_usa.html', data="whole foods cooking classes in dc")
@app.route('/whole-foods-cooking-classes-for-adults-in-nyc')
def landing_usa_whole_foods_cooking7():
    return render_template('landing_usa.html', data="whole foods cooking classes for adults in nyc")
@app.route('/whole-foods-cooking-classes-san-diego')
def landing_usa_whole_foods_cooking8():
    return render_template('landing_usa.html', data="whole foods cooking classes san diego")
@app.route('/whole-foods-cooking-classes-nj')
def landing_usa_whole_foods_cooking9():
    return render_template('landing_usa.html', data="whole foods cooking classes nj")

@app.route('/whole-foods-cooking-classes-in-las-vegas')
def landing_usa_whole_foods_cooking10():
    return render_template('landing_usa.html', data="whole foods cooking classes in las vegas")
@app.route('/whole-foods-cooking-classes-in-nyc')
def landing_usa_whole_foods_cooking11():
    return render_template('landing_usa.html', data="whole foods cooking classes in nyc")
@app.route('/whole-foods-cooking-classes-buffalo-ny')
def landing_usa_whole_foods_cooking12():
    return render_template('landing_usa.html', data="whole foods cooking classes buffalo ny")
@app.route('/whole-foods-cooking-classes-los-angeles')
def landing_usa_whole_foods_cooking13():
    return render_template('landing_usa.html', data="whole foods cooking classes los angeles")
@app.route('/whole-foods-cooking-classes-in-columbus-ohio')
def landing_usa_whole_foods_cooking14():
    return render_template('landing_usa.html', data="whole foods cooking classes in columbus ohio")
@app.route('/whole-foods-cooking-classes-charlotte-nc')
def landing_usa_whole_foods_cooking15():
    return render_template('landing_usa.html', data="whole foods cooking classes charlotte nc")
@app.route('/whole-foods-cooking-classes-philadelphia')
def landing_usa_whole_foods_cooking16():
    return render_template('landing_usa.html', data="whole foods cooking classes philadelphia")
@app.route('/whole-foods-cooking-classes-seattle')
def landing_usa_whole_foods_cooking17():
    return render_template('landing_usa.html', data="whole foods cooking classes seattle")
@app.route('/whole-foods-cooking-classes-minneapolis')
def landing_usa_whole_foods_cooking18():
    return render_template('landing_usa.html', data="whole foods cooking classes minneapolis")
@app.route('/whole-foods-cooking-classes-austin')
def landing_usa_whole_foods_cooking19():
    return render_template('landing_usa.html', data="whole foods cooking classes austin")

@app.route('/whole-foods-cooking-classes-in-dallas-tx')
def landing_usa_whole_foods_cooking20():
    return render_template('landing_usa.html', data="whole foods cooking classes in dallas tx")
@app.route('/whole-foods-cooking-classes-in-ri')
def landing_usa_whole_foods_cooking21():
    return render_template('landing_usa.html', data="whole foods cooking classes in ri")
@app.route('/whole-foods-cooking-classes-in-san-antonio-tx')
def landing_usa_whole_foods_cooking22():
    return render_template('landing_usa.html', data="whole foods cooking classes in san antonio tx")
@app.route('/whole-foods-cooking-classes-new-orleans')
def landing_usa_whole_foods_cooking23():
    return render_template('landing_usa.html', data="whole foods cooking classes new orleans")
@app.route('/whole-foods-cooking-classes-nashville-tn')
def landing_usa_whole_foods_cooking24():
    return render_template('landing_usa.html', data="whole foods cooking classes nashville tn")
@app.route('/whole-foods-cooking-classes-boston')
def landing_usa_whole_foods_cooking25():
    return render_template('landing_usa.html', data="whole foods cooking classes boston")
@app.route('/whole-foods-cooking-classes-denver')
def landing_usa_whole_foods_cooking26():
    return render_template('landing_usa.html', data="whole foods cooking classes denver")
@app.route('/whole-foods-cooking-classes-brooklyn')
def landing_usa_whole_foods_cooking27():
    return render_template('landing_usa.html', data="whole foods cooking classes brooklyn")
@app.route('/whole-foods-cooking-classes-miami')
def landing_usa_whole_foods_cooking28():
    return render_template('landing_usa.html', data="whole foods cooking classes miami")
@app.route('/whole-foods-cooking-classes-houston')
def landing_usa_whole_foods_cooking29():
    return render_template('landing_usa.html', data="whole foods cooking classes houston")

@app.route('/whole-foods-cooking-classes-bronx')
def landing_usa_whole_foods_cooking30():
    return render_template('landing_usa.html', data="whole foods cooking classes bronx")
@app.route('/whole-foods-cooking-classes-birmingham-al')
def landing_usa_whole_foods_cooking31():
    return render_template('landing_usa.html', data="whole foods cooking classes birmingham al")
@app.route('/whole-foods-cooking-classes-san-francisco')
def landing_usa_whole_foods_cooking32():
    return render_template('landing_usa.html', data="whole foods cooking classes san francisco")
@app.route('/whole-foods-cooking-classes-in-atlanta-ga')
def landing_usa_whole_foods_cooking33():
    return render_template('landing_usa.html', data="whole foods cooking classes in atlanta ga")
@app.route('/whole-foods-cooking-classes-phoenix-az')
def landing_usa_whole_foods_cooking34():
    return render_template('landing_usa.html', data="whole foods cooking classes phoenix az")
@app.route('/whole-foods-cooking-classes-richmond-va')
def landing_usa_whole_foods_cooking35():
    return render_template('landing_usa.html', data="whole foods cooking classes richmond va")
@app.route('/whole-foods-cooking-classes-mn')
def landing_usa_whole_foods_cooking36():
    return render_template('landing_usa.html', data="whole foods cooking classes mn")
@app.route('/whole-foods-cooking-classes-san-antonio-tx')
def landing_usa_whole_foods_cooking37():
    return render_template('landing_usa.html', data="whole foods cooking classes san antonio tx")
@app.route('/whole-foods-cooking-classes-sacramento')
def landing_usa_whole_foods_cooking38():
    return render_template('landing_usa.html', data="whole foods cooking classes sacramento")
@app.route('/whole-foods-cooking-classes-austin-tx')
def landing_usa_whole_foods_cooking39():
    return render_template('landing_usa.html', data="whole foods cooking classes austin tx")

@app.route('/whole-foods-cooking-classes-rochester-ny')
def landing_usa_whole_foods_cooking40():
    return render_template('landing_usa.html', data="whole foods cooking classes rochester ny")
@app.route('/whole-foods-cooking-classes-in-charlotte-nc')
def landing_usa_whole_foods_cooking41():
    return render_template('landing_usa.html', data="whole foods cooking classes in charlotte nc")
@app.route('/whole-foods-cooking-classes-tucson')
def landing_usa_whole_foods_cooking42():
    return render_template('landing_usa.html', data="whole foods cooking classes tucson")
@app.route('/whole-foods-cooking-classes-pittsburgh')
def landing_usa_whole_foods_cooking43():
    return render_template('landing_usa.html', data="whole foods cooking classes pittsburgh")
@app.route('/whole-foods-cooking-classes-in-detroit')
def landing_usa_whole_foods_cooking44():
    return render_template('landing_usa.html', data="whole foods cooking classes in detroit")
@app.route('/whole-foods-cooking-classes-kansas-city')
def landing_usa_whole_foods_cooking45():
    return render_template('landing_usa.html', data="whole foods cooking classes kansas city")
@app.route('/whole-foods-cooking-classes-detroit')
def landing_usa_whole_foods_cooking46():
    return render_template('landing_usa.html', data="whole foods cooking classes detroit")
@app.route('/whole-foods-cooking-classes-on-the-internet')
def landing_usa_whole_foods_cooking47():
    return render_template('landing_usa.html', data=" ")
@app.route('/teach-english-abroad-without-tefl-or-degree')
def landing_usa_whole_foods_cooking48():
    return render_template('landing_usa.html', data="whole foods cooking classes on the internet")
@app.route('/whole-foods-cooking-classes-mesa-az')
def landing_usa_whole_foods_cooking49():
    return render_template('landing_usa.html', data="whole foods cooking classes mesa az")

@app.route('/whole-foods-cooking-classes-greenville-sc')
def landing_usa_whole_foods_cooking50():
    return render_template('landing_usa.html', data="whole foods cooking classes greenville sc")
@app.route('/whole-foods-cooking-classes-washington-dc')
def landing_usa_whole_foods_cooking51():
    return render_template('landing_usa.html', data="whole foods cooking classes washington dc")
@app.route('/whole-foods-cooking-classes-louisville-ky')
def landing_usa_whole_foods_cooking52():
    return render_template('landing_usa.html', data="whole foods cooking classes louisville ky")
@app.route('/whole-foods-cooking-classes-in-knoxville-tn')
def landing_usa_whole_foods_cooking53():
    return render_template('landing_usa.html', data="whole foods cooking classes in knoxville tn")
@app.route('/whole-foods-cooking-classes-colorado-springs')
def landing_usa_whole_foods_cooking54():
    return render_template('landing_usa.html', data="whole foods cooking classes colorado springs")
@app.route('/whole-foods-cooking-classes-memphis-tn')
def landing_usa_whole_foods_cooking55():
    return render_template('landing_usa.html', data="whole foods cooking classes memphis tn")
@app.route('/whole-foods-cooking-classes-phoenix')
def landing_usa_whole_foods_cooking56():
    return render_template('landing_usa.html', data="whole foods cooking classes phoenix")
@app.route('/whole-foods-cooking-classes-boston-ma')
def landing_usa_whole_foods_cooking57():
    return render_template('landing_usa.html', data="whole foods cooking classes boston ma")
@app.route('/whole-foods-cooking-classes-st-louis-mo')
def landing_usa_whole_foods_cooking58():
    return render_template('landing_usa.html', data="whole foods cooking classes st louis mo")
@app.route('/whole-foods-cooking-classes-atlanta-ga')
def landing_usa_whole_foods_cooking59():
    return render_template('landing_usa.html', data="whole foods cooking classes atlanta ga")

@app.route('/whole-foods-cooking-classes-baton-rouge')
def landing_usa_whole_foods_cooking60():
    return render_template('landing_usa.html', data="whole foods cooking classes baton rouge")
@app.route('/whole-foods-cooking-classes-in-houston-texas')
def landing_usa_whole_foods_cooking61():
    return render_template('landing_usa.html', data="whole foods cooking classes in houston texas")
@app.route('/whole-foods-cooking-classes-san-antonio')
def landing_usa_whole_foods_cooking62():
    return render_template('landing_usa.html', data="whole foods cooking classes san antonio")
@app.route('/whole-foods-cooking-classes-houston-tx')
def landing_usa_whole_foods_cooking63():
    return render_template('landing_usa.html', data="whole foods cooking classes houston tx")
@app.route('/whole-foods-cooking-classes-dallas-tx')
def landing_usa_whole_foods_cooking64():
    return render_template('landing_usa.html', data=" ")
@app.route('/whole-foods-cooking-classes-portland-oregon')
def landing_usa_whole_foods_cooking65():
    return render_template('landing_usa.html', data="whole foods cooking classes portland oregon")
@app.route('/whole-foods-cooking-classes-queens-ny')
def landing_usa_whole_foods_cooking66():
    return render_template('landing_usa.html', data="whole foods cooking classes queens ny")
@app.route('/whole-foods-cooking-classes-cleveland-ohio')
def landing_usa_whole_foods_cooking67():
    return render_template('landing_usa.html', data="whole foods cooking classes cleveland ohio")
@app.route('/whole-foods-cooking-classes-bronx-ny')
def landing_usa_whole_foods_cooking68():
    return render_template('landing_usa.html', data="whole foods cooking classes bronx ny")
@app.route('/whole-foods-cooking-classes-tampa')
def landing_usa_whole_foods_cooking69():
    return render_template('landing_usa.html', data="whole foods cooking classes tampa")

@app.route('/whole-foods-cooking-classes-albany-ny')
def landing_usa_whole_foods_cooking70():
    return render_template('landing_usa.html', data="whole foods cooking classes albany ny")
@app.route('/whole-foods-cooking-classes-tampa-fl')
def landing_usa_whole_foods_cooking71():
    return render_template('landing_usa.html', data="whole foods cooking classes tampa fl")
@app.route('/whole-foods-cooking-classes-brooklyn-ny')
def landing_usa_whole_foods_cooking72():
    return render_template('landing_usa.html', data="whole foods cooking classes brooklyn ny")
@app.route('/whole-foods-cooking-classes-dc')
def landing_usa_whole_foods_cooking73():
    return render_template('landing_usa.html', data="whole foods cooking classes dc")
@app.route('/whole-foods-cooking-classes-knoxville-tne')
def landing_usa_whole_foods_cooking74():
    return render_template('landing_usa.html', data="whole foods cooking classes knoxville tn")
@app.route('/whole-foods-cooking-classes-raleigh-nc')
def landing_usa_whole_foods_cooking75():
    return render_template('landing_usa.html', data="whole foods cooking classes raleigh nc")
@app.route('/whole-foods-cooking-classes-staten-island-ny')
def landing_usa_whole_foods_cooking76():
    return render_template('landing_usa.html', data="whole foods cooking classes staten island ny")
@app.route('/whole-foods-cooking-classes-anchorage-ak')
def landing_usa_whole_foods_cooking77():
    return render_template('landing_usa.html', data="whole foods cooking classes anchorage ak")
@app.route('/whole-foods-cooking-classes-courses')
def landing_usa_whole_foods_cooking78():
    return render_template('landing_usa.html', data="whole foods cooking classes courses")
@app.route('/whole-foods-cooking-classes-sacramento-ca')
def landing_usa_whole_foods_cooking79():
    return render_template('landing_usa.html', data="whole foods cooking classes sacramento ca")

@app.route('/whole-foods-cooking-classes-chicago-il')
def landing_usa_whole_foods_cooking80():
    return render_template('landing_usa.html', data="whole foods cooking classes chicago il")
@app.route('/whole-foods-cooking-classes-miami-fl')
def landing_usa_whole_foods_cooking81():
    return render_template('landing_usa.html', data="whole foods cooking classes miami fl")
@app.route('/whole-foods-cooking-classes-bakersfield-ca')
def landing_usa_whole_foods_cooking82():
    return render_template('landing_usa.html', data="whole foods cooking classes bakersfield ca")
@app.route('/whole-foods-cooking-classes-baltimore-md')
def landing_usa_whole_foods_cooking83():
    return render_template('landing_usa.html', data="whole foods cooking classes baltimore md")
@app.route('/whole-foods-cooking-classes-new-york')
def landing_usa_whole_foods_cooking84():
    return render_template('landing_usa.html', data="whole foods cooking classes new york")
@app.route('/whole-foods-cooking-classes-las-vegas-nv')
def landing_usa_whole_foods_cooking85():
    return render_template('landing_usa.html', data="whole foods cooking classes las vegas nv")
@app.route('/whole-foods-cooking-classes-san-diego-ca')
def landing_usa_whole_foods_cooking86():
    return render_template('landing_usa.html', data="whole foods cooking classes san diego ca")
@app.route('/whole-foods-cooking-classes-austin-texas')
def landing_usa_whole_foods_cooking87():
    return render_template('landing_usa.html', data="whole foods cooking classes austin texas")
@app.route('/whole-foods-cooking-classes-milwaukee')
def landing_usa_whole_foods_cooking88():
    return render_template('landing_usa.html', data="whole foods cooking classes milwaukee")
@app.route('/whole-foods-cooking-classes-reno-nv')
def landing_usa_whole_foods_cooking89():
    return render_template('landing_usa.html', data="whole foods cooking classes reno nv")

@app.route('/whole-foods-cooking-classes-oklahoma-city')
def landing_usa_whole_foods_cooking90():
    return render_template('landing_usa.html', data="whole foods cooking classes oklahoma city")
@app.route('/whole-foods-cooking-classes-erie-pa')
def landing_usa_whole_foods_cooking91():
    return render_template('landing_usa.html', data="whole foods cooking classes erie pa")
@app.route('/whole-foods-cooking-classes-for-adults-in-chicago')
def landing_usa_whole_foods_cooking92():
    return render_template('landing_usa.html', data="whole foods cooking classes for adults in chicago")

####################################################################Italian Cooking Classes###############
@app.route('/italian-cooking-classes-nyc')
def landing_usa_italian_cooking1():
    return render_template('landing_usa.html', data="italian cooking classes nyc ")
@app.route('/italian-cooking-classes-near-me')
def landing_usa_italian_cooking2():
    return render_template('landing_usa.html', data="italian-cooking classes near me ")
@app.route('/italian-cooking-classes-in-chicago')
def landing_usa_italian_cooking3():
    return render_template('landing_usa.html', data="italian cooking classes in chicago ")
@app.route('/italian-cooking-classes-in-philadelphia')
def landing_usa_italian_cooking4():
    return render_template('landing_usa.html', data="italian cooking classes in philadelphia ")
@app.route('/italian-cooking-classes-in-houston')
def landing_usa_italian_cooking5():
    return render_template('landing_usa.html', data="italian cooking classes in houston")
@app.route('/italian-cooking-classes-in-dc')
def landing_usa_italian_cooking6():
    return render_template('landing_usa.html', data="italian cooking classes in dc")
@app.route('/italian-cooking-classes-for-adults-in-nyc')
def landing_usa_italian_cooking7():
    return render_template('landing_usa.html', data="italian cooking classes for adults in nyc")
@app.route('/italian-cooking-classes-san-diego')
def landing_usa_italian_cooking8():
    return render_template('landing_usa.html', data="italian cooking classes san diego")
@app.route('/italian-cooking-classes-nj')
def landing_usa_italian_cooking9():
    return render_template('landing_usa.html', data="italian cooking classes nj")

@app.route('/italian-cooking-classes-in-las-vegas')
def landing_usa_italian_cooking10():
    return render_template('landing_usa.html', data="italian cooking classes in las vegas")
@app.route('/italian-cooking-classes-in-nyc')
def landing_usa_italian_cooking11():
    return render_template('landing_usa.html', data="italian cooking classes in nyc")
@app.route('/italian-cooking-classes-buffalo-ny')
def landing_usa_italian_cooking12():
    return render_template('landing_usa.html', data="italian cooking classes buffalo ny")
@app.route('/italian-cooking-classes-los-angeles')
def landing_usa_italian_cooking13():
    return render_template('landing_usa.html', data="italian cooking classes los angeles")
@app.route('/italian-cooking-classes-in-columbus-ohio')
def landing_usa_italian_cooking14():
    return render_template('landing_usa.html', data="italian cooking classes in columbus ohio")
@app.route('/italian-cooking-classes-charlotte-nc')
def landing_usa_italian_cooking15():
    return render_template('landing_usa.html', data="italian cooking classes charlotte nc")
@app.route('/italian-cooking-classes-philadelphia')
def landing_usa_italian_cooking16():
    return render_template('landing_usa.html', data="italian cooking classes philadelphia")
@app.route('/italian-cooking-classes-seattle')
def landing_usa_italian_cooking17():
    return render_template('landing_usa.html', data="italian cooking classes seattle")
@app.route('/italian-cooking-classes-minneapolis')
def landing_usa_italian_cooking18():
    return render_template('landing_usa.html', data="italian cooking classes minneapolis")
@app.route('/italian-cooking-classes-austin')
def landing_usa_italian_cooking19():
    return render_template('landing_usa.html', data="italian cooking classes austin")

@app.route('/italian-cooking-classes-in-dallas-tx')
def landing_usa_italian_cooking20():
    return render_template('landing_usa.html', data="italian cooking classes in dallas tx")
@app.route('/italian-cooking-classes-in-ri')
def landing_usa_italian_cooking21():
    return render_template('landing_usa.html', data="italian cooking classes in ri")
@app.route('/italian-cooking-classes-in-san-antonio-tx')
def landing_usa_italian_cooking22():
    return render_template('landing_usa.html', data="italian cooking classes in san antonio tx")
@app.route('/italian-cooking-classes-new-orleans')
def landing_usa_italian_cooking23():
    return render_template('landing_usa.html', data="italian cooking classes new orleans")
@app.route('/italian-cooking-classes-nashville-tn')
def landing_usa_italian_cooking24():
    return render_template('landing_usa.html', data="italian cooking classes nashville tn")
@app.route('/italian-cooking-classes-boston')
def landing_usa_italian_cooking25():
    return render_template('landing_usa.html', data="italian cooking classes boston")
@app.route('/italian-cooking-classes-denver')
def landing_usa_italian_cooking26():
    return render_template('landing_usa.html', data="italian cooking classes denver")
@app.route('/italian-cooking-classes-brooklyn')
def landing_usa_italian_cooking27():
    return render_template('landing_usa.html', data="italian cooking classes brooklyn")
@app.route('/italian-cooking-classes-miami')
def landing_usa_italian_cooking28():
    return render_template('landing_usa.html', data="italian cooking classes miami")
@app.route('/italian-cooking-classes-houston')
def landing_usa_italian_cooking29():
    return render_template('landing_usa.html', data="italian cooking classes houston")

@app.route('/italian-cooking-classes-bronx')
def landing_usa_italian_cooking30():
    return render_template('landing_usa.html', data="italian cooking classes bronx")
@app.route('/italian-cooking-classes-birmingham-al')
def landing_usa_italian_cooking31():
    return render_template('landing_usa.html', data="italian cooking classes birmingham al")
@app.route('/italian-cooking-classes-san-francisco')
def landing_usa_italian_cooking32():
    return render_template('landing_usa.html', data="italian cooking classes san francisco")
@app.route('/italian-cooking-classes-in-atlanta-ga')
def landing_usa_italian_cooking33():
    return render_template('landing_usa.html', data="italian cooking classes in atlanta ga")
@app.route('/italian-cooking-classes-phoenix-az')
def landing_usa_italian_cooking34():
    return render_template('landing_usa.html', data="italian cooking classes phoenix az")
@app.route('/italian-cooking-classes-richmond-va')
def landing_usa_italian_cooking35():
    return render_template('landing_usa.html', data="italian cooking classes richmond va")
@app.route('/italian-cooking-classes-mn')
def landing_usa_italian_cooking36():
    return render_template('landing_usa.html', data="italian cooking classes mn")
@app.route('/italian-cooking-classes-san-antonio-tx')
def landing_usa_italian_cooking37():
    return render_template('landing_usa.html', data="italian cooking classes san antonio tx")
@app.route('/italian-cooking-classes-sacramento')
def landing_usa_italian_cooking38():
    return render_template('landing_usa.html', data="italian cooking classes sacramento")
@app.route('/italian-cooking-classes-austin-tx')
def landing_usa_italian_cooking39():
    return render_template('landing_usa.html', data="italian cooking classes austin tx")

@app.route('/italian-cooking-classes-rochester-ny')
def landing_usa_italian_cooking40():
    return render_template('landing_usa.html', data="italian cooking classes rochester ny")
@app.route('/italian-cooking-classes-in-charlotte-nc')
def landing_usa_italian_cooking41():
    return render_template('landing_usa.html', data="italian cooking classes in charlotte nc")
@app.route('/italian-cooking-classes-tucson')
def landing_usa_italian_cooking42():
    return render_template('landing_usa.html', data="italian cooking classes tucson")
@app.route('/italian-cooking-classes-pittsburgh')
def landing_usa_italian_cooking43():
    return render_template('landing_usa.html', data="italian cooking classes pittsburgh")
@app.route('/italian-cooking-classes-in-detroit')
def landing_usa_italian_cooking44():
    return render_template('landing_usa.html', data="italian cooking classes in detroit")
@app.route('/italian-cooking-classes-kansas-city')
def landing_usa_italian_cooking45():
    return render_template('landing_usa.html', data="italian cooking classes kansas city")
@app.route('/italian-cooking-classes-detroit')
def landing_usa_italian_cooking46():
    return render_template('landing_usa.html', data="italian cooking classes detroit")
@app.route('/italian-cooking-classes-on-the-internet')
def landing_usa_italian_cooking47():
    return render_template('landing_usa.html', data=" ")
@app.route('/teach-english-abroad-without-tefl-or-degree')
def landing_usa_italian_cooking48():
    return render_template('landing_usa.html', data="italian cooking classes on the internet")
@app.route('/italian-cooking-classes-mesa-az')
def landing_usa_italian_cooking49():
    return render_template('landing_usa.html', data="italian cooking classes mesa az")

@app.route('/italian-cooking-classes-greenville-sc')
def landing_usa_italian_cooking50():
    return render_template('landing_usa.html', data="italian cooking classes greenville sc")
@app.route('/italian-cooking-classes-washington-dc')
def landing_usa_italian_cooking51():
    return render_template('landing_usa.html', data="italian cooking classes washington dc")
@app.route('/italian-cooking-classes-louisville-ky')
def landing_usa_italian_cooking52():
    return render_template('landing_usa.html', data="italian cooking classes louisville ky")
@app.route('/italian-cooking-classes-in-knoxville-tn')
def landing_usa_italian_cooking53():
    return render_template('landing_usa.html', data="italian cooking classes in knoxville tn")
@app.route('/italian-cooking-classes-colorado-springs')
def landing_usa_italian_cooking54():
    return render_template('landing_usa.html', data="italian cooking classes colorado springs")
@app.route('/italian-cooking-classes-memphis-tn')
def landing_usa_italian_cooking55():
    return render_template('landing_usa.html', data="italian cooking classes memphis tn")
@app.route('/italian-cooking-classes-phoenix')
def landing_usa_italian_cooking56():
    return render_template('landing_usa.html', data="italian cooking classes phoenix")
@app.route('/italian-cooking-classes-boston-ma')
def landing_usa_italian_cooking57():
    return render_template('landing_usa.html', data="italian cooking classes boston ma")
@app.route('/italian-cooking-classes-st-louis-mo')
def landing_usa_italian_cooking58():
    return render_template('landing_usa.html', data="italian cooking classes st louis mo")
@app.route('/italian-cooking-classes-atlanta-ga')
def landing_usa_italian_cooking59():
    return render_template('landing_usa.html', data="italian cooking classes atlanta ga")

@app.route('/italian-cooking-classes-baton-rouge')
def landing_usa_italian_cooking60():
    return render_template('landing_usa.html', data="italian cooking classes baton rouge")
@app.route('/italian-cooking-classes-in-houston-texas')
def landing_usa_italian_cooking61():
    return render_template('landing_usa.html', data="italian cooking classes in houston texas")
@app.route('/italian-cooking-classes-san-antonio')
def landing_usa_italian_cooking62():
    return render_template('landing_usa.html', data="italian cooking classes san antonio")
@app.route('/italian-cooking-classes-houston-tx')
def landing_usa_italian_cooking63():
    return render_template('landing_usa.html', data="italian cooking classes houston tx")
@app.route('/italian-cooking-classes-dallas-tx')
def landing_usa_italian_cooking64():
    return render_template('landing_usa.html', data=" ")
@app.route('/italian-cooking-classes-portland-oregon')
def landing_usa_italian_cooking65():
    return render_template('landing_usa.html', data="italian cooking classes portland oregon")
@app.route('/italian-cooking-classes-queens-ny')
def landing_usa_italian_cooking66():
    return render_template('landing_usa.html', data="italian cooking classes queens ny")
@app.route('/italian-cooking-classes-cleveland-ohio')
def landing_usa_italian_cooking67():
    return render_template('landing_usa.html', data="italian cooking classes cleveland ohio")
@app.route('/italian-cooking-classes-bronx-ny')
def landing_usa_italian_cooking68():
    return render_template('landing_usa.html', data="italian cooking classes bronx ny")
@app.route('/italian-cooking-classes-tampa')
def landing_usa_italian_cooking69():
    return render_template('landing_usa.html', data="italian cooking classes tampa")

@app.route('/italian-cooking-classes-albany-ny')
def landing_usa_italian_cooking70():
    return render_template('landing_usa.html', data="italian cooking classes albany ny")
@app.route('/italian-cooking-classes-tampa-fl')
def landing_usa_italian_cooking71():
    return render_template('landing_usa.html', data="italian cooking classes tampa fl")
@app.route('/italian-cooking-classes-brooklyn-ny')
def landing_usa_italian_cooking72():
    return render_template('landing_usa.html', data="italian cooking classes brooklyn ny")
@app.route('/italian-cooking-classes-dc')
def landing_usa_italian_cooking73():
    return render_template('landing_usa.html', data="italian cooking classes dc")
@app.route('/italian-cooking-classes-knoxville-tne')
def landing_usa_italian_cooking74():
    return render_template('landing_usa.html', data="italian cooking classes knoxville tn")
@app.route('/italian-cooking-classes-raleigh-nc')
def landing_usa_italian_cooking75():
    return render_template('landing_usa.html', data="italian cooking classes raleigh nc")
@app.route('/italian-cooking-classes-staten-island-ny')
def landing_usa_italian_cooking76():
    return render_template('landing_usa.html', data="italian cooking classes staten island ny")
@app.route('/italian-cooking-classes-anchorage-ak')
def landing_usa_italian_cooking77():
    return render_template('landing_usa.html', data="italian cooking classes anchorage ak")
@app.route('/italian-cooking-classes-courses')
def landing_usa_italian_cooking78():
    return render_template('landing_usa.html', data="italian cooking classes courses")
@app.route('/italian-cooking-classes-sacramento-ca')
def landing_usa_italian_cooking79():
    return render_template('landing_usa.html', data="italian cooking classes sacramento ca")

@app.route('/italian-cooking-classes-chicago-il')
def landing_usa_italian_cooking80():
    return render_template('landing_usa.html', data="italian cooking classes chicago il")
@app.route('/italian-cooking-classes-miami-fl')
def landing_usa_italian_cooking81():
    return render_template('landing_usa.html', data="italian cooking classes miami fl")
@app.route('/italian-cooking-classes-bakersfield-ca')
def landing_usa_italian_cooking82():
    return render_template('landing_usa.html', data="italian cooking classes bakersfield ca")
@app.route('/italian-cooking-classes-baltimore-md')
def landing_usa_italian_cooking83():
    return render_template('landing_usa.html', data="italian cooking classes baltimore md")
@app.route('/italian-cooking-classes-new-york')
def landing_usa_italian_cooking84():
    return render_template('landing_usa.html', data="italian cooking classes new york")
@app.route('/italian-cooking-classes-las-vegas-nv')
def landing_usa_italian_cooking85():
    return render_template('landing_usa.html', data="italian cooking classes las vegas nv")
@app.route('/italian-cooking-classes-san-diego-ca')
def landing_usa_italian_cooking86():
    return render_template('landing_usa.html', data="italian cooking classes san diego ca")
@app.route('/italian-cooking-classes-austin-texas')
def landing_usa_italian_cooking87():
    return render_template('landing_usa.html', data="italian cooking classes austin texas")
@app.route('/italian-cooking-classes-milwaukee')
def landing_usa_italian_cooking88():
    return render_template('landing_usa.html', data="italian cooking classes milwaukee")
@app.route('/italian-cooking-classes-reno-nv')
def landing_usa_italian_cooking89():
    return render_template('landing_usa.html', data="italian cooking classes reno nv")

@app.route('/italian-cooking-classes-oklahoma-city')
def landing_usa_italian_cooking90():
    return render_template('landing_usa.html', data="italian cooking classes oklahoma city")
@app.route('/italian-cooking-classes-erie-pa')
def landing_usa_italian_cooking91():
    return render_template('landing_usa.html', data="italian cooking classes erie pa")
@app.route('/italian-cooking-classes-for-adults-in-chicago')
def landing_usa_italian_cooking92():
    return render_template('landing_usa.html', data="italian cooking classes for adults in chicago")



######London Keyword Begins ####

@app.route('/business-analyst-training-courses-and-classes-in-london')
def london_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training courses, classes in London')

@app.route('/business-analyst-training-course-abbey-wood')
def abbey_wood_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Abbey Wood')

@app.route('/business-analyst-training-course-acton')
def acton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Acton')

@app.route('/business-analyst-training-course-addington')
def addington_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Addington')

@app.route('/business-analyst-training-course-addiscombe')
def addiscombe_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Addiscombe')

@app.route('/business-analyst-training-course-aldborough-aatch')
def aldborough_hatch_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Aldborough Hatch')

@app.route('/business-analyst-training-course-aldgate')
def aldgate_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Aldgate')

@app.route('/business-analyst-training-course-aldwych')
def aldwych_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Aldwych')

@app.route('/business-analyst-training-course-alperton')
def alperton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Alperton')

@app.route('/business-analyst-training-course-anerley')
def anerley_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Anerley')

@app.route('/business-analyst-training-course-angel')
def angel_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Angel')

@app.route('/business-analyst-training-course-aperfield')
def aperfield_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Aperfield')

@app.route('/business-analyst-training-course-archway')
def archway_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Archway')

@app.route('/business-analyst-training-course-ardleigh-green')
def ardleigh_green_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Ardleigh Green')

@app.route('/business-analyst-training-course-arkley')
def arkley_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Arkley')

@app.route('/business-analyst-training-course-arnos-arove')
def arnos_grove_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Arnos Grove')

@app.route('/business-analyst-training-course-balham')
def balham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Balham')

@app.route('/business-analyst-training-course-bankside')
def bankside_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Bankside')

@app.route('/business-analyst-training-course-barbican')
def barbican_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Barbica')

@app.route('/business-analyst-training-course-barking')
def barking_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Barking')

@app.route('/business-analyst-training-course-barkingside')
def barkingside_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Barkingside')

@app.route('/business-analyst-training-course-barnehurst')
def barnehurst_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Barnehurst')

@app.route('/business-analyst-training-course-barnes')
def barnes_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Barnes')

@app.route('/business-analyst-training-course-barnes-cray')
def barnes_cray_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Barnes Cray')

@app.route('/business-analyst-training-course-barnet')
def barnet_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Barnet')

@app.route('/business-analyst-training-course-chipping-barnet')
def chipping_barnet_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Chipping Barnet')

@app.route('/business-analyst-training-course-high_barnet')
def high_barnet_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in High Barnet')

@app.route('/business-analyst-training-course-barnsbury')
def barnsbury_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Barnsbury')

@app.route('/business-analyst-training-course-battersea')
def battersea_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Battersea')

@app.route('/business-analyst-training-course-bayswater')
def bayswater_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Bayswater')

@app.route('/business-analyst-training-course-beckenham')
def beckenham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Beckenham')

@app.route('/business-analyst-training-course-beckton')
def beckton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Beckton')

@app.route('/business-analyst-training-course-becontree')
def becontree_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Becontree')

@app.route('/business-analyst-training-course-becontree-heath')
def becontree_heath_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Becontree Heath')

@app.route('/business-analyst-training-course-beddington')
def beddington_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Beddington')

@app.route('/business-analyst-training-course-bedford-park')
def bedford_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Bedford Park')

@app.route('/business-analyst-training-course-belgravia')
def belgravia_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Belgravia')

@app.route('/business-analyst-training-course-bellingham')
def bellingham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Bellingham')

@app.route('/business-analyst-training-course-belmont')
def belmont_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Belmont')

@app.route('/business-analyst-training-course-belsize-park')
def belsize_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Belsize Park')

@app.route('/business-analyst-training-course-belvedere')
def belvedere_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Belvedere')

@app.route('/business-analyst-training-course-bermondsey')
def bermondsey_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Bermondsey')

@app.route('/business-analyst-training-course-berrylands')
def berrylands_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Berrylands')

@app.route('/business-analyst-training-course-bethnal-green')
def bethnal_green_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Bethnal Green')

@app.route('/business-analyst-training-course-bexley')
def bexley_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Bexley')

@app.route('/business-analyst-training-course-bexley-village')
def bexley_village_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Bexley Village')

@app.route('/business-analyst-training-course-old-bexley')
def old_bexley_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Old Bexley')

@app.route('/business-analyst-training-course-bexleyheath')
def bexleyheath_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Bexleyheath')

@app.route('/business-analyst-training-course-bexley-new-town')
def bexley_new_town_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Bexley New Town')

@app.route('/business-analyst-training-course-bickley')
def bickley_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Bickley')

@app.route('/business-analyst-training-course-biggin-hill')
def biggin_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Biggin Hill')

@app.route('/business-analyst-training-course-blackfen')
def blackfen_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Blackfen')

@app.route('/business-analyst-training-course-blackfriars')
def blackfriars_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Blackfriars')

@app.route('/business-analyst-training-course-blackheath')
def blackheath_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Blackheath')

@app.route('/business-analyst-training-course-blackheath-royal-standard')
def blackheath_royal_standard_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Blackheath Royal Standard')

@app.route('/business-analyst-training-course-blackwall')
def blackwall_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Blackwall')

@app.route('/business-analyst-training-course-blendon')
def blendon_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Blendon')

@app.route('/business-analyst-training-course-bloomsbury')
def bloomsbury_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Bloomsbury')

@app.route('/business-analyst-training-course-botany-bay')
def botany_bay_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Botany Bay')

@app.route('/business-analyst-training-course-bounds-green')
def bounds_green_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Bounds Green')

@app.route('/business-analyst-training-course-bow')
def bow_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Bow')

@app.route('/business-analyst-training-course-bowes-park')
def bowes_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Bowes Park')

@app.route('/business-analyst-training-course-brentford')
def brentford_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Brentford')

@app.route('/business-analyst-training-course-brent-cross')
def brent_cross_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Brent Cross')

@app.route('/business-analyst-training-course-brent-park')
def brent_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Brent Park')

@app.route('/business-analyst-training-course-brimsdown')
def brimsdown_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Brimsdown')

@app.route('/business-analyst-training-course-brixton')
def brixton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Brixton')

@app.route('/business-analyst-training-course-brockley')
def brockley_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Brockley')

@app.route('/business-analyst-training-course-bromley')
def bromley_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Bromley')

@app.route('/business-analyst-training-course-bromley-by-bow')
def bromley_by_Bow_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Bromley by Bow')

@app.route('/business-analyst-training-course-bromley-common')
def bromley_common_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Bromley Common')

@app.route('/business-analyst-training-course-brompton')
def brompton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Brompton')

@app.route('/business-analyst-training-course-brondesbury')
def brondesbury_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Brondesbury')

@app.route('/business-analyst-training-course-brunswick-park')
def brunswick_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Brunswick Park')

@app.route('/business-analyst-training-course-bulls-cross')
def bulls_cross_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Bulls Cross')

@app.route('/business-analyst-training-course-burnt-oak')
def burnt_oak_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Burnt Oak')

@app.route('/business-analyst-training-course-burroughs')
def burroughs_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Burroughs')

@app.route('/business-analyst-training-course-the-camberwell')
def the_camberwell_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in The Camberwell')

@app.route('/business-analyst-training-course-cambridge-heath')
def cambridge_heath_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Cambridge Heath')

@app.route('/business-analyst-training-course-camden-town')
def camden_town_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Camden Town')

@app.route('/business-analyst-training-course-canary-wharf')
def canary_wharf_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Canary Wharf')

@app.route('/business-analyst-training-course-cann-hall')
def cann_hall_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Cann Hall')

@app.route('/business-analyst-training-course-canning-town')
def canning_town_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Canning Town')

@app.route('/business-analyst-training-course-canonbury')
def canonbury_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Canonbury')

@app.route('/business-analyst-training-course-carshalton')
def carshalton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Carshalton')

@app.route('/business-analyst-training-course-castelnau')
def castelnau_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Castelnau')

@app.route('/business-analyst-training-course-catford')
def catford_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Catford')

@app.route('/business-analyst-training-course-chadwell-heath')
def chadwell_heath_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Chadwell Heath')

@app.route('/business-analyst-training-course-chalk-farm')
def chalk_farm_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Chalk Farm')

@app.route('/business-analyst-training-course-charing-cross')
def charing_cross_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Charing Cross')

@app.route('/business-analyst-training-course-charlton')
def charlton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Charlton')

@app.route('/business-analyst-training-course-chase-cross')
def chase_cross_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Chase Cross')

@app.route('/business-analyst-training-course-cheam')
def cheam_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Cheam')

@app.route('/business-analyst-training-course-chelsea')
def chelsea_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Chelsea')

@app.route('/business-analyst-training-course-chelsfield')
def chelsfield_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Chelsfield')

@app.route('/business-analyst-training-course-chessington')
def chessington_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Chessington')

@app.route('/business-analyst-training-course-childs-hill')
def childs_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Childs Hill')

@app.route('/business-analyst-training-course-chinatown')
def chinatown_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Chinatown')

@app.route('/business-analyst-training-course-chinbrook')
def chinbrook_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Chinbrook')

@app.route('/business-analyst-training-course-chingford')
def chingford_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Chingford')

@app.route('/business-analyst-training-course-chislehurst')
def chislehurst_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Chislehurst')

@app.route('/business-analyst-training-course-chiswick')
def chiswick_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Chiswick')

@app.route('/business-analyst-training-course-church-end')
def church_end_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Church End')

@app.route('/business-analyst-training-course-clapham')
def clapham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Clapham')

@app.route('/business-analyst-training-course-clerkenwell')
def clerkenwell_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Clerkenwell')

@app.route('/business-analyst-training-course-cockfosters')
def cockfosters_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Cockfosters')

@app.route('/business-analyst-training-course-colindale')
def colindale_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Colindale')

@app.route('/business-analyst-training-course-collier-row')
def collier_row_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Collier Row')

@app.route('/business-analyst-training-course-colliers-wood')
def colliers_wood_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Colliers Wood')

@app.route('/business-analyst-training-course-colney-hatch')
def colney_hatch_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Colney Hatch')

@app.route('/business-analyst-training-course-colyers')
def colyers_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Colyers')

@app.route('/business-analyst-training-course-coney-hall')
def coney_hall_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Coney Hall')

@app.route('/business-analyst-training-course-coombe')
def coombe_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Coombe')

@app.route('/business-analyst-training-course-coulsdon')
def coulsdon_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Coulsdon')

@app.route('/business-analyst-training-course-covent-garden')
def covent_garden_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Covent Garden')

@app.route('/business-analyst-training-course-cowley')
def cowley_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Cowley')

@app.route('/business-analyst-training-course-cranford')
def cranford_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Cranford')

@app.route('/business-analyst-training-course-cranham')
def cranham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Cranham')

@app.route('/business-analyst-training-course-crayford')
def crayford_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Crayford')

@app.route('/business-analyst-training-course-creekmouth')
def creekmouth_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Creekmouth')

@app.route('/business-analyst-training-course-crews-hill')
def crews_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Crews Hill')

@app.route('/business-analyst-training-course-cricklewood')
def cricklewood_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Cricklewood')

@app.route('/business-analyst-training-course-crofton-park')
def crofton_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Crofton Park')

@app.route('/business-analyst-training-course-crook-log')
def crook_log_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Crook Log')

@app.route('/business-analyst-training-course-crossness')
def crossness_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Crossness')

@app.route('/business-analyst-training-course-crouch-end')
def crouch_end_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Crouch End')

@app.route('/business-analyst-training-course-croydon')
def croydon_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Croydon')

@app.route('/business-analyst-training-course-crystal-palace')
def crystal_palace_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Crystal Palace')

@app.route('/business-analyst-training-course-cubitt-town')
def cubitt_town_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Cubitt Town')

@app.route('/business-analyst-training-course-cudham')
def cudham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Cudham')

@app.route('/business-analyst-training-course-custom-house')
def custom_house_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Custom House')

@app.route('/business-analyst-training-course-dagenham')
def dagenham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Dagenham')

@app.route('/business-analyst-training-course-dalston')
def dalston_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Dalston')

@app.route('/business-analyst-training-course-dartmouth-park')
def dartmouth_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Dartmouth Park')

@app.route('/business-analyst-training-course-de-beauvoir-town')
def de_beauvoir_town_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in De Beauvoir Town')

@app.route('/business-analyst-training-course-denmark-hill')
def denmark_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Denmark Hill')

@app.route('/business-analyst-training-course-deptford')
def deptford_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Deptford')

@app.route('/business-analyst-training-course-derry-downs')
def derry_downs_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Derry Downs')

@app.route('/business-analyst-training-course-dollis-hill')
def dollis_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Dollis Hill')

@app.route('/business-analyst-training-course-downe')
def downe_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Downe')

@app.route('/business-analyst-training-course-downham')
def downham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Downham')

@app.route('/business-analyst-training-course-dulwich')
def dulwich_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Dulwich')

@app.route('/business-analyst-training-course-ealing')
def ealing_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Ealing')

@app.route('/business-analyst-training-course-earls-court')
def earls_court_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Earls Court')

@app.route('/business-analyst-training-course-earlsfield')
def earlsfield_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Earlsfield')

@app.route('/business-analyst-training-course-east-barnet')
def east_barnet_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in East Barnet')

@app.route('/business-analyst-training-course-east-bedfont')
def east_bedfont_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in East Bedfont')

@app.route('/business-analyst-training-course-east-dulwich')
def east_dulwich_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in East Dulwich')

@app.route('/business-analyst-training-course-east-finchley')
def east_finchley_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in East Finchley')

@app.route('/business-analyst-training-course-east-ham')
def east_ham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in East Ham')

@app.route('/business-analyst-training-course-east-sheen')
def east_sheen_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in East Sheen')

@app.route('/business-analyst-training-course-eastcote')
def eastcote_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Eastcote')

@app.route('/business-analyst-training-course-eden-park')
def eden_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Eden Park')

@app.route('/business-analyst-training-course-edgware')
def edgware_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Edgware')

@app.route('/business-analyst-training-course-edmonton')
def Edmonton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Edmonton')

@app.route('/business-analyst-training-course-eel-pie-island')
def eel_pie_island_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Eel Pie Island')

@app.route('/business-analyst-training-course-elephant-and-castle')
def elephant_and_castle_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Elephant and Castle')

@app.route('/business-analyst-training-course-elm-park')
def elm_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Elm Park')

@app.route('/business-analyst-training-course-elmers-end')
def elmers_end_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Elmers End')

@app.route('/business-analyst-training-course-elmstead')
def elmstead_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Elmstead')

@app.route('/business-analyst-training-course-eltham')
def eltham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Eltham')

@app.route('/business-analyst-training-course-emerson-park')
def emerson_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Emerson Park')

@app.route('/business-analyst-training-course-enfield-highway')
def enfield_highway_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Enfield Highway')

@app.route('/business-analyst-training-course-enfield-town')
def enfield_town_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Enfield Town')

@app.route('/business-analyst-training-course-enfield-wash')
def enfield_wash_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Enfield Wash')

@app.route('/business-analyst-training-course-erith')
def erith_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Erith')

@app.route('/business-analyst-training-course-falconwood')
def falconwood_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Falconwood')

@app.route('/business-analyst-training-course-farringdon')
def farringdon_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Farringdon')

@app.route('/business-analyst-training-course-feltham')
def feltham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Feltham')

@app.route('/business-analyst-training-course-finchley')
def finchley_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Finchley')

@app.route('/business-analyst-training-course-finsbury')
def finsbury_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Finsbury')

@app.route('/business-analyst-training-course-finsbury-park')
def finsbury_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Finsbury Park')

@app.route('/business-analyst-training-course-fitzrovia')
def fitzrovia_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Fitzrovia')

@app.route('/business-analyst-training-course-foots-cray')
def foots_cray_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Foots Cray')

@app.route('/business-analyst-training-course-forest-gate')
def forest_gate_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Forest Gate')

@app.route('/business-analyst-training-course-forest-hill')
def forest_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Forest Hill')

@app.route('/business-analyst-training-course-forestdale')
def forestdale_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Forestdale')

@app.route('/business-analyst-training-course-fortis-green')
def fortis_green_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Fortis Green')

@app.route('/business-analyst-training-course-freezywater')
def freezywater_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Freezywater')

@app.route('/business-analyst-training-course-friern-barnet')
def friern_barnet_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Friern Barnet')

@app.route('/business-analyst-training-course-frognal')
def frognal_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Frognal')

@app.route('/business-analyst-training-course-fulham')
def fulham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Fulham')

@app.route('/business-analyst-training-course-fulwell')
def fulwell_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Fulwell')

@app.route('/business-analyst-training-course-gallows-corner')
def gallows_corner_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Gallows Corner')

@app.route('/business-analyst-training-course-gants-hill')
def gants_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Gants Hill')

@app.route('/business-analyst-training-course-gidea-park')
def gidea_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Gidea Park')

@app.route('/business-analyst-training-course-gipsy-hill')
def gipsy_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Gipsy Hill')

@app.route('/business-analyst-training-course-golders-green')
def golders_green_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Golders Green')

@app.route('/business-analyst-training-course-goodmayes')
def goodmayes_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Goodmayes')

@app.route('/business-analyst-training-course-gospel-oak')
def gospel_oak_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Gospel Oak')

@app.route('/business-analyst-training-course-grahame-park')
def grahame_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Grahame Park')

@app.route('/business-analyst-training-course-grange-park')
def grange_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Grange Park')

@app.route('/business-analyst-training-course-greenford')
def greenford_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Greenford')

@app.route('/business-analyst-training-course-greenwich')
def greenwich_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Greenwich')

@app.route('/business-analyst-training-course-grove-park')
def grove_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Grove Park')

@app.route('/business-analyst-training-course-gunnersbury')
def gunnersbury_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Gunnersbury')

@app.route('/business-analyst-training-course-hackney')
def hackney_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hackney')

@app.route('/business-analyst-training-course-hackney-marshes')
def hackney_marshes_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hackney Marshes')

@app.route('/business-analyst-training-course-hackney-wick')
def hackney_wick_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hackney Wick')

@app.route('/business-analyst-training-course-hadley-wood')
def hadley_wood_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hadley Wood')

@app.route('/business-analyst-training-course-haggerston')
def haggerston_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Haggerston')

@app.route('/business-analyst-training-course-hainault')
def hainault_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hainault')

@app.route('/business-analyst-training-course-the-hale')
def the_hale_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in The Hale')

@app.route('/business-analyst-training-course-ham')
def ham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Ham')

@app.route('/business-analyst-training-course-hammersmith')
def hammersmith_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hammersmith')

@app.route('/business-analyst-training-course-hampstead')
def hampstead_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hampstead')

@app.route('/business-analyst-training-course-hampstead-garden-suburb')
def hampstead_garden_suburb_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hampstead Garden Suburb')

@app.route('/business-analyst-training-course-hampton')
def hampton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hampton')

@app.route('/business-analyst-training-course-hampton-hill')
def hampton_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hampton Hill')

@app.route('/business-analyst-training-course-hampton-wick')
def hampton_wick_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hampton Wick')

@app.route('/business-analyst-training-course-hanwell')
def hanwell_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hanwell')

@app.route('/business-analyst-training-course-hanworth')
def hanworth_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hanworth')

@app.route('/business-analyst-training-course-harefield')
def harefield_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Harefield')

@app.route('/business-analyst-training-course-harlesden')
def harlesden_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Harlesden')

@app.route('/business-analyst-training-course-harlington')
def harlington_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Harlington')

@app.route('/business-analyst-training-course-harmondsworth')
def harmondsworth_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Harmondsworth')

@app.route('/business-analyst-training-course-harold-hill')
def harold_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Harold Hill')

@app.route('/business-analyst-training-course-harold-park')
def harold_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Harold Park')

@app.route('/business-analyst-training-course-harold-wood')
def harold_wood_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Harold Wood')

@app.route('/business-analyst-training-course-harringay')
def harringay_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Harringay')

@app.route('/business-analyst-training-course-harrow')
def harrow_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Harrow')

@app.route('/business-analyst-training-course-harrow-on-the-hill')
def harrow_on_the_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Harrow on the Hill')

@app.route('/business-analyst-training-course-harrow-weald')
def harrow_weald_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Harrow Weald')

@app.route('/business-analyst-training-course-hatch_end')
def hatch_end_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hatch End')

@app.route('/business-analyst-training-course-hatton')
def hatton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hatton')

@app.route('/business-analyst-training-course-havering-atte-bower')
def havering_atte_bower_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Havering-atte-Bower')

@app.route('/business-analyst-training-course-hayes')
def hayes_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hayes')

@app.route('/business-analyst-training-course-hazelwood')
def hazelwood_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hazelwood')

@app.route('/business-analyst-training-course-hendon')
def hendon_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hendon')

@app.route('/business-analyst-training-course-herne-hill')
def herne_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Herne Hill')

@app.route('/business-analyst-training-course-heston')
def heston_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Heston')

@app.route('/business-analyst-training-course-highams-park')
def highams_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Highams Park')

@app.route('/business-analyst-training-course-highbury')
def highbury_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Highbury')

@app.route('/business-analyst-training-course-highgate')
def highgate_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Highgate')

@app.route('/business-analyst-training-course-hillingdon')
def hillingdon_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hillingdon')

@app.route('/business-analyst-training-course-hither-green')
def hither_green_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hither Green')

@app.route('/business-analyst-training-course-holborn')
def holborn_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Holborn')

@app.route('/business-analyst-training-course-holland-park')
def holland_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Holland Park')

@app.route('/business-analyst-training-course-holloway')
def holloway_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Holloway')

@app.route('/business-analyst-training-course-homerton')
def homerton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Homerton')

@app.route('/business-analyst-training-course-honor-oak')
def honor_oak_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Honor Oak')

@app.route('/business-analyst-training-course-hook')
def hook_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hook')

@app.route('/business-analyst-training-course-hornchurch')
def hornchurch_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hornchurch')

@app.route('/business-analyst-training-course-horn-park')
def hornc_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Horn Park')

@app.route('/business-analyst-training-course-hornsey')
def hornsey_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hornsey')

@app.route('/business-analyst-training-course-hounslow')
def hounslow_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hounslow')

@app.route('/business-analyst-training-course-hoxton')
def hoxton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Hoxton')

@app.route('/business-analyst-training-course-the-hyde')
def the_hyde_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in The Hyde')

@app.route('/business-analyst-training-course-ickenham')
def ickenham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Ickenham')

@app.route('/business-analyst-training-course-ilford')
def ilford_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Ilford')

@app.route('/business-analyst-training-course-isle-of-dogs')
def isle_of_dogs_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Isle of Dogs')

@app.route('/business-analyst-training-course-isleworth')
def isleworth_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Isleworth')

@app.route('/business-analyst-training-course-islington')
def islington_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Islington')

@app.route('/business-analyst-training-course-kenley')
def kenley_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Kenley')

@app.route('/business-analyst-training-course-kennington')
def kennington_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Kennington')

@app.route('/business-analyst-training-course-kensal-green')
def kensal_green_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Kensal Green')

@app.route('/business-analyst-training-course-kensington')
def kensington_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Kensington')

@app.route('/business-analyst-training-course-kentish-town')
def kentish_town_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Kentish Town')

@app.route('/business-analyst-training-course-kenton')
def kenton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Kenton')

@app.route('/business-analyst-training-course-keston')
def keston_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Keston')

@app.route('/business-analyst-training-course-kew')
def kew_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Kew')

@app.route('/business-analyst-training-course-kidbrooke')
def kidbrooke_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Kidbrooke')

@app.route('/business-analyst-training-course-kilburn')
def kilburn_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Kilburn')

@app.route('/business-analyst-training-course-kings-cross')
def kings_cross_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Kings Cross')

@app.route('/business-analyst-training-course-kingsbury')
def kingsbury_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Kingsbury')

@app.route('/business-analyst-training-course-kingston-vale')
def kingston_vale_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Kingston Vale')

@app.route('/business-analyst-training-course-kingston-upon-thames')
def kingston_upon_thames_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Kingston upon Thames')

@app.route('/business-analyst-training-course-knightsbridge')
def knightsbridge_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Knightsbridge')

@app.route('/business-analyst-training-course-ladywell')
def ladywell_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Ladywell')

@app.route('/business-analyst-training-course-lambeth')
def lambeth_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Lambeth')

@app.route('/business-analyst-training-course-lamorbey')
def lamorbey_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Lamorbey')

@app.route('/business-analyst-training-course-lampton')
def lampton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Lampton')

@app.route('/business-analyst-training-course-leamouth')
def leamouth_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Leamouth')

@app.route('/business-analyst-training-course-leaves-green')
def leaves_green_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Leaves Green')

@app.route('/business-analyst-training-course-lee')
def lee_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Lee')

@app.route('/business-analyst-training-course-lewisham')
def lewisham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Lewisham')

@app.route('/business-analyst-training-course-leyton')
def leyton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Leyton')

@app.route('/business-analyst-training-course-leytonstone')
def leytonstone_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Leytonstone')

@app.route('/business-analyst-training-course-limehouse')
def limehouse_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Limehouse')

@app.route('/business-analyst-training-course-lisson-grove')
def lisson_grove_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Lisson Grove')

@app.route('/business-analyst-training-course-little-ilford')
def little_ilford_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Little Ilford')

@app.route('/business-analyst-training-course-locksbottom')
def locksbottom_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Locksbottom')

@app.route('/business-analyst-training-course-longford')
def longford_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Longford')

@app.route('/business-analyst-training-course-longlands')
def longlands_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Longlands')

@app.route('/business-analyst-training-course-lower-clapton')
def lower_clapton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Lower Clapton')

@app.route('/business-analyst-training-course-lower-morden')
def lower_morden_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Lower Morden')

@app.route('/business-analyst-training-course-loxford')
def loxford_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Loxford')

@app.route('/business-analyst-training-course-maida-vale')
def maida_vale_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Maida Vale')

@app.route('/business-analyst-training-course-malden-rushett')
def malden_rushett_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Malden Rushett')

@app.route('/business-analyst-training-course-manor-house')
def manor_house_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Manor House')

@app.route('/business-analyst-training-course-manor-park')
def manor_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Manor Park')

@app.route('/business-analyst-training-course-marks-gate')
def marks_gate_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Marks Gate')

@app.route('/business-analyst-training-course-maryland')
def maryland_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Maryland')

@app.route('/business-analyst-training-course-st-marylebone')
def st_marylebone_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in St Marylebone')

@app.route('/business-analyst-training-course-marylebone')
def marylebone_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Marylebone')

@app.route('/business-analyst-training-course-mayfair')
def mayfair_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Mayfair')

@app.route('/business-analyst-training-course-maze-hill')
def maze_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Maze Hill')

@app.route('/business-analyst-training-course-merton-park')
def merton_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Merton Park')

@app.route('/business-analyst-training-course-middle-park')
def middle_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Middle Park')

@app.route('/business-analyst-training-course-mile-end')
def mile_end_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Mile End')

@app.route('/business-analyst-training-course-mill-hill')
def mill_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Mill Hill')

@app.route('/business-analyst-training-course-millbank')
def millbank_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Millbank')

@app.route('/business-analyst-training-course-millwall')
def millwall_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Millwall')

@app.route('/business-analyst-training-course-mitcham')
def mitcham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Mitcham')

@app.route('/business-analyst-training-course-monken-hadley')
def monken_hadley_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Monken Hadley')

@app.route('/business-analyst-training-course-morden')
def morden_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Morden')

@app.route('/business-analyst-training-course-morden-park')
def morden_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Morden Park')

@app.route('/business-analyst-training-course-mortlake')
def mortlake_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Mortlake')

@app.route('/business-analyst-training-course-motspur-park')
def motspur_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Motspur Park')

@app.route('/business-analyst-training-course-mottingham')
def mottingham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Mottingham')

@app.route('/business-analyst-training-course-muswell-hill')
def muswell_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Muswell Hill')

@app.route('/business-analyst-training-course-nags-head')
def nags_head_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Nags Head')

@app.route('/business-analyst-training-course-neasden')
def neasden_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Neasden')

@app.route('/business-analyst-training-course-new-addington')
def new_addington_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in New Addington')

@app.route('/business-analyst-training-course-new-barnet')
def new_barnet_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in New Barnet')

@app.route('/business-analyst-training-course-new-cross')
def new_cross_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in New Cross')

@app.route('/business-analyst-training-course-new-eltham')
def new_eltham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in New Eltham')

@app.route('/business-analyst-training-course-new-malden')
def new_malden_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in New Malden')

@app.route('/business-analyst-training-course-new-southgate')
def new_southgate_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in New Southgate')

@app.route('/business-analyst-training-course-newbury-park')
def newbury_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Newbury Park')

@app.route('/business-analyst-training-course-newington')
def newington_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Newington')

@app.route('/business-analyst-training-course-nine-elms')
def nine_elms_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Nine Elms')

@app.route('/business-analyst-training-course-noak-hill')
def noak_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Noak Hill')

@app.route('/business-analyst-training-course-norbiton')
def norbiton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Norbiton')

@app.route('/business-analyst-training-course-norbury')
def norbury_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Norbury')

@app.route('/business-analyst-training-course-north-end')
def north_end_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in North End')

@app.route('/business-analyst-training-course-north-finchley')
def north_finchley_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in North Finchley')

@app.route('/business-analyst-training-course-north-harrow')
def north_harrow_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in North Harrow')

@app.route('/business-analyst-training-course-north-kensington')
def north_kensington_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in North Kensington')

@app.route('/business-analyst-training-course-north-ockendon')
def north_ockendon_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in North Ockendon')

@app.route('/business-analyst-training-course-north-sheen')
def north_sheen_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in North Sheen')

@app.route('/business-analyst-training-course-north-woolwich')
def north_woolwich_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in North Woolwich')

@app.route('/business-analyst-training-course-northolt')
def northolt_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Northolt')

@app.route('/business-analyst-training-course-northumberland-heath')
def northumberland_heath_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Northumberland Heath')

@app.route('/business-analyst-training-course-northwood')
def northwood_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Northwood')

@app.route('/business-analyst-training-course-norwood-green')
def norwood_green_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Norwood Green')

@app.route('/business-analyst-training-course-notting-hill')
def notting_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Notting Hill')

@app.route('/business-analyst-training-course-nunhead')
def nunhead_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Nunhead')

@app.route('/business-analyst-training-course-oakleigh-park')
def oakleigh_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Oakleigh Park')

@app.route('/business-analyst-training-course-old-coulsdon')
def old_coulsdon_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Old Coulsdon')

@app.route('/business-analyst-training-course-old-ford')
def old_ford_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Old Ford')

@app.route('/business-analyst-training-course-old-malden')
def old_malden_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Old Malden')

@app.route('/business-analyst-training-course-old-oak-common')
def old_oak_common_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Old Oak Common')

@app.route('/business-analyst-training-course-orpington')
def orpington_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Orpington')

@app.route('/business-analyst-training-course-osidge')
def osidge_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Osidge')

@app.route('/business-analyst-training-course-osterley')
def osterley_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Osterley')

@app.route('/business-analyst-training-course-paddington')
def paddington_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Paddington')

@app.route('/business-analyst-training-course-palmers-green')
def palmers_green_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Palmers Green')

@app.route('/business-analyst-training-course-park-royal')
def park_royal_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Park Royal')

@app.route('/business-analyst-training-course-parsons-green')
def parsons_green_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Parsons Green')

@app.route('/business-analyst-training-course-peckham')
def peckham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Peckham')

@app.route('/business-analyst-training-course-penge')
def penge_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Penge')

@app.route('/business-analyst-training-course-pentonville')
def pentonville_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Pentonville')

@app.route('/business-analyst-training-course-perivale')
def perivale_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Perivale')

@app.route('/business-analyst-training-course-petersham')
def petersham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Petersham')

@app.route('/business-analyst-training-course-petts-wood')
def petts_wood_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Petts Wood')

@app.route('/business-analyst-training-course-pimlico')
def pimlico_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Pimlico')

@app.route('/business-analyst-training-course-pinner')
def pinner_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Pinner')

@app.route('/business-analyst-training-course-plaistow')
def plaistow_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Plaistow')

@app.route('/business-analyst-training-course-plumstead')
def plumstead_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Plumstead')

@app.route('/business-analyst-training-course-ponders_end')
def ponders_end_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Ponders End')

@app.route('/business-analyst-training-course-poplar')
def poplar_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Poplar')

@app.route('/business-analyst-training-course-pratts-bottom')
def pratts_bottom_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Pratts Bottom')

@app.route('/business-analyst-training-course-preston')
def preston_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Preston')

@app.route('/business-analyst-training-course-primrose-hill')
def primrose_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Primrose Hill')

@app.route('/business-analyst-training-course-purley')
def purley_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Purley')

@app.route('/business-analyst-training-course-putney')
def putney_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Putney')

@app.route('/business-analyst-training-course-queens-park')
def queens_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Queens Park')

@app.route('/business-analyst-training-course-queensbury')
def queensbury_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Queensbury')

@app.route('/business-analyst-training-course-rainham')
def rainham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Rainham')

@app.route('/business-analyst-training-course-ratcliff')
def ratcliff_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Ratcliff')

@app.route('/business-analyst-training-course-rayners-lane')
def rayners_lane_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Rayners Lane')

@app.route('/business-analyst-training-course-raynes-park')
def raynes_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Raynes Park')

@app.route('/business-analyst-training-course-redbridge')
def redbridge_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Redbridge')

@app.route('/business-analyst-training-course-richmond')
def richmond_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Richmond')

@app.route('/business-analyst-training-course-riddlesdown')
def riddlesdown_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Riddlesdown')

@app.route('/business-analyst-training-course-roehampton')
def roehampton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Roehampton')

@app.route('/business-analyst-training-course-romford')
def romford_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Romford')

@app.route('/business-analyst-training-course-rotherhithe')
def rotherhithe_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Rotherhithe')

@app.route('/business-analyst-training-course-ruislip')
def ruislip_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Ruislip')

@app.route('/business-analyst-training-course-rush-green')
def rush_green_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Rush Green')

@app.route('/business-analyst-training-course-ruxley')
def ruxley_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Ruxley')

@app.route('/business-analyst-training-course-sanderstead')
def sanderstead_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Sanderstead')

@app.route('/business-analyst-training-course-sands-end')
def sands_end_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Sands End')

@app.route('/business-analyst-training-course-selhurst')
def selhurst_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Selhurst')

@app.route('/business-analyst-training-course-selsdon')
def selsdon_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Selsdon')

@app.route('/business-analyst-training-course-seven-kings')
def seven_kings_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Seven Kings')

@app.route('/business-analyst-training-course-seven-sisters')
def seven_sisters_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Seven Sisters')

@app.route('/business-analyst-training-course-shacklewell')
def shacklewell_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Shacklewell')

@app.route('/business-analyst-training-course-shadwell')
def shadwell_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Shadwell')

@app.route('/business-analyst-training-course-shepherds-bush')
def shepherds_bush_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Shepherds Bush')

@app.route('/business-analyst-training-course-shirley')
def shirley_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Shirley')

@app.route('/business-analyst-training-course-shooters-hill')
def shooters_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Shooters Hill')

@app.route('/business-analyst-training-course-shoreditch')
def shoreditch_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Shoreditch')

@app.route('/business-analyst-training-course-sidcup')
def sidcup_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Sidcup')

@app.route('/business-analyst-training-course-silvertown')
def silvertown_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Silvertown')

@app.route('/business-analyst-training-course-sipson')
def sipson_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Sipson')

@app.route('/business-analyst-training-course-slade-green')
def slade_green_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Slade Green')

@app.route('/business-analyst-training-course-snaresbrook')
def snaresbrook_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Snaresbrook')

@app.route('/business-analyst-training-course-soho')
def soho_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Soho')

@app.route('/business-analyst-training-course-somerstown')
def somerstown_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Somerstown')

@app.route('/business-analyst-training-course-south-croydon')
def south_croydon_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in South Croydon')

@app.route('/business-analyst-training-course-south-hackney')
def south_hackney_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in South Hackney')

@app.route('/business-analyst-training-course-south-harrow')
def south_harrow_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in South Harrow')

@app.route('/business-analyst-training-course-south-hornchurch')
def south_hornchurch_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in South Hornchurch')

@app.route('/business-analyst-training-course-south-kensington')
def south_kensington_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in South Kensington')

@app.route('/business-analyst-training-course-south-norwood')
def south_norwood_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in South Norwood')

@app.route('/business-analyst-training-course-south-ruislip')
def south_ruislip_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in South Ruislip')

@app.route('/business-analyst-training-course-south-wimbledon')
def south_wimbledon_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in South Wimbledon')

@app.route('/business-analyst-training-course-south-woodford')
def south_woodford_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in South Woodford')

@app.route('/business-analyst-training-course-south-tottenham')
def south_tottenham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in South Tottenham')

@app.route('/business-analyst-training-course-southend')
def southend_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Southend')

@app.route('/business-analyst-training-course-southall')
def southall_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Southall')

@app.route('/business-analyst-training-course-southborough')
def southborough_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Southborough')

@app.route('/business-analyst-training-course-southfields')
def southfields_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Southfields')

@app.route('/business-analyst-training-course-southgate')
def southgate_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Southgate')

@app.route('/business-analyst-training-course-spitalfields')
def spitalfields_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Spitalfields')

@app.route('/business-analyst-training-course-st-helier')
def st_helier_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in St Helier')

@app.route('/business-analyst-training-course-st-james')
def st_james_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in St James')

@app.route('/business-analyst-training-course-st-margarets')
def st_margarets_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in St Margarets')

@app.route('/business-analyst-training-course-st-giles')
def st_giles_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in St Giles')

@app.route('/business-analyst-training-course-st-johns')
def st_johns_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in St Johns')

@app.route('/business-analyst-training-course-st-johns-wood')
def st_johns_wood_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in St Johns Wood')

@app.route('/business-analyst-training-course-st-lukes')
def st_lukes_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in St Lukes')

@app.route('/business-analyst-training-course-st-mary-cray')
def st_mary_cray_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in St Mary Cray')

@app.route('/business-analyst-training-course-st-pancras')
def st_pancras_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in St Pancras')

@app.route('/business-analyst-training-course-st-pauls-cray')
def st_pauls_cray_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in St Pauls Cray')

@app.route('/business-analyst-training-course-stamford-hill')
def stamford_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Stamford Hill')

@app.route('/business-analyst-training-course-stanmore')
def stanmore_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Stanmore')

@app.route('/business-analyst-training-course-stepney')
def stepney_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Stepney')

@app.route('/business-analyst-training-course-stockwell')
def stockwell_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Stockwell')

@app.route('/business-analyst-training-course-stoke-newington')
def stoke_newington_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Stoke Newington')

@app.route('/business-analyst-training-course-stratford')
def stratford_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Stratford')

@app.route('/business-analyst-training-course-strawberry-hill')
def strawberry_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Strawberry Hill')

@app.route('/business-analyst-training-course-streatham')
def streatham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Streatham')

@app.route('/business-analyst-training-course-stroud-green')
def stroud_green_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Stroud Green')

@app.route('/business-analyst-training-course-sudbury')
def sudbury_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Sudbury')

@app.route('/business-analyst-training-course-sundridge')
def sundridge_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Sundridge')

@app.route('/business-analyst-training-course-surbiton')
def surbiton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Surbiton')

@app.route('/business-analyst-training-course-surrey-quays')
def surrey_quays_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Surrey Quays')

@app.route('/business-analyst-training-course-sutton')
def sutton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Sutton')

@app.route('/business-analyst-training-course-swiss-cottage')
def swiss_cottage_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Swiss Cottage')

@app.route('/business-analyst-training-course-sydenham')
def sydenham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Sydenham')

@app.route('/business-analyst-training-course-lower-sydenham')
def lower_sydenham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Lower Sydenham')

@app.route('/business-analyst-training-course-upper-sydenham')
def upper_sydenham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Upper Sydenham')

@app.route('/business-analyst-training-course-sydenham-hill')
def sydenham_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Sydenham Hill')

@app.route('/business-analyst-training-course-teddington')
def teddington_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Teddington')

@app.route('/business-analyst-training-course-temple')
def temple_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Temple')

@app.route('/business-analyst-training-course-temple-fortune')
def temple_fortune_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Temple Fortune')

@app.route('/business-analyst-training-course-thamesmead')
def thamesmead_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Thamesmead')

@app.route('/business-analyst-training-course-thornton-heath')
def thornton_heath_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Thornton Heath')

@app.route('/business-analyst-training-course-tokyngton')
def tokyngton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Tokyngton')

@app.route('/business-analyst-training-course-tolworth')
def tolworth_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Tolworth')

@app.route('/business-analyst-training-course-tooting')
def tooting_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Tooting')

@app.route('/business-analyst-training-course-tooting-bec')
def tooting_bec_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Tooting Bec')

@app.route('/business-analyst-training-course-tottenham')
def tottenham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Tottenham')

@app.route('/business-analyst-training-course-tottenham-green')
def tottenham_green_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Tottenham Green')

@app.route('/business-analyst-training-course-tottenham_hale')
def tottenham_hale_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Tottenham Hale')

@app.route('/business-analyst-training-course-totteridge')
def totteridge_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Totteridge')

@app.route('/business-analyst-training-course-tower-hill')
def tower_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Tower Hill')

@app.route('/business-analyst-training-course-tufnell-park')
def tufnell_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Tufnell Park')

@app.route('/business-analyst-training-course-tulse-hill')
def tulse_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Tulse Hill')

@app.route('/business-analyst-training-course-turnpike-lane')
def turnpike_lane_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Turnpike Lane')

@app.route('/business-analyst-training-course-twickenham')
def twickenham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Twickenham')

@app.route('/business-analyst-training-course-upminster')
def upminster_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Upminster')

@app.route('/business-analyst-training-course-upminster-bridge')
def upminster_bridge_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Upminster Bridge')

@app.route('/business-analyst-training-course-upper-clapton')
def upper_clapton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Upper Clapton')

@app.route('/business-analyst-training-course-upper-holloway')
def upper_holloway_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Upper Holloway')

@app.route('/business-analyst-training-course-upper-norwood')
def upper_norwood_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Upper Norwood')

@app.route('/business-analyst-training-course-upper-ruxley')
def upper_ruxley_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Upper Ruxley')

@app.route('/business-analyst-training-course-upper-walthamstow')
def upper_walthamstow_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Upper Walthamstow')

@app.route('/business-analyst-training-course-upton')
def upton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Upton')

@app.route('/business-analyst-training-course-upton-park')
def upton_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Upton Park')

@app.route('/business-analyst-training-course-uxbridge')
def uxbridge_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Uxbridge')

@app.route('/business-analyst-training-course-uauxhall')
def uauxhall_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Vauxhall')

@app.route('/business-analyst-training-course-waddon')
def waddon_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Waddon')

@app.route('/business-analyst-training-course-wallington')
def wallington_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Wallington')

@app.route('/business-analyst-training-course-walthamstow')
def walthamstow_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Walthamstow')

@app.route('/business-analyst-training-course-walthamstow-village')
def walthamstow_village_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Walthamstow Village')

@app.route('/business-analyst-training-course-walworth')
def walworth_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Walworth')

@app.route('/business-analyst-training-course-wandsworth')
def wandsworth_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Wandsworth')

@app.route('/business-analyst-training-course-wanstead')
def wanstead_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Wanstead')

@app.route('/business-analyst-training-course-wapping')
def wapping_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Wapping')

@app.route('/business-analyst-training-course-wealdstone')
def wealdstone_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Wealdstone')

@app.route('/business-analyst-training-course-well-hall')
def well_hall_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Well Hall')

@app.route('/business-analyst-training-course-welling')
def welling_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Welling')

@app.route('/business-analyst-training-course-wembley')
def wembley_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Wembley')

@app.route('/business-analyst-training-course-wembley-park')
def wembley_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Wembley Park')

@app.route('/business-analyst-training-course-wennington')
def wennington_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Wennington')

@app.route('/business-analyst-training-course-west-brompton')
def west_brompton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in West Brompton')

@app.route('/business-analyst-training-course-west-drayton')
def west_drayton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in West Drayton')

@app.route('/business-analyst-training-course-west-ealing')
def west_ealing_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in West Ealing')

@app.route('/business-analyst-training-course-west-green')
def west_green_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in West Green')

@app.route('/business-analyst-training-course-west-ham')
def west_ham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in West Ham')

@app.route('/business-analyst-training-course-west-hampstead')
def west_hampstead_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in West Hampstead')

@app.route('/business-analyst-training-course-west-harrow')
def west_harrow_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in West Harrow')

@app.route('/business-analyst-training-course-west-heath')
def west_heath_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in West Heath')

@app.route('/business-analyst-training-course-west-hendon')
def west_hendon_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in West Hendon')

@app.route('/business-analyst-training-course-west-kensington')
def west_kensington_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in West Kensington')

@app.route('/business-analyst-training-course-west-norwood')
def west_norwood_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in West Norwood')

@app.route('/business-analyst-training-course-west-wickham')
def west_wickham_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in West Wickham')

@app.route('/business-analyst-training-course-westcombe-park')
def westcombe_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Westcombe Park')

@app.route('/business-analyst-training-course-westminster')
def westminster_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Westminster')

@app.route('/business-analyst-training-course-whetstone')
def whetstone_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Whetstone')

@app.route('/business-analyst-training-course-white-city')
def white_city_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in White City')

@app.route('/business-analyst-training-course-whitechapel')
def whitechapel_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Whitechapel')

@app.route('/business-analyst-training-course-widmore')
def widmore_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Widmore')

@app.route('/business-analyst-training-course-widmore-green')
def widmore_green_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Widmore Green')

@app.route('/business-analyst-training-course-whitton')
def whitton_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Whitton')

@app.route('/business-analyst-training-course-willesden')
def willesden_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Willesden')

@app.route('/business-analyst-training-course-wimbledon')
def wimbledon_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Wimbledon')

@app.route('/business-analyst-training-course-winchmore-hill')
def winchmore_hill_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Winchmore Hill')

@app.route('/business-analyst-training-course-wood-green')
def wood_green_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Wood Green')

@app.route('/business-analyst-training-course-woodford')
def woodford_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Woodford')

@app.route('/business-analyst-training-course-woodford-green')
def woodford_green_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Woodford Green')

@app.route('/business-analyst-training-course-woodlands')
def woodlands_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Woodlands')

@app.route('/business-analyst-training-course-woodside')
def woodside_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Woodside')

@app.route('/business-analyst-training-course-woodside-park')
def woodside_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Woodside Park')

@app.route('/business-analyst-training-course-woolwich')
def woolwich_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Woolwich')

@app.route('/business-analyst-training-course-worcester-park')
def worcester_park_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Worcester Park')

@app.route('/business-analyst-training-course-wormwood-scrubs')
def wormwood_scrubs_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Wormwood Scrubs')

@app.route('/business-analyst-training-course-yeading')
def yeading_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Yeading')

@app.route('/business-analyst-training-course-yiewsley')
def yiewsley_business_analyst_training_course():
    return render_template('landing/london.html', data='business analyst training course in Yiewsley')


######London Digital Marketing Classes Keyword Begins ####

@app.route('/digital-analyst-training-courses-classes-in-abbey-wood')
def abbey_wood_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Abbey Wood')

@app.route('/digital-marketing-training-courses-classes-in-acton')
def acton_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Acton')

@app.route('/digital-marketing-training-courses-classes-in-addington')
def addington_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Addington')

@app.route('/digital-marketing-training-courses-classes-in-addiscombe')
def addiscombe_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Addiscombe')

@app.route('/digital-marketing-training-courses-classes-in-aldborough-aatch')
def aldborough_hatch_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Aldborough Hatch')

@app.route('/digital-marketing-training-courses-classes-in-aldgate')
def aldgate_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Aldgate')

@app.route('/digital-marketing-training-courses-classes-in-aldwych')
def aldwych_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Aldwych')

@app.route('/digital-marketing-training-courses-classes-in-alperton')
def alperton_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Alperton')

@app.route('/digital-marketing-training-courses-classes-in-anerley')
def anerley_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Anerley')

@app.route('/digital-marketing-training-courses-classes-in-angel')
def angel_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Angel')

@app.route('/digital-marketing-training-courses-classes-in-aperfield')
def aperfield_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Aperfield')

@app.route('/digital-marketing-training-courses-classes-in-archway')
def archway_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Archway')

@app.route('/digital-marketing-training-courses-classes-in-ardleigh-green')
def ardleigh_green_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Ardleigh Green')

@app.route('/digital-marketing-training-courses-classes-in-arkley')
def arkley_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Arkley')

@app.route('/digital-marketing-training-courses-classes-in-arnos-arove')
def arnos_grove_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Arnos Grove')

@app.route('/digital-marketing-training-courses-classes-in-balham')
def balham_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Balham')

@app.route('/digital-marketing-training-courses-classes-in-bankside')
def bankside_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Bankside')

@app.route('/digital-marketing-training-courses-classes-in-barbican')
def barbican_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Barbica')

@app.route('/digital-marketing-training-courses-classes-in-barking')
def barking_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Barking')

@app.route('/digital-marketing-training-courses-classes-in-barkingside')
def barkingside_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Barkingside')

@app.route('/digital-marketing-training-courses-classes-in-barnehurst')
def barnehurst_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Barnehurst')

@app.route('/digital-marketing-training-courses-classes-in-barnes')
def barnes_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Barnes')

@app.route('/digital-marketing-training-courses-classes-in-barnes-cray')
def barnes_cray_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Barnes Cray')

@app.route('/digital-marketing-training-courses-classes-in-barnet')
def barnet_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Barnet')

@app.route('/digital-marketing-training-courses-classes-in-chipping-barnet')
def chipping_barnet_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Chipping Barnet')

@app.route('/digital-marketing-training-courses-classes-in-high_barnet')
def high_barnet_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in High Barnet')

@app.route('/digital-marketing-training-courses-classes-in-barnsbury')
def barnsbury_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Barnsbury')

@app.route('/digital-marketing-training-courses-classes-in-battersea')
def battersea_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Battersea')

@app.route('/digital-marketing-training-courses-classes-in-bayswater')
def bayswater_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Bayswater')

@app.route('/digital-marketing-training-courses-classes-in-beckenham')
def beckenham_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Beckenham')

@app.route('/digital-marketing-training-courses-classes-in-beckton')
def beckton_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Beckton')

@app.route('/digital-marketing-training-courses-classes-in-becontree')
def becontree_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Becontree')

@app.route('/digital-marketing-training-courses-classes-in-becontree-heath')
def becontree_heath_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Becontree Heath')

@app.route('/digital-marketing-training-courses-classes-in-beddington')
def beddington_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Beddington')

@app.route('/digital-marketing-training-courses-classes-in-bedford-park')
def bedford_park_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Bedford Park')

@app.route('/digital-marketing-training-courses-classes-in-belgravia')
def belgravia_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Belgravia')

@app.route('/digital-marketing-training-courses-classes-in-bellingham')
def bellingham_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Bellingham')

@app.route('/digital-marketing-training-courses-classes-in-belmont')
def belmont_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Belmont')

@app.route('/digital-marketing-training-courses-classes-in-belsize-park')
def belsize_park_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Belsize Park')

@app.route('/digital-marketing-training-courses-classes-in-belvedere')
def belvedere_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Belvedere')

@app.route('/digital-marketing-training-courses-classes-in-bermondsey')
def bermondsey_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Bermondsey')

@app.route('/digital-marketing-training-courses-classes-in-berrylands')
def berrylands_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Berrylands')

@app.route('/digital-marketing-training-courses-classes-in-bethnal-green')
def bethnal_green_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Bethnal Green')

@app.route('/digital-marketing-training-courses-classes-in-bexley')
def bexley_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Bexley')

@app.route('/digital-marketing-training-courses-classes-in-bexley-village')
def bexley_village_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Bexley Village')

@app.route('/digital-marketing-training-courses-classes-in-old-bexley')
def old_bexley_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Old Bexley')

@app.route('/digital-marketing-training-courses-classes-in-bexleyheath')
def bexleyheath_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Bexleyheath')

@app.route('/digital-marketing-training-courses-classes-in-bexley-new-town')
def bexley_new_town_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Bexley New Town')

@app.route('/digital-marketing-training-courses-classes-in-bickley')
def bickley_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Bickley')

@app.route('/digital-marketing-training-courses-classes-in-biggin-hill')
def biggin_hill_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Biggin Hill')

@app.route('/digital-marketing-training-courses-classes-in-blackfen')
def blackfen_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Blackfen')

@app.route('/digital-marketing-training-courses-classes-in-blackfriars')
def blackfriars_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Blackfriars')

@app.route('/digital-marketing-training-courses-classes-in-blackheath')
def blackheath_digital_marketing_training_course():
    return render_template('landing/london.html', data='digital marketing training course in Blackheath')

