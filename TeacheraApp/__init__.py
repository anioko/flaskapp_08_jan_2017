from app import app

from TeacheraApp import *

from .croydon import croydon as croydon_blueprint
app.register_blueprint(croydon_blueprint, url_prefix='/croydon')

from .nigeria import nigeria as nigeria_blueprint
app.register_blueprint(nigeria_blueprint)

from .london import london as london_blueprint
app.register_blueprint(london_blueprint)