
from flask import abort, flash, redirect, render_template, url_for, request, send_from_directory, current_app, make_response
from . import croydon

from datetime import datetime, timedelta
from jinja2 import Environment, FileSystemLoader
#from flask import send_from_directory, current_app, make_response
from flask_sitemap import Sitemap

@croydon.route('/courses-in-Croydon')
def sitemap():
    return render_template('sitemap_city.html', title="learn how to code" ,
                           keyword1="JavaScript" ,
                           keyword2="c++" ,
                           keyword3="c#" ,
                           keyword4="Ruby" ,
                           keyword5="Python" ,
                           keyword6="HTML5" ,
                           keyword7="Java" ,
                           keyword8="Perl" ,
                           keyword9="Php" ,
                           title1="learn how to code" ,
                           location="Croydon")




###############Level One URLS ############

@croydon.route('/learn-to-code-c++-Croydon')
def tech_keyword_learn_1():
    return render_template('landing_city_keyword_london.html', title="learn how to code" , keyword="c++" , location="Croydon")
@croydon.route('/learn-to-code-c#-Croydon')
def tech_keyword_learn_2():
    return render_template('landing_city_keyword_london.html', title="learn how to code" , keyword="c#" , location="Croydon")
@croydon.route('/learn-to-code-Ruby-Croydon')
def tech_keyword_learn_3():
    return render_template('landing_city_keyword_london.html', title="learn how to code" , keyword="Ruby" , location="Croydon")
@croydon.route('/learn-to-code-Python-Croydon')
def tech_keyword_learn_4():
    return render_template('landing_city_keyword_london.html', title="learn how to code" , keyword="Python" , location="Croydon")
@croydon.route('/learn-to-code-HTML5-Croydon')
def tech_keyword_learn_5():
    return render_template('landing_city_keyword_london.html', title="learn how to code" , keyword="HTML5" , location="Croydon")
@croydon.route('/learn-to-code-Java-Croydon')
def tech_keyword_learn_6():
    return render_template('landing_city_keyword_london.html', title="learn how to code" , keyword="Java" , location="Croydon")
@croydon.route('/learn-to-code-Perl-Croydon')
def tech_keyword_learn_7():
    return render_template('landing_city_keyword_london.html', title="learn how to code" , keyword="Perl" , location="Croydon")
@croydon.route('/learn-to-code-Php-Croydon')
def tech_keyword_learn_8():
    return render_template('landing_city_keyword_london.html', title="learn how to code" , keyword="Php" , location="Croydon")
    
@croydon.route('/learn-to-code-JavaScript-Croydon')
def tech_keyword_learn_9():
    return render_template('landing_city_keyword_london.html', title="learn how to code" , keyword="JavaScript" , location="Croydon")

##### Programmming Level 2 Keywords ###
@croydon.route('/python-croydon')
def programming_keyword_croydon_1():
    return render_template('landing.html', title="python " , keyword="classes" , location="croydon")
@croydon.route('/ruby-croydon')
def programming_keyword_croydon_2():
    return render_template('landing.html', title="ruby " , keyword="classes" , location="croydon")
@croydon.route('/html5-croydon')
def programming_keyword_croydon_3():
    return render_template('landing.html', title="html5 " , keyword="classes" , location="croydon")
@croydon.route('/java-croydon')
def programming_keyword_croydon_4():
    return render_template('landing.html', title="java " , keyword="classes" , location="croydon")
@croydon.route('/django-croydon')
def programming_keyword_croydon_5():
    return render_template('landing.html', title="django " , keyword="classes" , location="croydon")
@croydon.route('/javascript-croydon')
def programming_keyword_croydon_6():
    return render_template('landing.html', title="javascript " , keyword="classes" , location=" croydon")
@croydon.route('/learn-to-code-croydon')
def programming_keyword_croydon_7():
    return render_template('landing.html', title="learn to" , keyword="code" , location="croydon")
@croydon.route('/learn-to-code-c-sharp-croydon')
def programming_keyword_croydon_8():
    return render_template('landing.html', title="learn to code" , keyword="c sharp" , location="croydon")
@croydon.route('/learn-to-code-c++-croydon')
def programming_keyword_croydon_9():
    return render_template('landing.html', title="learn to code" , keyword="c++" , location="croydon")
@croydon.route('/learn-programming-in-croydon')
def programming_keyword_croydon_10():
    return render_template('landing.html', title="learn" , keyword="programming" , location="croydon")
@croydon.route('/learn-software-development-in-croydon')
def programming_keyword_croydon_11():
    return render_template('landing.html', title="learn" , keyword="software development" , location="croydon")
@croydon.route('/programming-classes-in-croydon')
def programming_keyword_croydon_12():
    return render_template('landing.html', title="programming" , keyword="classes" , location="croydon")
@croydon.route('/programming-courses-in-croydon')
def programming_keyword_croydon_13():
    return render_template('landing.html', title="programming" , keyword="courses" , location="croydon")
@croydon.route('/intensive-programming-classes-in-croydon')
def programming_keyword_croydon_14():
    return render_template('landing.html', title="intensive programming" , keyword="classes" , location="croydon")
@croydon.route('/cooking-course-croydon')
def cooking_keyword_croydon_1():
    return render_template('landing.html', title="cooking" , keyword="course" , location="croydon")
@croydon.route('/photography-course-croydon')
def photography_keyword_croydon_1():
    return render_template('landing.html', title="photography" , keyword="course" , location="croydon")
@croydon.route('/art-course-croydon')
def art_keyword_croydon_1():
    return render_template('landing.html', title="art" , keyword="course" , location="croydon")
@croydon.route('/group-exercise-classes-croydon')
def exercise_keyword_croydon_1():
    return render_template('landing.html', title="group" , keyword="exercise classes" , location="croydon")


