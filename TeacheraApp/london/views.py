
from flask import abort, flash, redirect, render_template, url_for, request, send_from_directory, current_app, make_response
from . import london


@london.route('/business-analyst-vacancies-list-london')
def london_business_analyst_sitemap_london():
    return render_template('sitemap_london_business_analyst_trainer.html')

@london.route('/data-analyst-vacancies-list-london')
def london_data_analyst_sitemap_london():
    return render_template('sitemap_london_data_analyst_trainer.html')
##london cities Keywords ###


######London Keyword Begins ####


###Business Analyst Trainers####

@london.route('/business-analyst-trainer-part-time-vacancies-and-jobs-in-london')
def london_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in London')

@london.route('/business-analyst-trainer-part-time-vacancies-abbey-wood')
def abbey_wood_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Abbey Wood')

@london.route('/business-analyst-trainer-part-time-vacancies-acton')
def acton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Acton')

@london.route('/business-analyst-trainer-part-time-vacancies-addington')
def addington_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Addington')

@london.route('/business-analyst-trainer-part-time-vacancies-addiscombe')
def addiscombe_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Addiscombe')

@london.route('/business-analyst-trainer-part-time-vacancies-aldborough-aatch')
def aldborough_hatch_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Aldborough Hatch')

@london.route('/business-analyst-trainer-part-time-vacancies-aldgate')
def aldgate_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Aldgate')

@london.route('/business-analyst-trainer-part-time-vacancies-aldwych')
def aldwych_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Aldwych')

@london.route('/business-analyst-trainer-part-time-vacancies-alperton')
def alperton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Alperton')

@london.route('/business-analyst-trainer-part-time-vacancies-anerley')
def anerley_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Anerley')

@london.route('/business-analyst-trainer-part-time-vacancies-angel')
def angel_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Angel')

@london.route('/business-analyst-trainer-part-time-vacancies-aperfield')
def aperfield_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Aperfield')

@london.route('/business-analyst-trainer-part-time-vacancies-archway')
def archway_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Archway')

@london.route('/business-analyst-trainer-part-time-vacancies-ardleigh-green')
def ardleigh_green_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Ardleigh Green')

@london.route('/business-analyst-trainer-part-time-vacancies-arkley')
def arkley_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Arkley')

@london.route('/business-analyst-trainer-part-time-vacancies-arnos-arove')
def arnos_grove_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Arnos Grove')

@london.route('/business-analyst-trainer-part-time-vacancies-balham')
def balham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Balham')

@london.route('/business-analyst-trainer-part-time-vacancies-bankside')
def bankside_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Bankside')

@london.route('/business-analyst-trainer-part-time-vacancies-barbican')
def barbican_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Barbica')

@london.route('/business-analyst-trainer-part-time-vacancies-barking')
def barking_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Barking')

@london.route('/business-analyst-trainer-part-time-vacancies-barkingside')
def barkingside_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Barkingside')

@london.route('/business-analyst-trainer-part-time-vacancies-barnehurst')
def barnehurst_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Barnehurst')

@london.route('/business-analyst-trainer-part-time-vacancies-barnes')
def barnes_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Barnes')

@london.route('/business-analyst-trainer-part-time-vacancies-barnes-cray')
def barnes_cray_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Barnes Cray')

@london.route('/business-analyst-trainer-part-time-vacancies-barnet')
def barnet_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Barnet')

@london.route('/business-analyst-trainer-part-time-vacancies-chipping-barnet')
def chipping_barnet_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Chipping Barnet')

@london.route('/business-analyst-trainer-part-time-vacancies-high_barnet')
def high_barnet_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in High Barnet')

@london.route('/business-analyst-trainer-part-time-vacancies-barnsbury')
def barnsbury_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Barnsbury')

@london.route('/business-analyst-trainer-part-time-vacancies-battersea')
def battersea_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Battersea')

@london.route('/business-analyst-trainer-part-time-vacancies-bayswater')
def bayswater_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Bayswater')

@london.route('/business-analyst-trainer-part-time-vacancies-beckenham')
def beckenham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Beckenham')

@london.route('/business-analyst-trainer-part-time-vacancies-beckton')
def beckton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Beckton')

@london.route('/business-analyst-trainer-part-time-vacancies-becontree')
def becontree_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Becontree')

@london.route('/business-analyst-trainer-part-time-vacancies-becontree-heath')
def becontree_heath_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Becontree Heath')

@london.route('/business-analyst-trainer-part-time-vacancies-beddington')
def beddington_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Beddington')

@london.route('/business-analyst-trainer-part-time-vacancies-bedford-park')
def bedford_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Bedford Park')

@london.route('/business-analyst-trainer-part-time-vacancies-belgravia')
def belgravia_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Belgravia')

@london.route('/business-analyst-trainer-part-time-vacancies-bellingham')
def bellingham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Bellingham')

@london.route('/business-analyst-trainer-part-time-vacancies-belmont')
def belmont_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Belmont')

@london.route('/business-analyst-trainer-part-time-vacancies-belsize-park')
def belsize_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Belsize Park')

@london.route('/business-analyst-trainer-part-time-vacancies-belvedere')
def belvedere_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Belvedere')

@london.route('/business-analyst-trainer-part-time-vacancies-bermondsey')
def bermondsey_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Bermondsey')

@london.route('/business-analyst-trainer-part-time-vacancies-berrylands')
def berrylands_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Berrylands')

@london.route('/business-analyst-trainer-part-time-vacancies-bethnal-green')
def bethnal_green_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Bethnal Green')

@london.route('/business-analyst-trainer-part-time-vacancies-bexley')
def bexley_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Bexley')

@london.route('/business-analyst-trainer-part-time-vacancies-bexley-village')
def bexley_village_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Bexley Village')

@london.route('/business-analyst-trainer-part-time-vacancies-old-bexley')
def old_bexley_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Old Bexley')

@london.route('/business-analyst-trainer-part-time-vacancies-bexleyheath')
def bexleyheath_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Bexleyheath')

@london.route('/business-analyst-trainer-part-time-vacancies-bexley-new-town')
def bexley_new_town_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Bexley New Town')

@london.route('/business-analyst-trainer-part-time-vacancies-bickley')
def bickley_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Bickley')

@london.route('/business-analyst-trainer-part-time-vacancies-biggin-hill')
def biggin_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Biggin Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-blackfen')
def blackfen_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Blackfen')

@london.route('/business-analyst-trainer-part-time-vacancies-blackfriars')
def blackfriars_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Blackfriars')

@london.route('/business-analyst-trainer-part-time-vacancies-blackheath')
def blackheath_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Blackheath')

@london.route('/business-analyst-trainer-part-time-vacancies-blackheath-royal-standard')
def blackheath_royal_standard_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Blackheath Royal Standard')

@london.route('/business-analyst-trainer-part-time-vacancies-blackwall')
def blackwall_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Blackwall')

@london.route('/business-analyst-trainer-part-time-vacancies-blendon')
def blendon_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Blendon')

@london.route('/business-analyst-trainer-part-time-vacancies-bloomsbury')
def bloomsbury_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Bloomsbury')

@london.route('/business-analyst-trainer-part-time-vacancies-botany-bay')
def botany_bay_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Botany Bay')

@london.route('/business-analyst-trainer-part-time-vacancies-bounds-green')
def bounds_green_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Bounds Green')

@london.route('/business-analyst-trainer-part-time-vacancies-bow')
def bow_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Bow')

@london.route('/business-analyst-trainer-part-time-vacancies-bowes-park')
def bowes_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Bowes Park')

@london.route('/business-analyst-trainer-part-time-vacancies-brentford')
def brentford_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Brentford')

@london.route('/business-analyst-trainer-part-time-vacancies-brent-cross')
def brent_cross_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Brent Cross')

@london.route('/business-analyst-trainer-part-time-vacancies-brent-park')
def brent_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Brent Park')

@london.route('/business-analyst-trainer-part-time-vacancies-brimsdown')
def brimsdown_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Brimsdown')

@london.route('/business-analyst-trainer-part-time-vacancies-brixton')
def brixton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Brixton')

@london.route('/business-analyst-trainer-part-time-vacancies-brockley')
def brockley_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Brockley')

@london.route('/business-analyst-trainer-part-time-vacancies-bromley')
def bromley_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Bromley')

@london.route('/business-analyst-trainer-part-time-vacancies-bromley-by-bow')
def bromley_by_Bow_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Bromley by Bow')

@london.route('/business-analyst-trainer-part-time-vacancies-bromley-common')
def bromley_common_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Bromley Common')

@london.route('/business-analyst-trainer-part-time-vacancies-brompton')
def brompton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Brompton')

@london.route('/business-analyst-trainer-part-time-vacancies-brondesbury')
def brondesbury_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Brondesbury')

@london.route('/business-analyst-trainer-part-time-vacancies-brunswick-park')
def brunswick_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Brunswick Park')

@london.route('/business-analyst-trainer-part-time-vacancies-bulls-cross')
def bulls_cross_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Bulls Cross')

@london.route('/business-analyst-trainer-part-time-vacancies-burnt-oak')
def burnt_oak_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Burnt Oak')

@london.route('/business-analyst-trainer-part-time-vacancies-burroughs')
def burroughs_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Burroughs')

@london.route('/business-analyst-trainer-part-time-vacancies-the-camberwell')
def the_camberwell_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in The Camberwell')

@london.route('/business-analyst-trainer-part-time-vacancies-cambridge-heath')
def cambridge_heath_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Cambridge Heath')

@london.route('/business-analyst-trainer-part-time-vacancies-camden-town')
def camden_town_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Camden Town')

@london.route('/business-analyst-trainer-part-time-vacancies-canary-wharf')
def canary_wharf_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Canary Wharf')

@london.route('/business-analyst-trainer-part-time-vacancies-cann-hall')
def cann_hall_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Cann Hall')

@london.route('/business-analyst-trainer-part-time-vacancies-canning-town')
def canning_town_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Canning Town')

@london.route('/business-analyst-trainer-part-time-vacancies-canonbury')
def canonbury_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Canonbury')

@london.route('/business-analyst-trainer-part-time-vacancies-carshalton')
def carshalton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Carshalton')

@london.route('/business-analyst-trainer-part-time-vacancies-castelnau')
def castelnau_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Castelnau')

@london.route('/business-analyst-trainer-part-time-vacancies-catford')
def catford_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Catford')

@london.route('/business-analyst-trainer-part-time-vacancies-chadwell-heath')
def chadwell_heath_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Chadwell Heath')

@london.route('/business-analyst-trainer-part-time-vacancies-chalk-farm')
def chalk_farm_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Chalk Farm')

@london.route('/business-analyst-trainer-part-time-vacancies-charing-cross')
def charing_cross_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Charing Cross')

@london.route('/business-analyst-trainer-part-time-vacancies-charlton')
def charlton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Charlton')

@london.route('/business-analyst-trainer-part-time-vacancies-chase-cross')
def chase_cross_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Chase Cross')

@london.route('/business-analyst-trainer-part-time-vacancies-cheam')
def cheam_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Cheam')

@london.route('/business-analyst-trainer-part-time-vacancies-chelsea')
def chelsea_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Chelsea')

@london.route('/business-analyst-trainer-part-time-vacancies-chelsfield')
def chelsfield_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Chelsfield')

@london.route('/business-analyst-trainer-part-time-vacancies-chessington')
def chessington_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Chessington')

@london.route('/business-analyst-trainer-part-time-vacancies-childs-hill')
def childs_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Childs Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-chinatown')
def chinatown_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Chinatown')

@london.route('/business-analyst-trainer-part-time-vacancies-chinbrook')
def chinbrook_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Chinbrook')

@london.route('/business-analyst-trainer-part-time-vacancies-chingford')
def chingford_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Chingford')

@london.route('/business-analyst-trainer-part-time-vacancies-chislehurst')
def chislehurst_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Chislehurst')

@london.route('/business-analyst-trainer-part-time-vacancies-chiswick')
def chiswick_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Chiswick')

@london.route('/business-analyst-trainer-part-time-vacancies-church-end')
def church_end_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Church End')

@london.route('/business-analyst-trainer-part-time-vacancies-clapham')
def clapham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Clapham')

@london.route('/business-analyst-trainer-part-time-vacancies-clerkenwell')
def clerkenwell_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Clerkenwell')

@london.route('/business-analyst-trainer-part-time-vacancies-cockfosters')
def cockfosters_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Cockfosters')

@london.route('/business-analyst-trainer-part-time-vacancies-colindale')
def colindale_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Colindale')

@london.route('/business-analyst-trainer-part-time-vacancies-collier-row')
def collier_row_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Collier Row')

@london.route('/business-analyst-trainer-part-time-vacancies-colliers-wood')
def colliers_wood_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Colliers Wood')

@london.route('/business-analyst-trainer-part-time-vacancies-colney-hatch')
def colney_hatch_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Colney Hatch')

@london.route('/business-analyst-trainer-part-time-vacancies-colyers')
def colyers_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Colyers')

@london.route('/business-analyst-trainer-part-time-vacancies-coney-hall')
def coney_hall_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Coney Hall')

@london.route('/business-analyst-trainer-part-time-vacancies-coombe')
def coombe_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Coombe')

@london.route('/business-analyst-trainer-part-time-vacancies-coulsdon')
def coulsdon_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Coulsdon')

@london.route('/business-analyst-trainer-part-time-vacancies-covent-garden')
def covent_garden_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Covent Garden')

@london.route('/business-analyst-trainer-part-time-vacancies-cowley')
def cowley_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Cowley')

@london.route('/business-analyst-trainer-part-time-vacancies-cranford')
def cranford_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Cranford')

@london.route('/business-analyst-trainer-part-time-vacancies-cranham')
def cranham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Cranham')

@london.route('/business-analyst-trainer-part-time-vacancies-crayford')
def crayford_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Crayford')

@london.route('/business-analyst-trainer-part-time-vacancies-creekmouth')
def creekmouth_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Creekmouth')

@london.route('/business-analyst-trainer-part-time-vacancies-crews-hill')
def crews_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Crews Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-cricklewood')
def cricklewood_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Cricklewood')

@london.route('/business-analyst-trainer-part-time-vacancies-crofton-park')
def crofton_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Crofton Park')

@london.route('/business-analyst-trainer-part-time-vacancies-crook-log')
def crook_log_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Crook Log')

@london.route('/business-analyst-trainer-part-time-vacancies-crossness')
def crossness_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Crossness')

@london.route('/business-analyst-trainer-part-time-vacancies-crouch-end')
def crouch_end_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Crouch End')

@london.route('/business-analyst-trainer-part-time-vacancies-croydon')
def croydon_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Croydon')

@london.route('/business-analyst-trainer-part-time-vacancies-crystal-palace')
def crystal_palace_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Crystal Palace')

@london.route('/business-analyst-trainer-part-time-vacancies-cubitt-town')
def cubitt_town_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Cubitt Town')

@london.route('/business-analyst-trainer-part-time-vacancies-cudham')
def cudham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Cudham')

@london.route('/business-analyst-trainer-part-time-vacancies-custom-house')
def custom_house_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Custom House')

@london.route('/business-analyst-trainer-part-time-vacancies-dagenham')
def dagenham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Dagenham')

@london.route('/business-analyst-trainer-part-time-vacancies-dalston')
def dalston_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Dalston')

@london.route('/business-analyst-trainer-part-time-vacancies-dartmouth-park')
def dartmouth_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Dartmouth Park')

@london.route('/business-analyst-trainer-part-time-vacancies-de-beauvoir-town')
def de_beauvoir_town_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in De Beauvoir Town')

@london.route('/business-analyst-trainer-part-time-vacancies-denmark-hill')
def denmark_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Denmark Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-deptford')
def deptford_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Deptford')

@london.route('/business-analyst-trainer-part-time-vacancies-derry-downs')
def derry_downs_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Derry Downs')

@london.route('/business-analyst-trainer-part-time-vacancies-dollis-hill')
def dollis_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Dollis Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-downe')
def downe_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Downe')

@london.route('/business-analyst-trainer-part-time-vacancies-downham')
def downham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Downham')

@london.route('/business-analyst-trainer-part-time-vacancies-dulwich')
def dulwich_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Dulwich')

@london.route('/business-analyst-trainer-part-time-vacancies-ealing')
def ealing_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Ealing')

@london.route('/business-analyst-trainer-part-time-vacancies-earls-court')
def earls_court_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Earls Court')

@london.route('/business-analyst-trainer-part-time-vacancies-earlsfield')
def earlsfield_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Earlsfield')

@london.route('/business-analyst-trainer-part-time-vacancies-east-barnet')
def east_barnet_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in East Barnet')

@london.route('/business-analyst-trainer-part-time-vacancies-east-bedfont')
def east_bedfont_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in East Bedfont')

@london.route('/business-analyst-trainer-part-time-vacancies-east-dulwich')
def east_dulwich_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in East Dulwich')

@london.route('/business-analyst-trainer-part-time-vacancies-east-finchley')
def east_finchley_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in East Finchley')

@london.route('/business-analyst-trainer-part-time-vacancies-east-ham')
def east_ham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in East Ham')

@london.route('/business-analyst-trainer-part-time-vacancies-east-sheen')
def east_sheen_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in East Sheen')

@london.route('/business-analyst-trainer-part-time-vacancies-eastcote')
def eastcote_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Eastcote')

@london.route('/business-analyst-trainer-part-time-vacancies-eden-park')
def eden_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Eden Park')

@london.route('/business-analyst-trainer-part-time-vacancies-edgware')
def edgware_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Edgware')

@london.route('/business-analyst-trainer-part-time-vacancies-edmonton')
def Edmonton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Edmonton')

@london.route('/business-analyst-trainer-part-time-vacancies-eel-pie-island')
def eel_pie_island_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Eel Pie Island')

@london.route('/business-analyst-trainer-part-time-vacancies-elephant-and-castle')
def elephant_and_castle_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Elephant and Castle')

@london.route('/business-analyst-trainer-part-time-vacancies-elm-park')
def elm_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Elm Park')

@london.route('/business-analyst-trainer-part-time-vacancies-elmers-end')
def elmers_end_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Elmers End')

@london.route('/business-analyst-trainer-part-time-vacancies-elmstead')
def elmstead_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Elmstead')

@london.route('/business-analyst-trainer-part-time-vacancies-eltham')
def eltham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Eltham')

@london.route('/business-analyst-trainer-part-time-vacancies-emerson-park')
def emerson_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Emerson Park')

@london.route('/business-analyst-trainer-part-time-vacancies-enfield-highway')
def enfield_highway_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Enfield Highway')

@london.route('/business-analyst-trainer-part-time-vacancies-enfield-town')
def enfield_town_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Enfield Town')

@london.route('/business-analyst-trainer-part-time-vacancies-enfield-wash')
def enfield_wash_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Enfield Wash')

@london.route('/business-analyst-trainer-part-time-vacancies-erith')
def erith_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Erith')

@london.route('/business-analyst-trainer-part-time-vacancies-falconwood')
def falconwood_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Falconwood')

@london.route('/business-analyst-trainer-part-time-vacancies-farringdon')
def farringdon_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Farringdon')

@london.route('/business-analyst-trainer-part-time-vacancies-feltham')
def feltham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Feltham')

@london.route('/business-analyst-trainer-part-time-vacancies-finchley')
def finchley_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Finchley')

@london.route('/business-analyst-trainer-part-time-vacancies-finsbury')
def finsbury_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Finsbury')

@london.route('/business-analyst-trainer-part-time-vacancies-finsbury-park')
def finsbury_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Finsbury Park')

@london.route('/business-analyst-trainer-part-time-vacancies-fitzrovia')
def fitzrovia_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Fitzrovia')

@london.route('/business-analyst-trainer-part-time-vacancies-foots-cray')
def foots_cray_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Foots Cray')

@london.route('/business-analyst-trainer-part-time-vacancies-forest-gate')
def forest_gate_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Forest Gate')

@london.route('/business-analyst-trainer-part-time-vacancies-forest-hill')
def forest_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Forest Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-forestdale')
def forestdale_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Forestdale')

@london.route('/business-analyst-trainer-part-time-vacancies-fortis-green')
def fortis_green_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Fortis Green')

@london.route('/business-analyst-trainer-part-time-vacancies-freezywater')
def freezywater_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Freezywater')

@london.route('/business-analyst-trainer-part-time-vacancies-friern-barnet')
def friern_barnet_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Friern Barnet')

@london.route('/business-analyst-trainer-part-time-vacancies-frognal')
def frognal_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Frognal')

@london.route('/business-analyst-trainer-part-time-vacancies-fulham')
def fulham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Fulham')

@london.route('/business-analyst-trainer-part-time-vacancies-fulwell')
def fulwell_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Fulwell')

@london.route('/business-analyst-trainer-part-time-vacancies-gallows-corner')
def gallows_corner_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Gallows Corner')

@london.route('/business-analyst-trainer-part-time-vacancies-gants-hill')
def gants_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Gants Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-gidea-park')
def gidea_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Gidea Park')

@london.route('/business-analyst-trainer-part-time-vacancies-gipsy-hill')
def gipsy_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Gipsy Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-golders-green')
def golders_green_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Golders Green')

@london.route('/business-analyst-trainer-part-time-vacancies-goodmayes')
def goodmayes_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Goodmayes')

@london.route('/business-analyst-trainer-part-time-vacancies-gospel-oak')
def gospel_oak_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Gospel Oak')

@london.route('/business-analyst-trainer-part-time-vacancies-grahame-park')
def grahame_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Grahame Park')

@london.route('/business-analyst-trainer-part-time-vacancies-grange-park')
def grange_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Grange Park')

@london.route('/business-analyst-trainer-part-time-vacancies-greenford')
def greenford_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Greenford')

@london.route('/business-analyst-trainer-part-time-vacancies-greenwich')
def greenwich_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Greenwich')

@london.route('/business-analyst-trainer-part-time-vacancies-grove-park')
def grove_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Grove Park')

@london.route('/business-analyst-trainer-part-time-vacancies-gunnersbury')
def gunnersbury_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Gunnersbury')

@london.route('/business-analyst-trainer-part-time-vacancies-hackney')
def hackney_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hackney')

@london.route('/business-analyst-trainer-part-time-vacancies-hackney-marshes')
def hackney_marshes_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hackney Marshes')

@london.route('/business-analyst-trainer-part-time-vacancies-hackney-wick')
def hackney_wick_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hackney Wick')

@london.route('/business-analyst-trainer-part-time-vacancies-hadley-wood')
def hadley_wood_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hadley Wood')

@london.route('/business-analyst-trainer-part-time-vacancies-haggerston')
def haggerston_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Haggerston')

@london.route('/business-analyst-trainer-part-time-vacancies-hainault')
def hainault_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hainault')

@london.route('/business-analyst-trainer-part-time-vacancies-the-hale')
def the_hale_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in The Hale')

@london.route('/business-analyst-trainer-part-time-vacancies-ham')
def ham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Ham')

@london.route('/business-analyst-trainer-part-time-vacancies-hammersmith')
def hammersmith_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hammersmith')

@london.route('/business-analyst-trainer-part-time-vacancies-hampstead')
def hampstead_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hampstead')

@london.route('/business-analyst-trainer-part-time-vacancies-hampstead-garden-suburb')
def hampstead_garden_suburb_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hampstead Garden Suburb')

@london.route('/business-analyst-trainer-part-time-vacancies-hampton')
def hampton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hampton')

@london.route('/business-analyst-trainer-part-time-vacancies-hampton-hill')
def hampton_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hampton Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-hampton-wick')
def hampton_wick_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hampton Wick')

@london.route('/business-analyst-trainer-part-time-vacancies-hanwell')
def hanwell_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hanwell')

@london.route('/business-analyst-trainer-part-time-vacancies-hanworth')
def hanworth_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hanworth')

@london.route('/business-analyst-trainer-part-time-vacancies-harefield')
def harefield_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Harefield')

@london.route('/business-analyst-trainer-part-time-vacancies-harlesden')
def harlesden_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Harlesden')

@london.route('/business-analyst-trainer-part-time-vacancies-harlington')
def harlington_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Harlington')

@london.route('/business-analyst-trainer-part-time-vacancies-harmondsworth')
def harmondsworth_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Harmondsworth')

@london.route('/business-analyst-trainer-part-time-vacancies-harold-hill')
def harold_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Harold Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-harold-park')
def harold_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Harold Park')

@london.route('/business-analyst-trainer-part-time-vacancies-harold-wood')
def harold_wood_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Harold Wood')

@london.route('/business-analyst-trainer-part-time-vacancies-harringay')
def harringay_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Harringay')

@london.route('/business-analyst-trainer-part-time-vacancies-harrow')
def harrow_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Harrow')

@london.route('/business-analyst-trainer-part-time-vacancies-harrow-on-the-hill')
def harrow_on_the_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Harrow on the Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-harrow-weald')
def harrow_weald_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Harrow Weald')

@london.route('/business-analyst-trainer-part-time-vacancies-hatch_end')
def hatch_end_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hatch End')

@london.route('/business-analyst-trainer-part-time-vacancies-hatton')
def hatton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hatton')

@london.route('/business-analyst-trainer-part-time-vacancies-havering-atte-bower')
def havering_atte_bower_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Havering-atte-Bower')

@london.route('/business-analyst-trainer-part-time-vacancies-hayes')
def hayes_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hayes')

@london.route('/business-analyst-trainer-part-time-vacancies-hazelwood')
def hazelwood_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hazelwood')

@london.route('/business-analyst-trainer-part-time-vacancies-hendon')
def hendon_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hendon')

@london.route('/business-analyst-trainer-part-time-vacancies-herne-hill')
def herne_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Herne Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-heston')
def heston_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Heston')

@london.route('/business-analyst-trainer-part-time-vacancies-highams-park')
def highams_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Highams Park')

@london.route('/business-analyst-trainer-part-time-vacancies-highbury')
def highbury_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Highbury')

@london.route('/business-analyst-trainer-part-time-vacancies-highgate')
def highgate_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Highgate')

@london.route('/business-analyst-trainer-part-time-vacancies-hillingdon')
def hillingdon_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hillingdon')

@london.route('/business-analyst-trainer-part-time-vacancies-hither-green')
def hither_green_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hither Green')

@london.route('/business-analyst-trainer-part-time-vacancies-holborn')
def holborn_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Holborn')

@london.route('/business-analyst-trainer-part-time-vacancies-holland-park')
def holland_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Holland Park')

@london.route('/business-analyst-trainer-part-time-vacancies-holloway')
def holloway_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Holloway')

@london.route('/business-analyst-trainer-part-time-vacancies-homerton')
def homerton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Homerton')

@london.route('/business-analyst-trainer-part-time-vacancies-honor-oak')
def honor_oak_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Honor Oak')

@london.route('/business-analyst-trainer-part-time-vacancies-hook')
def hook_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hook')

@london.route('/business-analyst-trainer-part-time-vacancies-hornchurch')
def hornchurch_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hornchurch')

@london.route('/business-analyst-trainer-part-time-vacancies-horn-park')
def hornc_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Horn Park')

@london.route('/business-analyst-trainer-part-time-vacancies-hornsey')
def hornsey_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hornsey')

@london.route('/business-analyst-trainer-part-time-vacancies-hounslow')
def hounslow_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hounslow')

@london.route('/business-analyst-trainer-part-time-vacancies-hoxton')
def hoxton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Hoxton')

@london.route('/business-analyst-trainer-part-time-vacancies-the-hyde')
def the_hyde_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in The Hyde')

@london.route('/business-analyst-trainer-part-time-vacancies-ickenham')
def ickenham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Ickenham')

@london.route('/business-analyst-trainer-part-time-vacancies-ilford')
def ilford_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Ilford')

@london.route('/business-analyst-trainer-part-time-vacancies-isle-of-dogs')
def isle_of_dogs_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Isle of Dogs')

@london.route('/business-analyst-trainer-part-time-vacancies-isleworth')
def isleworth_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Isleworth')

@london.route('/business-analyst-trainer-part-time-vacancies-islington')
def islington_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Islington')

@london.route('/business-analyst-trainer-part-time-vacancies-kenley')
def kenley_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Kenley')

@london.route('/business-analyst-trainer-part-time-vacancies-kennington')
def kennington_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Kennington')

@london.route('/business-analyst-trainer-part-time-vacancies-kensal-green')
def kensal_green_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Kensal Green')

@london.route('/business-analyst-trainer-part-time-vacancies-kensington')
def kensington_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Kensington')

@london.route('/business-analyst-trainer-part-time-vacancies-kentish-town')
def kentish_town_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Kentish Town')

@london.route('/business-analyst-trainer-part-time-vacancies-kenton')
def kenton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Kenton')

@london.route('/business-analyst-trainer-part-time-vacancies-keston')
def keston_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Keston')

@london.route('/business-analyst-trainer-part-time-vacancies-kew')
def kew_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Kew')

@london.route('/business-analyst-trainer-part-time-vacancies-kidbrooke')
def kidbrooke_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Kidbrooke')

@london.route('/business-analyst-trainer-part-time-vacancies-kilburn')
def kilburn_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Kilburn')

@london.route('/business-analyst-trainer-part-time-vacancies-kings-cross')
def kings_cross_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Kings Cross')

@london.route('/business-analyst-trainer-part-time-vacancies-kingsbury')
def kingsbury_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Kingsbury')

@london.route('/business-analyst-trainer-part-time-vacancies-kingston-vale')
def kingston_vale_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Kingston Vale')

@london.route('/business-analyst-trainer-part-time-vacancies-kingston-upon-thames')
def kingston_upon_thames_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Kingston upon Thames')

@london.route('/business-analyst-trainer-part-time-vacancies-knightsbridge')
def knightsbridge_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Knightsbridge')

@london.route('/business-analyst-trainer-part-time-vacancies-ladywell')
def ladywell_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Ladywell')

@london.route('/business-analyst-trainer-part-time-vacancies-lambeth')
def lambeth_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Lambeth')

@london.route('/business-analyst-trainer-part-time-vacancies-lamorbey')
def lamorbey_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Lamorbey')

@london.route('/business-analyst-trainer-part-time-vacancies-lampton')
def lampton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Lampton')

@london.route('/business-analyst-trainer-part-time-vacancies-leamouth')
def leamouth_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Leamouth')

@london.route('/business-analyst-trainer-part-time-vacancies-leaves-green')
def leaves_green_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Leaves Green')

@london.route('/business-analyst-trainer-part-time-vacancies-lee')
def lee_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Lee')

@london.route('/business-analyst-trainer-part-time-vacancies-lewisham')
def lewisham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Lewisham')

@london.route('/business-analyst-trainer-part-time-vacancies-leyton')
def leyton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Leyton')

@london.route('/business-analyst-trainer-part-time-vacancies-leytonstone')
def leytonstone_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Leytonstone')

@london.route('/business-analyst-trainer-part-time-vacancies-limehouse')
def limehouse_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Limehouse')

@london.route('/business-analyst-trainer-part-time-vacancies-lisson-grove')
def lisson_grove_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Lisson Grove')

@london.route('/business-analyst-trainer-part-time-vacancies-little-ilford')
def little_ilford_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Little Ilford')

@london.route('/business-analyst-trainer-part-time-vacancies-locksbottom')
def locksbottom_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Locksbottom')

@london.route('/business-analyst-trainer-part-time-vacancies-longford')
def longford_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Longford')

@london.route('/business-analyst-trainer-part-time-vacancies-longlands')
def longlands_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Longlands')

@london.route('/business-analyst-trainer-part-time-vacancies-lower-clapton')
def lower_clapton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Lower Clapton')

@london.route('/business-analyst-trainer-part-time-vacancies-lower-morden')
def lower_morden_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Lower Morden')

@london.route('/business-analyst-trainer-part-time-vacancies-loxford')
def loxford_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Loxford')

@london.route('/business-analyst-trainer-part-time-vacancies-maida-vale')
def maida_vale_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Maida Vale')

@london.route('/business-analyst-trainer-part-time-vacancies-malden-rushett')
def malden_rushett_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Malden Rushett')

@london.route('/business-analyst-trainer-part-time-vacancies-manor-house')
def manor_house_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Manor House')

@london.route('/business-analyst-trainer-part-time-vacancies-manor-park')
def manor_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Manor Park')

@london.route('/business-analyst-trainer-part-time-vacancies-marks-gate')
def marks_gate_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Marks Gate')

@london.route('/business-analyst-trainer-part-time-vacancies-maryland')
def maryland_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Maryland')

@london.route('/business-analyst-trainer-part-time-vacancies-st-marylebone')
def st_marylebone_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in St Marylebone')

@london.route('/business-analyst-trainer-part-time-vacancies-marylebone')
def marylebone_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Marylebone')

@london.route('/business-analyst-trainer-part-time-vacancies-mayfair')
def mayfair_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Mayfair')

@london.route('/business-analyst-trainer-part-time-vacancies-maze-hill')
def maze_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Maze Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-merton-park')
def merton_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Merton Park')

@london.route('/business-analyst-trainer-part-time-vacancies-middle-park')
def middle_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Middle Park')

@london.route('/business-analyst-trainer-part-time-vacancies-mile-end')
def mile_end_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Mile End')

@london.route('/business-analyst-trainer-part-time-vacancies-mill-hill')
def mill_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Mill Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-millbank')
def millbank_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Millbank')

@london.route('/business-analyst-trainer-part-time-vacancies-millwall')
def millwall_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Millwall')

@london.route('/business-analyst-trainer-part-time-vacancies-mitcham')
def mitcham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Mitcham')

@london.route('/business-analyst-trainer-part-time-vacancies-monken-hadley')
def monken_hadley_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Monken Hadley')

@london.route('/business-analyst-trainer-part-time-vacancies-morden')
def morden_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Morden')

@london.route('/business-analyst-trainer-part-time-vacancies-morden-park')
def morden_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Morden Park')

@london.route('/business-analyst-trainer-part-time-vacancies-mortlake')
def mortlake_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Mortlake')

@london.route('/business-analyst-trainer-part-time-vacancies-motspur-park')
def motspur_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Motspur Park')

@london.route('/business-analyst-trainer-part-time-vacancies-mottingham')
def mottingham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Mottingham')

@london.route('/business-analyst-trainer-part-time-vacancies-muswell-hill')
def muswell_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Muswell Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-nags-head')
def nags_head_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Nags Head')

@london.route('/business-analyst-trainer-part-time-vacancies-neasden')
def neasden_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Neasden')

@london.route('/business-analyst-trainer-part-time-vacancies-new-addington')
def new_addington_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in New Addington')

@london.route('/business-analyst-trainer-part-time-vacancies-new-barnet')
def new_barnet_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in New Barnet')

@london.route('/business-analyst-trainer-part-time-vacancies-new-cross')
def new_cross_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in New Cross')

@london.route('/business-analyst-trainer-part-time-vacancies-new-eltham')
def new_eltham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in New Eltham')

@london.route('/business-analyst-trainer-part-time-vacancies-new-malden')
def new_malden_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in New Malden')

@london.route('/business-analyst-trainer-part-time-vacancies-new-southgate')
def new_southgate_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in New Southgate')

@london.route('/business-analyst-trainer-part-time-vacancies-newbury-park')
def newbury_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Newbury Park')

@london.route('/business-analyst-trainer-part-time-vacancies-newington')
def newington_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Newington')

@london.route('/business-analyst-trainer-part-time-vacancies-nine-elms')
def nine_elms_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Nine Elms')

@london.route('/business-analyst-trainer-part-time-vacancies-noak-hill')
def noak_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Noak Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-norbiton')
def norbiton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Norbiton')

@london.route('/business-analyst-trainer-part-time-vacancies-norbury')
def norbury_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Norbury')

@london.route('/business-analyst-trainer-part-time-vacancies-north-end')
def north_end_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in North End')

@london.route('/business-analyst-trainer-part-time-vacancies-north-finchley')
def north_finchley_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in North Finchley')

@london.route('/business-analyst-trainer-part-time-vacancies-north-harrow')
def north_harrow_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in North Harrow')

@london.route('/business-analyst-trainer-part-time-vacancies-north-kensington')
def north_kensington_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in North Kensington')

@london.route('/business-analyst-trainer-part-time-vacancies-north-ockendon')
def north_ockendon_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in North Ockendon')

@london.route('/business-analyst-trainer-part-time-vacancies-north-sheen')
def north_sheen_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in North Sheen')

@london.route('/business-analyst-trainer-part-time-vacancies-north-woolwich')
def north_woolwich_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in North Woolwich')

@london.route('/business-analyst-trainer-part-time-vacancies-northolt')
def northolt_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Northolt')

@london.route('/business-analyst-trainer-part-time-vacancies-northumberland-heath')
def northumberland_heath_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Northumberland Heath')

@london.route('/business-analyst-trainer-part-time-vacancies-northwood')
def northwood_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Northwood')

@london.route('/business-analyst-trainer-part-time-vacancies-norwood-green')
def norwood_green_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Norwood Green')

@london.route('/business-analyst-trainer-part-time-vacancies-notting-hill')
def notting_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Notting Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-nunhead')
def nunhead_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Nunhead')

@london.route('/business-analyst-trainer-part-time-vacancies-oakleigh-park')
def oakleigh_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Oakleigh Park')

@london.route('/business-analyst-trainer-part-time-vacancies-old-coulsdon')
def old_coulsdon_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Old Coulsdon')

@london.route('/business-analyst-trainer-part-time-vacancies-old-ford')
def old_ford_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Old Ford')

@london.route('/business-analyst-trainer-part-time-vacancies-old-malden')
def old_malden_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Old Malden')

@london.route('/business-analyst-trainer-part-time-vacancies-old-oak-common')
def old_oak_common_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Old Oak Common')

@london.route('/business-analyst-trainer-part-time-vacancies-orpington')
def orpington_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Orpington')

@london.route('/business-analyst-trainer-part-time-vacancies-osidge')
def osidge_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Osidge')

@london.route('/business-analyst-trainer-part-time-vacancies-osterley')
def osterley_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Osterley')

@london.route('/business-analyst-trainer-part-time-vacancies-paddington')
def paddington_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Paddington')

@london.route('/business-analyst-trainer-part-time-vacancies-palmers-green')
def palmers_green_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Palmers Green')

@london.route('/business-analyst-trainer-part-time-vacancies-park-royal')
def park_royal_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Park Royal')

@london.route('/business-analyst-trainer-part-time-vacancies-parsons-green')
def parsons_green_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Parsons Green')

@london.route('/business-analyst-trainer-part-time-vacancies-peckham')
def peckham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Peckham')

@london.route('/business-analyst-trainer-part-time-vacancies-penge')
def penge_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Penge')

@london.route('/business-analyst-trainer-part-time-vacancies-pentonville')
def pentonville_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Pentonville')

@london.route('/business-analyst-trainer-part-time-vacancies-perivale')
def perivale_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Perivale')

@london.route('/business-analyst-trainer-part-time-vacancies-petersham')
def petersham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Petersham')

@london.route('/business-analyst-trainer-part-time-vacancies-petts-wood')
def petts_wood_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Petts Wood')

@london.route('/business-analyst-trainer-part-time-vacancies-pimlico')
def pimlico_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Pimlico')

@london.route('/business-analyst-trainer-part-time-vacancies-pinner')
def pinner_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Pinner')

@london.route('/business-analyst-trainer-part-time-vacancies-plaistow')
def plaistow_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Plaistow')

@london.route('/business-analyst-trainer-part-time-vacancies-plumstead')
def plumstead_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Plumstead')

@london.route('/business-analyst-trainer-part-time-vacancies-ponders_end')
def ponders_end_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Ponders End')

@london.route('/business-analyst-trainer-part-time-vacancies-poplar')
def poplar_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Poplar')

@london.route('/business-analyst-trainer-part-time-vacancies-pratts-bottom')
def pratts_bottom_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Pratts Bottom')

@london.route('/business-analyst-trainer-part-time-vacancies-preston')
def preston_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Preston')

@london.route('/business-analyst-trainer-part-time-vacancies-primrose-hill')
def primrose_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Primrose Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-purley')
def purley_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Purley')

@london.route('/business-analyst-trainer-part-time-vacancies-putney')
def putney_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Putney')

@london.route('/business-analyst-trainer-part-time-vacancies-queens-park')
def queens_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Queens Park')

@london.route('/business-analyst-trainer-part-time-vacancies-queensbury')
def queensbury_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Queensbury')

@london.route('/business-analyst-trainer-part-time-vacancies-rainham')
def rainham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Rainham')

@london.route('/business-analyst-trainer-part-time-vacancies-ratcliff')
def ratcliff_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Ratcliff')

@london.route('/business-analyst-trainer-part-time-vacancies-rayners-lane')
def rayners_lane_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Rayners Lane')

@london.route('/business-analyst-trainer-part-time-vacancies-raynes-park')
def raynes_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Raynes Park')

@london.route('/business-analyst-trainer-part-time-vacancies-redbridge')
def redbridge_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Redbridge')

@london.route('/business-analyst-trainer-part-time-vacancies-richmond')
def richmond_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Richmond')

@london.route('/business-analyst-trainer-part-time-vacancies-riddlesdown')
def riddlesdown_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Riddlesdown')

@london.route('/business-analyst-trainer-part-time-vacancies-roehampton')
def roehampton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Roehampton')

@london.route('/business-analyst-trainer-part-time-vacancies-romford')
def romford_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Romford')

@london.route('/business-analyst-trainer-part-time-vacancies-rotherhithe')
def rotherhithe_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Rotherhithe')

@london.route('/business-analyst-trainer-part-time-vacancies-ruislip')
def ruislip_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Ruislip')

@london.route('/business-analyst-trainer-part-time-vacancies-rush-green')
def rush_green_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Rush Green')

@london.route('/business-analyst-trainer-part-time-vacancies-ruxley')
def ruxley_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Ruxley')

@london.route('/business-analyst-trainer-part-time-vacancies-sanderstead')
def sanderstead_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Sanderstead')

@london.route('/business-analyst-trainer-part-time-vacancies-sands-end')
def sands_end_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Sands End')

@london.route('/business-analyst-trainer-part-time-vacancies-selhurst')
def selhurst_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Selhurst')

@london.route('/business-analyst-trainer-part-time-vacancies-selsdon')
def selsdon_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Selsdon')

@london.route('/business-analyst-trainer-part-time-vacancies-seven-kings')
def seven_kings_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Seven Kings')

@london.route('/business-analyst-trainer-part-time-vacancies-seven-sisters')
def seven_sisters_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Seven Sisters')

@london.route('/business-analyst-trainer-part-time-vacancies-shacklewell')
def shacklewell_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Shacklewell')

@london.route('/business-analyst-trainer-part-time-vacancies-shadwell')
def shadwell_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Shadwell')

@london.route('/business-analyst-trainer-part-time-vacancies-shepherds-bush')
def shepherds_bush_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Shepherds Bush')

@london.route('/business-analyst-trainer-part-time-vacancies-shirley')
def shirley_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Shirley')

@london.route('/business-analyst-trainer-part-time-vacancies-shooters-hill')
def shooters_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Shooters Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-shoreditch')
def shoreditch_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Shoreditch')

@london.route('/business-analyst-trainer-part-time-vacancies-sidcup')
def sidcup_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Sidcup')

@london.route('/business-analyst-trainer-part-time-vacancies-silvertown')
def silvertown_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Silvertown')

@london.route('/business-analyst-trainer-part-time-vacancies-sipson')
def sipson_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Sipson')

@london.route('/business-analyst-trainer-part-time-vacancies-slade-green')
def slade_green_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Slade Green')

@london.route('/business-analyst-trainer-part-time-vacancies-snaresbrook')
def snaresbrook_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Snaresbrook')

@london.route('/business-analyst-trainer-part-time-vacancies-soho')
def soho_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Soho')

@london.route('/business-analyst-trainer-part-time-vacancies-somerstown')
def somerstown_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Somerstown')

@london.route('/business-analyst-trainer-part-time-vacancies-south-croydon')
def south_croydon_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in South Croydon')

@london.route('/business-analyst-trainer-part-time-vacancies-south-hackney')
def south_hackney_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in South Hackney')

@london.route('/business-analyst-trainer-part-time-vacancies-south-harrow')
def south_harrow_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in South Harrow')

@london.route('/business-analyst-trainer-part-time-vacancies-south-hornchurch')
def south_hornchurch_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in South Hornchurch')

@london.route('/business-analyst-trainer-part-time-vacancies-south-kensington')
def south_kensington_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in South Kensington')

@london.route('/business-analyst-trainer-part-time-vacancies-south-norwood')
def south_norwood_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in South Norwood')

@london.route('/business-analyst-trainer-part-time-vacancies-south-ruislip')
def south_ruislip_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in South Ruislip')

@london.route('/business-analyst-trainer-part-time-vacancies-south-wimbledon')
def south_wimbledon_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in South Wimbledon')

@london.route('/business-analyst-trainer-part-time-vacancies-south-woodford')
def south_woodford_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in South Woodford')

@london.route('/business-analyst-trainer-part-time-vacancies-south-tottenham')
def south_tottenham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in South Tottenham')

@london.route('/business-analyst-trainer-part-time-vacancies-southend')
def southend_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Southend')

@london.route('/business-analyst-trainer-part-time-vacancies-southall')
def southall_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Southall')

@london.route('/business-analyst-trainer-part-time-vacancies-southborough')
def southborough_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Southborough')

@london.route('/business-analyst-trainer-part-time-vacancies-southfields')
def southfields_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Southfields')

@london.route('/business-analyst-trainer-part-time-vacancies-southgate')
def southgate_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Southgate')

@london.route('/business-analyst-trainer-part-time-vacancies-spitalfields')
def spitalfields_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Spitalfields')

@london.route('/business-analyst-trainer-part-time-vacancies-st-helier')
def st_helier_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in St Helier')

@london.route('/business-analyst-trainer-part-time-vacancies-st-james')
def st_james_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in St James')

@london.route('/business-analyst-trainer-part-time-vacancies-st-margarets')
def st_margarets_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in St Margarets')

@london.route('/business-analyst-trainer-part-time-vacancies-st-giles')
def st_giles_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in St Giles')

@london.route('/business-analyst-trainer-part-time-vacancies-st-johns')
def st_johns_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in St Johns')

@london.route('/business-analyst-trainer-part-time-vacancies-st-johns-wood')
def st_johns_wood_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in St Johns Wood')

@london.route('/business-analyst-trainer-part-time-vacancies-st-lukes')
def st_lukes_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in St Lukes')

@london.route('/business-analyst-trainer-part-time-vacancies-st-mary-cray')
def st_mary_cray_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in St Mary Cray')

@london.route('/business-analyst-trainer-part-time-vacancies-st-pancras')
def st_pancras_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in St Pancras')

@london.route('/business-analyst-trainer-part-time-vacancies-st-pauls-cray')
def st_pauls_cray_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in St Pauls Cray')

@london.route('/business-analyst-trainer-part-time-vacancies-stamford-hill')
def stamford_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Stamford Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-stanmore')
def stanmore_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Stanmore')

@london.route('/business-analyst-trainer-part-time-vacancies-stepney')
def stepney_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Stepney')

@london.route('/business-analyst-trainer-part-time-vacancies-stockwell')
def stockwell_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Stockwell')

@london.route('/business-analyst-trainer-part-time-vacancies-stoke-newington')
def stoke_newington_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Stoke Newington')

@london.route('/business-analyst-trainer-part-time-vacancies-stratford')
def stratford_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Stratford')

@london.route('/business-analyst-trainer-part-time-vacancies-strawberry-hill')
def strawberry_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Strawberry Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-streatham')
def streatham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Streatham')

@london.route('/business-analyst-trainer-part-time-vacancies-stroud-green')
def stroud_green_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Stroud Green')

@london.route('/business-analyst-trainer-part-time-vacancies-sudbury')
def sudbury_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Sudbury')

@london.route('/business-analyst-trainer-part-time-vacancies-sundridge')
def sundridge_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Sundridge')

@london.route('/business-analyst-trainer-part-time-vacancies-surbiton')
def surbiton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Surbiton')

@london.route('/business-analyst-trainer-part-time-vacancies-surrey-quays')
def surrey_quays_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Surrey Quays')

@london.route('/business-analyst-trainer-part-time-vacancies-sutton')
def sutton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Sutton')

@london.route('/business-analyst-trainer-part-time-vacancies-swiss-cottage')
def swiss_cottage_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Swiss Cottage')

@london.route('/business-analyst-trainer-part-time-vacancies-sydenham')
def sydenham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Sydenham')

@london.route('/business-analyst-trainer-part-time-vacancies-lower-sydenham')
def lower_sydenham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Lower Sydenham')

@london.route('/business-analyst-trainer-part-time-vacancies-upper-sydenham')
def upper_sydenham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Upper Sydenham')

@london.route('/business-analyst-trainer-part-time-vacancies-sydenham-hill')
def sydenham_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Sydenham Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-teddington')
def teddington_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Teddington')

@london.route('/business-analyst-trainer-part-time-vacancies-temple')
def temple_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Temple')

@london.route('/business-analyst-trainer-part-time-vacancies-temple-fortune')
def temple_fortune_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Temple Fortune')

@london.route('/business-analyst-trainer-part-time-vacancies-thamesmead')
def thamesmead_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Thamesmead')

@london.route('/business-analyst-trainer-part-time-vacancies-thornton-heath')
def thornton_heath_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Thornton Heath')

@london.route('/business-analyst-trainer-part-time-vacancies-tokyngton')
def tokyngton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Tokyngton')

@london.route('/business-analyst-trainer-part-time-vacancies-tolworth')
def tolworth_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Tolworth')

@london.route('/business-analyst-trainer-part-time-vacancies-tooting')
def tooting_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Tooting')

@london.route('/business-analyst-trainer-part-time-vacancies-tooting-bec')
def tooting_bec_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Tooting Bec')

@london.route('/business-analyst-trainer-part-time-vacancies-tottenham')
def tottenham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Tottenham')

@london.route('/business-analyst-trainer-part-time-vacancies-tottenham-green')
def tottenham_green_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Tottenham Green')

@london.route('/business-analyst-trainer-part-time-vacancies-tottenham_hale')
def tottenham_hale_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Tottenham Hale')

@london.route('/business-analyst-trainer-part-time-vacancies-totteridge')
def totteridge_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Totteridge')

@london.route('/business-analyst-trainer-part-time-vacancies-tower-hill')
def tower_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Tower Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-tufnell-park')
def tufnell_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Tufnell Park')

@london.route('/business-analyst-trainer-part-time-vacancies-tulse-hill')
def tulse_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Tulse Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-turnpike-lane')
def turnpike_lane_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Turnpike Lane')

@london.route('/business-analyst-trainer-part-time-vacancies-twickenham')
def twickenham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Twickenham')

@london.route('/business-analyst-trainer-part-time-vacancies-upminster')
def upminster_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Upminster')

@london.route('/business-analyst-trainer-part-time-vacancies-upminster-bridge')
def upminster_bridge_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Upminster Bridge')

@london.route('/business-analyst-trainer-part-time-vacancies-upper-clapton')
def upper_clapton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Upper Clapton')

@london.route('/business-analyst-trainer-part-time-vacancies-upper-holloway')
def upper_holloway_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Upper Holloway')

@london.route('/business-analyst-trainer-part-time-vacancies-upper-norwood')
def upper_norwood_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Upper Norwood')

@london.route('/business-analyst-trainer-part-time-vacancies-upper-ruxley')
def upper_ruxley_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Upper Ruxley')

@london.route('/business-analyst-trainer-part-time-vacancies-upper-walthamstow')
def upper_walthamstow_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Upper Walthamstow')

@london.route('/business-analyst-trainer-part-time-vacancies-upton')
def upton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Upton')

@london.route('/business-analyst-trainer-part-time-vacancies-upton-park')
def upton_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Upton Park')

@london.route('/business-analyst-trainer-part-time-vacancies-uxbridge')
def uxbridge_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Uxbridge')

@london.route('/business-analyst-trainer-part-time-vacancies-uauxhall')
def uauxhall_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Vauxhall')

@london.route('/business-analyst-trainer-part-time-vacancies-waddon')
def waddon_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Waddon')

@london.route('/business-analyst-trainer-part-time-vacancies-wallington')
def wallington_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Wallington')

@london.route('/business-analyst-trainer-part-time-vacancies-walthamstow')
def walthamstow_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Walthamstow')

@london.route('/business-analyst-trainer-part-time-vacancies-walthamstow-village')
def walthamstow_village_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Walthamstow Village')

@london.route('/business-analyst-trainer-part-time-vacancies-walworth')
def walworth_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Walworth')

@london.route('/business-analyst-trainer-part-time-vacancies-wandsworth')
def wandsworth_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Wandsworth')

@london.route('/business-analyst-trainer-part-time-vacancies-wanstead')
def wanstead_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Wanstead')

@london.route('/business-analyst-trainer-part-time-vacancies-wapping')
def wapping_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Wapping')

@london.route('/business-analyst-trainer-part-time-vacancies-wealdstone')
def wealdstone_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Wealdstone')

@london.route('/business-analyst-trainer-part-time-vacancies-well-hall')
def well_hall_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Well Hall')

@london.route('/business-analyst-trainer-part-time-vacancies-welling')
def welling_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Welling')

@london.route('/business-analyst-trainer-part-time-vacancies-wembley')
def wembley_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Wembley')

@london.route('/business-analyst-trainer-part-time-vacancies-wembley-park')
def wembley_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Wembley Park')

@london.route('/business-analyst-trainer-part-time-vacancies-wennington')
def wennington_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Wennington')

@london.route('/business-analyst-trainer-part-time-vacancies-west-brompton')
def west_brompton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in West Brompton')

@london.route('/business-analyst-trainer-part-time-vacancies-west-drayton')
def west_drayton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in West Drayton')

@london.route('/business-analyst-trainer-part-time-vacancies-west-ealing')
def west_ealing_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in West Ealing')

@london.route('/business-analyst-trainer-part-time-vacancies-west-green')
def west_green_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in West Green')

@london.route('/business-analyst-trainer-part-time-vacancies-west-ham')
def west_ham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in West Ham')

@london.route('/business-analyst-trainer-part-time-vacancies-west-hampstead')
def west_hampstead_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in West Hampstead')

@london.route('/business-analyst-trainer-part-time-vacancies-west-harrow')
def west_harrow_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in West Harrow')

@london.route('/business-analyst-trainer-part-time-vacancies-west-heath')
def west_heath_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in West Heath')

@london.route('/business-analyst-trainer-part-time-vacancies-west-hendon')
def west_hendon_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in West Hendon')

@london.route('/business-analyst-trainer-part-time-vacancies-west-kensington')
def west_kensington_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in West Kensington')

@london.route('/business-analyst-trainer-part-time-vacancies-west-norwood')
def west_norwood_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in West Norwood')

@london.route('/business-analyst-trainer-part-time-vacancies-west-wickham')
def west_wickham_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in West Wickham')

@london.route('/business-analyst-trainer-part-time-vacancies-westcombe-park')
def westcombe_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Westcombe Park')

@london.route('/business-analyst-trainer-part-time-vacancies-westminster')
def westminster_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Westminster')

@london.route('/business-analyst-trainer-part-time-vacancies-whetstone')
def whetstone_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Whetstone')

@london.route('/business-analyst-trainer-part-time-vacancies-white-city')
def white_city_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in White City')

@london.route('/business-analyst-trainer-part-time-vacancies-whitechapel')
def whitechapel_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Whitechapel')

@london.route('/business-analyst-trainer-part-time-vacancies-widmore')
def widmore_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Widmore')

@london.route('/business-analyst-trainer-part-time-vacancies-widmore-green')
def widmore_green_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Widmore Green')

@london.route('/business-analyst-trainer-part-time-vacancies-whitton')
def whitton_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Whitton')

@london.route('/business-analyst-trainer-part-time-vacancies-willesden')
def willesden_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Willesden')

@london.route('/business-analyst-trainer-part-time-vacancies-wimbledon')
def wimbledon_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Wimbledon')

@london.route('/business-analyst-trainer-part-time-vacancies-winchmore-hill')
def winchmore_hill_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Winchmore Hill')

@london.route('/business-analyst-trainer-part-time-vacancies-wood-green')
def wood_green_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Wood Green')

@london.route('/business-analyst-trainer-part-time-vacancies-woodford')
def woodford_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Woodford')

@london.route('/business-analyst-trainer-part-time-vacancies-woodford-green')
def woodford_green_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Woodford Green')

@london.route('/business-analyst-trainer-part-time-vacancies-woodlands')
def woodlands_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Woodlands')

@london.route('/business-analyst-trainer-part-time-vacancies-woodside')
def woodside_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Woodside')

@london.route('/business-analyst-trainer-part-time-vacancies-woodside-park')
def woodside_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Woodside Park')

@london.route('/business-analyst-trainer-part-time-vacancies-woolwich')
def woolwich_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Woolwich')

@london.route('/business-analyst-trainer-part-time-vacancies-worcester-park')
def worcester_park_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Worcester Park')

@london.route('/business-analyst-trainer-part-time-vacancies-wormwood-scrubs')
def wormwood_scrubs_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Wormwood Scrubs')

@london.route('/business-analyst-trainer-part-time-vacancies-yeading')
def yeading_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Yeading')

@london.route('/business-analyst-trainer-part-time-vacancies-yiewsley')
def yiewsley_business_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='business analyst in Yiewsley')

####Data Analyst ####

######London Keyword Begins ####

@london.route('/data-analyst-trainer-part-time-vacancies-and-jobs-in-london')
def london_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in London')

@london.route('/data-analyst-trainer-part-time-vacancies-abbey-wood')
def abbey_wood_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Abbey Wood')

@london.route('/data-analyst-trainer-part-time-vacancies-acton')
def acton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Acton')

@london.route('/data-analyst-trainer-part-time-vacancies-addington')
def addington_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Addington')

@london.route('/data-analyst-trainer-part-time-vacancies-addiscombe')
def addiscombe_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Addiscombe')

@london.route('/data-analyst-trainer-part-time-vacancies-aldborough-aatch')
def aldborough_hatch_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Aldborough Hatch')

@london.route('/data-analyst-trainer-part-time-vacancies-aldgate')
def aldgate_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Aldgate')

@london.route('/data-analyst-trainer-part-time-vacancies-aldwych')
def aldwych_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Aldwych')

@london.route('/data-analyst-trainer-part-time-vacancies-alperton')
def alperton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Alperton')

@london.route('/data-analyst-trainer-part-time-vacancies-anerley')
def anerley_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Anerley')

@london.route('/data-analyst-trainer-part-time-vacancies-angel')
def angel_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Angel')

@london.route('/data-analyst-trainer-part-time-vacancies-aperfield')
def aperfield_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Aperfield')

@london.route('/data-analyst-trainer-part-time-vacancies-archway')
def archway_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Archway')

@london.route('/data-analyst-trainer-part-time-vacancies-ardleigh-green')
def ardleigh_green_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Ardleigh Green')

@london.route('/data-analyst-trainer-part-time-vacancies-arkley')
def arkley_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Arkley')

@london.route('/data-analyst-trainer-part-time-vacancies-arnos-arove')
def arnos_grove_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Arnos Grove')

@london.route('/data-analyst-trainer-part-time-vacancies-balham')
def balham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Balham')

@london.route('/data-analyst-trainer-part-time-vacancies-bankside')
def bankside_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Bankside')

@london.route('/data-analyst-trainer-part-time-vacancies-barbican')
def barbican_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Barbica')

@london.route('/data-analyst-trainer-part-time-vacancies-barking')
def barking_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Barking')

@london.route('/data-analyst-trainer-part-time-vacancies-barkingside')
def barkingside_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Barkingside')

@london.route('/data-analyst-trainer-part-time-vacancies-barnehurst')
def barnehurst_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Barnehurst')

@london.route('/data-analyst-trainer-part-time-vacancies-barnes')
def barnes_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Barnes')

@london.route('/data-analyst-trainer-part-time-vacancies-barnes-cray')
def barnes_cray_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Barnes Cray')

@london.route('/data-analyst-trainer-part-time-vacancies-barnet')
def barnet_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Barnet')

@london.route('/data-analyst-trainer-part-time-vacancies-chipping-barnet')
def chipping_barnet_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Chipping Barnet')

@london.route('/data-analyst-trainer-part-time-vacancies-high_barnet')
def high_barnet_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in High Barnet')

@london.route('/data-analyst-trainer-part-time-vacancies-barnsbury')
def barnsbury_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Barnsbury')

@london.route('/data-analyst-trainer-part-time-vacancies-battersea')
def battersea_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Battersea')

@london.route('/data-analyst-trainer-part-time-vacancies-bayswater')
def bayswater_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Bayswater')

@london.route('/data-analyst-trainer-part-time-vacancies-beckenham')
def beckenham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Beckenham')

@london.route('/data-analyst-trainer-part-time-vacancies-beckton')
def beckton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Beckton')

@london.route('/data-analyst-trainer-part-time-vacancies-becontree')
def becontree_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Becontree')

@london.route('/data-analyst-trainer-part-time-vacancies-becontree-heath')
def becontree_heath_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Becontree Heath')

@london.route('/data-analyst-trainer-part-time-vacancies-beddington')
def beddington_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Beddington')

@london.route('/data-analyst-trainer-part-time-vacancies-bedford-park')
def bedford_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Bedford Park')

@london.route('/data-analyst-trainer-part-time-vacancies-belgravia')
def belgravia_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Belgravia')

@london.route('/data-analyst-trainer-part-time-vacancies-bellingham')
def bellingham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Bellingham')

@london.route('/data-analyst-trainer-part-time-vacancies-belmont')
def belmont_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Belmont')

@london.route('/data-analyst-trainer-part-time-vacancies-belsize-park')
def belsize_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Belsize Park')

@london.route('/data-analyst-trainer-part-time-vacancies-belvedere')
def belvedere_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Belvedere')

@london.route('/data-analyst-trainer-part-time-vacancies-bermondsey')
def bermondsey_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Bermondsey')

@london.route('/data-analyst-trainer-part-time-vacancies-berrylands')
def berrylands_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Berrylands')

@london.route('/data-analyst-trainer-part-time-vacancies-bethnal-green')
def bethnal_green_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Bethnal Green')

@london.route('/data-analyst-trainer-part-time-vacancies-bexley')
def bexley_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Bexley')

@london.route('/data-analyst-trainer-part-time-vacancies-bexley-village')
def bexley_village_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Bexley Village')

@london.route('/data-analyst-trainer-part-time-vacancies-old-bexley')
def old_bexley_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Old Bexley')

@london.route('/data-analyst-trainer-part-time-vacancies-bexleyheath')
def bexleyheath_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Bexleyheath')

@london.route('/data-analyst-trainer-part-time-vacancies-bexley-new-town')
def bexley_new_town_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Bexley New Town')

@london.route('/data-analyst-trainer-part-time-vacancies-bickley')
def bickley_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Bickley')

@london.route('/data-analyst-trainer-part-time-vacancies-biggin-hill')
def biggin_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Biggin Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-blackfen')
def blackfen_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Blackfen')

@london.route('/data-analyst-trainer-part-time-vacancies-blackfriars')
def blackfriars_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Blackfriars')

@london.route('/data-analyst-trainer-part-time-vacancies-blackheath')
def blackheath_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Blackheath')

@london.route('/data-analyst-trainer-part-time-vacancies-blackheath-royal-standard')
def blackheath_royal_standard_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Blackheath Royal Standard')

@london.route('/data-analyst-trainer-part-time-vacancies-blackwall')
def blackwall_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Blackwall')

@london.route('/data-analyst-trainer-part-time-vacancies-blendon')
def blendon_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Blendon')

@london.route('/data-analyst-trainer-part-time-vacancies-bloomsbury')
def bloomsbury_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Bloomsbury')

@london.route('/data-analyst-trainer-part-time-vacancies-botany-bay')
def botany_bay_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Botany Bay')

@london.route('/data-analyst-trainer-part-time-vacancies-bounds-green')
def bounds_green_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Bounds Green')

@london.route('/data-analyst-trainer-part-time-vacancies-bow')
def bow_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Bow')

@london.route('/data-analyst-trainer-part-time-vacancies-bowes-park')
def bowes_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Bowes Park')

@london.route('/data-analyst-trainer-part-time-vacancies-brentford')
def brentford_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Brentford')

@london.route('/data-analyst-trainer-part-time-vacancies-brent-cross')
def brent_cross_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Brent Cross')

@london.route('/data-analyst-trainer-part-time-vacancies-brent-park')
def brent_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Brent Park')

@london.route('/data-analyst-trainer-part-time-vacancies-brimsdown')
def brimsdown_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Brimsdown')

@london.route('/data-analyst-trainer-part-time-vacancies-brixton')
def brixton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Brixton')

@london.route('/data-analyst-trainer-part-time-vacancies-brockley')
def brockley_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Brockley')

@london.route('/data-analyst-trainer-part-time-vacancies-bromley')
def bromley_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Bromley')

@london.route('/data-analyst-trainer-part-time-vacancies-bromley-by-bow')
def bromley_by_Bow_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Bromley by Bow')

@london.route('/data-analyst-trainer-part-time-vacancies-bromley-common')
def bromley_common_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Bromley Common')

@london.route('/data-analyst-trainer-part-time-vacancies-brompton')
def brompton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Brompton')

@london.route('/data-analyst-trainer-part-time-vacancies-brondesbury')
def brondesbury_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Brondesbury')

@london.route('/data-analyst-trainer-part-time-vacancies-brunswick-park')
def brunswick_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Brunswick Park')

@london.route('/data-analyst-trainer-part-time-vacancies-bulls-cross')
def bulls_cross_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Bulls Cross')

@london.route('/data-analyst-trainer-part-time-vacancies-burnt-oak')
def burnt_oak_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Burnt Oak')

@london.route('/data-analyst-trainer-part-time-vacancies-burroughs')
def burroughs_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Burroughs')

@london.route('/data-analyst-trainer-part-time-vacancies-the-camberwell')
def the_camberwell_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in The Camberwell')

@london.route('/data-analyst-trainer-part-time-vacancies-cambridge-heath')
def cambridge_heath_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Cambridge Heath')

@london.route('/data-analyst-trainer-part-time-vacancies-camden-town')
def camden_town_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Camden Town')

@london.route('/data-analyst-trainer-part-time-vacancies-canary-wharf')
def canary_wharf_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Canary Wharf')

@london.route('/data-analyst-trainer-part-time-vacancies-cann-hall')
def cann_hall_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Cann Hall')

@london.route('/data-analyst-trainer-part-time-vacancies-canning-town')
def canning_town_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Canning Town')

@london.route('/data-analyst-trainer-part-time-vacancies-canonbury')
def canonbury_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Canonbury')

@london.route('/data-analyst-trainer-part-time-vacancies-carshalton')
def carshalton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Carshalton')

@london.route('/data-analyst-trainer-part-time-vacancies-castelnau')
def castelnau_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Castelnau')

@london.route('/data-analyst-trainer-part-time-vacancies-catford')
def catford_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Catford')

@london.route('/data-analyst-trainer-part-time-vacancies-chadwell-heath')
def chadwell_heath_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Chadwell Heath')

@london.route('/data-analyst-trainer-part-time-vacancies-chalk-farm')
def chalk_farm_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Chalk Farm')

@london.route('/data-analyst-trainer-part-time-vacancies-charing-cross')
def charing_cross_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Charing Cross')

@london.route('/data-analyst-trainer-part-time-vacancies-charlton')
def charlton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Charlton')

@london.route('/data-analyst-trainer-part-time-vacancies-chase-cross')
def chase_cross_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Chase Cross')

@london.route('/data-analyst-trainer-part-time-vacancies-cheam')
def cheam_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Cheam')

@london.route('/data-analyst-trainer-part-time-vacancies-chelsea')
def chelsea_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Chelsea')

@london.route('/data-analyst-trainer-part-time-vacancies-chelsfield')
def chelsfield_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Chelsfield')

@london.route('/data-analyst-trainer-part-time-vacancies-chessington')
def chessington_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Chessington')

@london.route('/data-analyst-trainer-part-time-vacancies-childs-hill')
def childs_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Childs Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-chinatown')
def chinatown_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Chinatown')

@london.route('/data-analyst-trainer-part-time-vacancies-chinbrook')
def chinbrook_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Chinbrook')

@london.route('/data-analyst-trainer-part-time-vacancies-chingford')
def chingford_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Chingford')

@london.route('/data-analyst-trainer-part-time-vacancies-chislehurst')
def chislehurst_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Chislehurst')

@london.route('/data-analyst-trainer-part-time-vacancies-chiswick')
def chiswick_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Chiswick')

@london.route('/data-analyst-trainer-part-time-vacancies-church-end')
def church_end_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Church End')

@london.route('/data-analyst-trainer-part-time-vacancies-clapham')
def clapham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Clapham')

@london.route('/data-analyst-trainer-part-time-vacancies-clerkenwell')
def clerkenwell_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Clerkenwell')

@london.route('/data-analyst-trainer-part-time-vacancies-cockfosters')
def cockfosters_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Cockfosters')

@london.route('/data-analyst-trainer-part-time-vacancies-colindale')
def colindale_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Colindale')

@london.route('/data-analyst-trainer-part-time-vacancies-collier-row')
def collier_row_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Collier Row')

@london.route('/data-analyst-trainer-part-time-vacancies-colliers-wood')
def colliers_wood_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Colliers Wood')

@london.route('/data-analyst-trainer-part-time-vacancies-colney-hatch')
def colney_hatch_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Colney Hatch')

@london.route('/data-analyst-trainer-part-time-vacancies-colyers')
def colyers_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Colyers')

@london.route('/data-analyst-trainer-part-time-vacancies-coney-hall')
def coney_hall_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Coney Hall')

@london.route('/data-analyst-trainer-part-time-vacancies-coombe')
def coombe_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Coombe')

@london.route('/data-analyst-trainer-part-time-vacancies-coulsdon')
def coulsdon_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Coulsdon')

@london.route('/data-analyst-trainer-part-time-vacancies-covent-garden')
def covent_garden_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Covent Garden')

@london.route('/data-analyst-trainer-part-time-vacancies-cowley')
def cowley_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Cowley')

@london.route('/data-analyst-trainer-part-time-vacancies-cranford')
def cranford_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Cranford')

@london.route('/data-analyst-trainer-part-time-vacancies-cranham')
def cranham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Cranham')

@london.route('/data-analyst-trainer-part-time-vacancies-crayford')
def crayford_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Crayford')

@london.route('/data-analyst-trainer-part-time-vacancies-creekmouth')
def creekmouth_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Creekmouth')

@london.route('/data-analyst-trainer-part-time-vacancies-crews-hill')
def crews_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Crews Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-cricklewood')
def cricklewood_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Cricklewood')

@london.route('/data-analyst-trainer-part-time-vacancies-crofton-park')
def crofton_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Crofton Park')

@london.route('/data-analyst-trainer-part-time-vacancies-crook-log')
def crook_log_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Crook Log')

@london.route('/data-analyst-trainer-part-time-vacancies-crossness')
def crossness_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Crossness')

@london.route('/data-analyst-trainer-part-time-vacancies-crouch-end')
def crouch_end_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Crouch End')

@london.route('/data-analyst-trainer-part-time-vacancies-croydon')
def croydon_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Croydon')

@london.route('/data-analyst-trainer-part-time-vacancies-crystal-palace')
def crystal_palace_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Crystal Palace')

@london.route('/data-analyst-trainer-part-time-vacancies-cubitt-town')
def cubitt_town_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Cubitt Town')

@london.route('/data-analyst-trainer-part-time-vacancies-cudham')
def cudham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Cudham')

@london.route('/data-analyst-trainer-part-time-vacancies-custom-house')
def custom_house_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Custom House')

@london.route('/data-analyst-trainer-part-time-vacancies-dagenham')
def dagenham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Dagenham')

@london.route('/data-analyst-trainer-part-time-vacancies-dalston')
def dalston_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Dalston')

@london.route('/data-analyst-trainer-part-time-vacancies-dartmouth-park')
def dartmouth_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Dartmouth Park')

@london.route('/data-analyst-trainer-part-time-vacancies-de-beauvoir-town')
def de_beauvoir_town_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in De Beauvoir Town')

@london.route('/data-analyst-trainer-part-time-vacancies-denmark-hill')
def denmark_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Denmark Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-deptford')
def deptford_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Deptford')

@london.route('/data-analyst-trainer-part-time-vacancies-derry-downs')
def derry_downs_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Derry Downs')

@london.route('/data-analyst-trainer-part-time-vacancies-dollis-hill')
def dollis_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Dollis Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-downe')
def downe_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Downe')

@london.route('/data-analyst-trainer-part-time-vacancies-downham')
def downham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Downham')

@london.route('/data-analyst-trainer-part-time-vacancies-dulwich')
def dulwich_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Dulwich')

@london.route('/data-analyst-trainer-part-time-vacancies-ealing')
def ealing_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Ealing')

@london.route('/data-analyst-trainer-part-time-vacancies-earls-court')
def earls_court_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Earls Court')

@london.route('/data-analyst-trainer-part-time-vacancies-earlsfield')
def earlsfield_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Earlsfield')

@london.route('/data-analyst-trainer-part-time-vacancies-east-barnet')
def east_barnet_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in East Barnet')

@london.route('/data-analyst-trainer-part-time-vacancies-east-bedfont')
def east_bedfont_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in East Bedfont')

@london.route('/data-analyst-trainer-part-time-vacancies-east-dulwich')
def east_dulwich_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in East Dulwich')

@london.route('/data-analyst-trainer-part-time-vacancies-east-finchley')
def east_finchley_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in East Finchley')

@london.route('/data-analyst-trainer-part-time-vacancies-east-ham')
def east_ham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in East Ham')

@london.route('/data-analyst-trainer-part-time-vacancies-east-sheen')
def east_sheen_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in East Sheen')

@london.route('/data-analyst-trainer-part-time-vacancies-eastcote')
def eastcote_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Eastcote')

@london.route('/data-analyst-trainer-part-time-vacancies-eden-park')
def eden_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Eden Park')

@london.route('/data-analyst-trainer-part-time-vacancies-edgware')
def edgware_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Edgware')

@london.route('/data-analyst-trainer-part-time-vacancies-edmonton')
def Edmonton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Edmonton')

@london.route('/data-analyst-trainer-part-time-vacancies-eel-pie-island')
def eel_pie_island_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Eel Pie Island')

@london.route('/data-analyst-trainer-part-time-vacancies-elephant-and-castle')
def elephant_and_castle_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Elephant and Castle')

@london.route('/data-analyst-trainer-part-time-vacancies-elm-park')
def elm_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Elm Park')

@london.route('/data-analyst-trainer-part-time-vacancies-elmers-end')
def elmers_end_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Elmers End')

@london.route('/data-analyst-trainer-part-time-vacancies-elmstead')
def elmstead_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Elmstead')

@london.route('/data-analyst-trainer-part-time-vacancies-eltham')
def eltham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Eltham')

@london.route('/data-analyst-trainer-part-time-vacancies-emerson-park')
def emerson_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Emerson Park')

@london.route('/data-analyst-trainer-part-time-vacancies-enfield-highway')
def enfield_highway_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Enfield Highway')

@london.route('/data-analyst-trainer-part-time-vacancies-enfield-town')
def enfield_town_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Enfield Town')

@london.route('/data-analyst-trainer-part-time-vacancies-enfield-wash')
def enfield_wash_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Enfield Wash')

@london.route('/data-analyst-trainer-part-time-vacancies-erith')
def erith_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Erith')

@london.route('/data-analyst-trainer-part-time-vacancies-falconwood')
def falconwood_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Falconwood')

@london.route('/data-analyst-trainer-part-time-vacancies-farringdon')
def farringdon_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Farringdon')

@london.route('/data-analyst-trainer-part-time-vacancies-feltham')
def feltham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Feltham')

@london.route('/data-analyst-trainer-part-time-vacancies-finchley')
def finchley_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Finchley')

@london.route('/data-analyst-trainer-part-time-vacancies-finsbury')
def finsbury_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Finsbury')

@london.route('/data-analyst-trainer-part-time-vacancies-finsbury-park')
def finsbury_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Finsbury Park')

@london.route('/data-analyst-trainer-part-time-vacancies-fitzrovia')
def fitzrovia_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Fitzrovia')

@london.route('/data-analyst-trainer-part-time-vacancies-foots-cray')
def foots_cray_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Foots Cray')

@london.route('/data-analyst-trainer-part-time-vacancies-forest-gate')
def forest_gate_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Forest Gate')

@london.route('/data-analyst-trainer-part-time-vacancies-forest-hill')
def forest_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Forest Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-forestdale')
def forestdale_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Forestdale')

@london.route('/data-analyst-trainer-part-time-vacancies-fortis-green')
def fortis_green_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Fortis Green')

@london.route('/data-analyst-trainer-part-time-vacancies-freezywater')
def freezywater_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Freezywater')

@london.route('/data-analyst-trainer-part-time-vacancies-friern-barnet')
def friern_barnet_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Friern Barnet')

@london.route('/data-analyst-trainer-part-time-vacancies-frognal')
def frognal_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Frognal')

@london.route('/data-analyst-trainer-part-time-vacancies-fulham')
def fulham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Fulham')

@london.route('/data-analyst-trainer-part-time-vacancies-fulwell')
def fulwell_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Fulwell')

@london.route('/data-analyst-trainer-part-time-vacancies-gallows-corner')
def gallows_corner_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Gallows Corner')

@london.route('/data-analyst-trainer-part-time-vacancies-gants-hill')
def gants_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Gants Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-gidea-park')
def gidea_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Gidea Park')

@london.route('/data-analyst-trainer-part-time-vacancies-gipsy-hill')
def gipsy_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Gipsy Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-golders-green')
def golders_green_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Golders Green')

@london.route('/data-analyst-trainer-part-time-vacancies-goodmayes')
def goodmayes_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Goodmayes')

@london.route('/data-analyst-trainer-part-time-vacancies-gospel-oak')
def gospel_oak_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Gospel Oak')

@london.route('/data-analyst-trainer-part-time-vacancies-grahame-park')
def grahame_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Grahame Park')

@london.route('/data-analyst-trainer-part-time-vacancies-grange-park')
def grange_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Grange Park')

@london.route('/data-analyst-trainer-part-time-vacancies-greenford')
def greenford_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Greenford')

@london.route('/data-analyst-trainer-part-time-vacancies-greenwich')
def greenwich_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Greenwich')

@london.route('/data-analyst-trainer-part-time-vacancies-grove-park')
def grove_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Grove Park')

@london.route('/data-analyst-trainer-part-time-vacancies-gunnersbury')
def gunnersbury_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Gunnersbury')

@london.route('/data-analyst-trainer-part-time-vacancies-hackney')
def hackney_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hackney')

@london.route('/data-analyst-trainer-part-time-vacancies-hackney-marshes')
def hackney_marshes_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hackney Marshes')

@london.route('/data-analyst-trainer-part-time-vacancies-hackney-wick')
def hackney_wick_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hackney Wick')

@london.route('/data-analyst-trainer-part-time-vacancies-hadley-wood')
def hadley_wood_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hadley Wood')

@london.route('/data-analyst-trainer-part-time-vacancies-haggerston')
def haggerston_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Haggerston')

@london.route('/data-analyst-trainer-part-time-vacancies-hainault')
def hainault_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hainault')

@london.route('/data-analyst-trainer-part-time-vacancies-the-hale')
def the_hale_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in The Hale')

@london.route('/data-analyst-trainer-part-time-vacancies-ham')
def ham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Ham')

@london.route('/data-analyst-trainer-part-time-vacancies-hammersmith')
def hammersmith_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hammersmith')

@london.route('/data-analyst-trainer-part-time-vacancies-hampstead')
def hampstead_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hampstead')

@london.route('/data-analyst-trainer-part-time-vacancies-hampstead-garden-suburb')
def hampstead_garden_suburb_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hampstead Garden Suburb')

@london.route('/data-analyst-trainer-part-time-vacancies-hampton')
def hampton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hampton')

@london.route('/data-analyst-trainer-part-time-vacancies-hampton-hill')
def hampton_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hampton Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-hampton-wick')
def hampton_wick_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hampton Wick')

@london.route('/data-analyst-trainer-part-time-vacancies-hanwell')
def hanwell_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hanwell')

@london.route('/data-analyst-trainer-part-time-vacancies-hanworth')
def hanworth_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hanworth')

@london.route('/data-analyst-trainer-part-time-vacancies-harefield')
def harefield_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Harefield')

@london.route('/data-analyst-trainer-part-time-vacancies-harlesden')
def harlesden_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Harlesden')

@london.route('/data-analyst-trainer-part-time-vacancies-harlington')
def harlington_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Harlington')

@london.route('/data-analyst-trainer-part-time-vacancies-harmondsworth')
def harmondsworth_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Harmondsworth')

@london.route('/data-analyst-trainer-part-time-vacancies-harold-hill')
def harold_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Harold Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-harold-park')
def harold_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Harold Park')

@london.route('/data-analyst-trainer-part-time-vacancies-harold-wood')
def harold_wood_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Harold Wood')

@london.route('/data-analyst-trainer-part-time-vacancies-harringay')
def harringay_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Harringay')

@london.route('/data-analyst-trainer-part-time-vacancies-harrow')
def harrow_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Harrow')

@london.route('/data-analyst-trainer-part-time-vacancies-harrow-on-the-hill')
def harrow_on_the_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Harrow on the Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-harrow-weald')
def harrow_weald_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Harrow Weald')

@london.route('/data-analyst-trainer-part-time-vacancies-hatch_end')
def hatch_end_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hatch End')

@london.route('/data-analyst-trainer-part-time-vacancies-hatton')
def hatton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hatton')

@london.route('/data-analyst-trainer-part-time-vacancies-havering-atte-bower')
def havering_atte_bower_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Havering-atte-Bower')

@london.route('/data-analyst-trainer-part-time-vacancies-hayes')
def hayes_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hayes')

@london.route('/data-analyst-trainer-part-time-vacancies-hazelwood')
def hazelwood_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hazelwood')

@london.route('/data-analyst-trainer-part-time-vacancies-hendon')
def hendon_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hendon')

@london.route('/data-analyst-trainer-part-time-vacancies-herne-hill')
def herne_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Herne Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-heston')
def heston_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Heston')

@london.route('/data-analyst-trainer-part-time-vacancies-highams-park')
def highams_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Highams Park')

@london.route('/data-analyst-trainer-part-time-vacancies-highbury')
def highbury_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Highbury')

@london.route('/data-analyst-trainer-part-time-vacancies-highgate')
def highgate_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Highgate')

@london.route('/data-analyst-trainer-part-time-vacancies-hillingdon')
def hillingdon_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hillingdon')

@london.route('/data-analyst-trainer-part-time-vacancies-hither-green')
def hither_green_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hither Green')

@london.route('/data-analyst-trainer-part-time-vacancies-holborn')
def holborn_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Holborn')

@london.route('/data-analyst-trainer-part-time-vacancies-holland-park')
def holland_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Holland Park')

@london.route('/data-analyst-trainer-part-time-vacancies-holloway')
def holloway_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Holloway')

@london.route('/data-analyst-trainer-part-time-vacancies-homerton')
def homerton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Homerton')

@london.route('/data-analyst-trainer-part-time-vacancies-honor-oak')
def honor_oak_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Honor Oak')

@london.route('/data-analyst-trainer-part-time-vacancies-hook')
def hook_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hook')

@london.route('/data-analyst-trainer-part-time-vacancies-hornchurch')
def hornchurch_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hornchurch')

@london.route('/data-analyst-trainer-part-time-vacancies-horn-park')
def hornc_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Horn Park')

@london.route('/data-analyst-trainer-part-time-vacancies-hornsey')
def hornsey_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hornsey')

@london.route('/data-analyst-trainer-part-time-vacancies-hounslow')
def hounslow_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hounslow')

@london.route('/data-analyst-trainer-part-time-vacancies-hoxton')
def hoxton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Hoxton')

@london.route('/data-analyst-trainer-part-time-vacancies-the-hyde')
def the_hyde_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in The Hyde')

@london.route('/data-analyst-trainer-part-time-vacancies-ickenham')
def ickenham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Ickenham')

@london.route('/data-analyst-trainer-part-time-vacancies-ilford')
def ilford_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Ilford')

@london.route('/data-analyst-trainer-part-time-vacancies-isle-of-dogs')
def isle_of_dogs_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Isle of Dogs')

@london.route('/data-analyst-trainer-part-time-vacancies-isleworth')
def isleworth_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Isleworth')

@london.route('/data-analyst-trainer-part-time-vacancies-islington')
def islington_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Islington')

@london.route('/data-analyst-trainer-part-time-vacancies-kenley')
def kenley_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Kenley')

@london.route('/data-analyst-trainer-part-time-vacancies-kennington')
def kennington_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Kennington')

@london.route('/data-analyst-trainer-part-time-vacancies-kensal-green')
def kensal_green_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Kensal Green')

@london.route('/data-analyst-trainer-part-time-vacancies-kensington')
def kensington_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Kensington')

@london.route('/data-analyst-trainer-part-time-vacancies-kentish-town')
def kentish_town_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Kentish Town')

@london.route('/data-analyst-trainer-part-time-vacancies-kenton')
def kenton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Kenton')

@london.route('/data-analyst-trainer-part-time-vacancies-keston')
def keston_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Keston')

@london.route('/data-analyst-trainer-part-time-vacancies-kew')
def kew_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Kew')

@london.route('/data-analyst-trainer-part-time-vacancies-kidbrooke')
def kidbrooke_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Kidbrooke')

@london.route('/data-analyst-trainer-part-time-vacancies-kilburn')
def kilburn_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Kilburn')

@london.route('/data-analyst-trainer-part-time-vacancies-kings-cross')
def kings_cross_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Kings Cross')

@london.route('/data-analyst-trainer-part-time-vacancies-kingsbury')
def kingsbury_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Kingsbury')

@london.route('/data-analyst-trainer-part-time-vacancies-kingston-vale')
def kingston_vale_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Kingston Vale')

@london.route('/data-analyst-trainer-part-time-vacancies-kingston-upon-thames')
def kingston_upon_thames_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Kingston upon Thames')

@london.route('/data-analyst-trainer-part-time-vacancies-knightsbridge')
def knightsbridge_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Knightsbridge')

@london.route('/data-analyst-trainer-part-time-vacancies-ladywell')
def ladywell_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Ladywell')

@london.route('/data-analyst-trainer-part-time-vacancies-lambeth')
def lambeth_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Lambeth')

@london.route('/data-analyst-trainer-part-time-vacancies-lamorbey')
def lamorbey_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Lamorbey')

@london.route('/data-analyst-trainer-part-time-vacancies-lampton')
def lampton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Lampton')

@london.route('/data-analyst-trainer-part-time-vacancies-leamouth')
def leamouth_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Leamouth')

@london.route('/data-analyst-trainer-part-time-vacancies-leaves-green')
def leaves_green_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Leaves Green')

@london.route('/data-analyst-trainer-part-time-vacancies-lee')
def lee_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Lee')

@london.route('/data-analyst-trainer-part-time-vacancies-lewisham')
def lewisham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Lewisham')

@london.route('/data-analyst-trainer-part-time-vacancies-leyton')
def leyton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Leyton')

@london.route('/data-analyst-trainer-part-time-vacancies-leytonstone')
def leytonstone_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Leytonstone')

@london.route('/data-analyst-trainer-part-time-vacancies-limehouse')
def limehouse_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Limehouse')

@london.route('/data-analyst-trainer-part-time-vacancies-lisson-grove')
def lisson_grove_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Lisson Grove')

@london.route('/data-analyst-trainer-part-time-vacancies-little-ilford')
def little_ilford_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Little Ilford')

@london.route('/data-analyst-trainer-part-time-vacancies-locksbottom')
def locksbottom_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Locksbottom')

@london.route('/data-analyst-trainer-part-time-vacancies-longford')
def longford_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Longford')

@london.route('/data-analyst-trainer-part-time-vacancies-longlands')
def longlands_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Longlands')

@london.route('/data-analyst-trainer-part-time-vacancies-lower-clapton')
def lower_clapton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Lower Clapton')

@london.route('/data-analyst-trainer-part-time-vacancies-lower-morden')
def lower_morden_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Lower Morden')

@london.route('/data-analyst-trainer-part-time-vacancies-loxford')
def loxford_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Loxford')

@london.route('/data-analyst-trainer-part-time-vacancies-maida-vale')
def maida_vale_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Maida Vale')

@london.route('/data-analyst-trainer-part-time-vacancies-malden-rushett')
def malden_rushett_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Malden Rushett')

@london.route('/data-analyst-trainer-part-time-vacancies-manor-house')
def manor_house_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Manor House')

@london.route('/data-analyst-trainer-part-time-vacancies-manor-park')
def manor_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Manor Park')

@london.route('/data-analyst-trainer-part-time-vacancies-marks-gate')
def marks_gate_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Marks Gate')

@london.route('/data-analyst-trainer-part-time-vacancies-maryland')
def maryland_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Maryland')

@london.route('/data-analyst-trainer-part-time-vacancies-st-marylebone')
def st_marylebone_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in St Marylebone')

@london.route('/data-analyst-trainer-part-time-vacancies-marylebone')
def marylebone_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Marylebone')

@london.route('/data-analyst-trainer-part-time-vacancies-mayfair')
def mayfair_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Mayfair')

@london.route('/data-analyst-trainer-part-time-vacancies-maze-hill')
def maze_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Maze Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-merton-park')
def merton_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Merton Park')

@london.route('/data-analyst-trainer-part-time-vacancies-middle-park')
def middle_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Middle Park')

@london.route('/data-analyst-trainer-part-time-vacancies-mile-end')
def mile_end_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Mile End')

@london.route('/data-analyst-trainer-part-time-vacancies-mill-hill')
def mill_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Mill Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-millbank')
def millbank_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Millbank')

@london.route('/data-analyst-trainer-part-time-vacancies-millwall')
def millwall_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Millwall')

@london.route('/data-analyst-trainer-part-time-vacancies-mitcham')
def mitcham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Mitcham')

@london.route('/data-analyst-trainer-part-time-vacancies-monken-hadley')
def monken_hadley_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Monken Hadley')

@london.route('/data-analyst-trainer-part-time-vacancies-morden')
def morden_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Morden')

@london.route('/data-analyst-trainer-part-time-vacancies-morden-park')
def morden_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Morden Park')

@london.route('/data-analyst-trainer-part-time-vacancies-mortlake')
def mortlake_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Mortlake')

@london.route('/data-analyst-trainer-part-time-vacancies-motspur-park')
def motspur_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Motspur Park')

@london.route('/data-analyst-trainer-part-time-vacancies-mottingham')
def mottingham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Mottingham')

@london.route('/data-analyst-trainer-part-time-vacancies-muswell-hill')
def muswell_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Muswell Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-nags-head')
def nags_head_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Nags Head')

@london.route('/data-analyst-trainer-part-time-vacancies-neasden')
def neasden_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Neasden')

@london.route('/data-analyst-trainer-part-time-vacancies-new-addington')
def new_addington_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in New Addington')

@london.route('/data-analyst-trainer-part-time-vacancies-new-barnet')
def new_barnet_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in New Barnet')

@london.route('/data-analyst-trainer-part-time-vacancies-new-cross')
def new_cross_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in New Cross')

@london.route('/data-analyst-trainer-part-time-vacancies-new-eltham')
def new_eltham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in New Eltham')

@london.route('/data-analyst-trainer-part-time-vacancies-new-malden')
def new_malden_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in New Malden')

@london.route('/data-analyst-trainer-part-time-vacancies-new-southgate')
def new_southgate_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in New Southgate')

@london.route('/data-analyst-trainer-part-time-vacancies-newbury-park')
def newbury_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Newbury Park')

@london.route('/data-analyst-trainer-part-time-vacancies-newington')
def newington_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Newington')

@london.route('/data-analyst-trainer-part-time-vacancies-nine-elms')
def nine_elms_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Nine Elms')

@london.route('/data-analyst-trainer-part-time-vacancies-noak-hill')
def noak_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Noak Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-norbiton')
def norbiton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Norbiton')

@london.route('/data-analyst-trainer-part-time-vacancies-norbury')
def norbury_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Norbury')

@london.route('/data-analyst-trainer-part-time-vacancies-north-end')
def north_end_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in North End')

@london.route('/data-analyst-trainer-part-time-vacancies-north-finchley')
def north_finchley_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in North Finchley')

@london.route('/data-analyst-trainer-part-time-vacancies-north-harrow')
def north_harrow_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in North Harrow')

@london.route('/data-analyst-trainer-part-time-vacancies-north-kensington')
def north_kensington_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in North Kensington')

@london.route('/data-analyst-trainer-part-time-vacancies-north-ockendon')
def north_ockendon_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in North Ockendon')

@london.route('/data-analyst-trainer-part-time-vacancies-north-sheen')
def north_sheen_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in North Sheen')

@london.route('/data-analyst-trainer-part-time-vacancies-north-woolwich')
def north_woolwich_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in North Woolwich')

@london.route('/data-analyst-trainer-part-time-vacancies-northolt')
def northolt_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Northolt')

@london.route('/data-analyst-trainer-part-time-vacancies-northumberland-heath')
def northumberland_heath_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Northumberland Heath')

@london.route('/data-analyst-trainer-part-time-vacancies-northwood')
def northwood_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Northwood')

@london.route('/data-analyst-trainer-part-time-vacancies-norwood-green')
def norwood_green_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Norwood Green')

@london.route('/data-analyst-trainer-part-time-vacancies-notting-hill')
def notting_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Notting Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-nunhead')
def nunhead_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Nunhead')

@london.route('/data-analyst-trainer-part-time-vacancies-oakleigh-park')
def oakleigh_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Oakleigh Park')

@london.route('/data-analyst-trainer-part-time-vacancies-old-coulsdon')
def old_coulsdon_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Old Coulsdon')

@london.route('/data-analyst-trainer-part-time-vacancies-old-ford')
def old_ford_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Old Ford')

@london.route('/data-analyst-trainer-part-time-vacancies-old-malden')
def old_malden_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Old Malden')

@london.route('/data-analyst-trainer-part-time-vacancies-old-oak-common')
def old_oak_common_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Old Oak Common')

@london.route('/data-analyst-trainer-part-time-vacancies-orpington')
def orpington_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Orpington')

@london.route('/data-analyst-trainer-part-time-vacancies-osidge')
def osidge_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Osidge')

@london.route('/data-analyst-trainer-part-time-vacancies-osterley')
def osterley_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Osterley')

@london.route('/data-analyst-trainer-part-time-vacancies-paddington')
def paddington_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Paddington')

@london.route('/data-analyst-trainer-part-time-vacancies-palmers-green')
def palmers_green_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Palmers Green')

@london.route('/data-analyst-trainer-part-time-vacancies-park-royal')
def park_royal_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Park Royal')

@london.route('/data-analyst-trainer-part-time-vacancies-parsons-green')
def parsons_green_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Parsons Green')

@london.route('/data-analyst-trainer-part-time-vacancies-peckham')
def peckham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Peckham')

@london.route('/data-analyst-trainer-part-time-vacancies-penge')
def penge_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Penge')

@london.route('/data-analyst-trainer-part-time-vacancies-pentonville')
def pentonville_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Pentonville')

@london.route('/data-analyst-trainer-part-time-vacancies-perivale')
def perivale_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Perivale')

@london.route('/data-analyst-trainer-part-time-vacancies-petersham')
def petersham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Petersham')

@london.route('/data-analyst-trainer-part-time-vacancies-petts-wood')
def petts_wood_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Petts Wood')

@london.route('/data-analyst-trainer-part-time-vacancies-pimlico')
def pimlico_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Pimlico')

@london.route('/data-analyst-trainer-part-time-vacancies-pinner')
def pinner_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Pinner')

@london.route('/data-analyst-trainer-part-time-vacancies-plaistow')
def plaistow_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Plaistow')

@london.route('/data-analyst-trainer-part-time-vacancies-plumstead')
def plumstead_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Plumstead')

@london.route('/data-analyst-trainer-part-time-vacancies-ponders_end')
def ponders_end_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Ponders End')

@london.route('/data-analyst-trainer-part-time-vacancies-poplar')
def poplar_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Poplar')

@london.route('/data-analyst-trainer-part-time-vacancies-pratts-bottom')
def pratts_bottom_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Pratts Bottom')

@london.route('/data-analyst-trainer-part-time-vacancies-preston')
def preston_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Preston')

@london.route('/data-analyst-trainer-part-time-vacancies-primrose-hill')
def primrose_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Primrose Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-purley')
def purley_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Purley')

@london.route('/data-analyst-trainer-part-time-vacancies-putney')
def putney_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Putney')

@london.route('/data-analyst-trainer-part-time-vacancies-queens-park')
def queens_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Queens Park')

@london.route('/data-analyst-trainer-part-time-vacancies-queensbury')
def queensbury_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Queensbury')

@london.route('/data-analyst-trainer-part-time-vacancies-rainham')
def rainham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Rainham')

@london.route('/data-analyst-trainer-part-time-vacancies-ratcliff')
def ratcliff_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Ratcliff')

@london.route('/data-analyst-trainer-part-time-vacancies-rayners-lane')
def rayners_lane_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Rayners Lane')

@london.route('/data-analyst-trainer-part-time-vacancies-raynes-park')
def raynes_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Raynes Park')

@london.route('/data-analyst-trainer-part-time-vacancies-redbridge')
def redbridge_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Redbridge')

@london.route('/data-analyst-trainer-part-time-vacancies-richmond')
def richmond_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Richmond')

@london.route('/data-analyst-trainer-part-time-vacancies-riddlesdown')
def riddlesdown_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Riddlesdown')

@london.route('/data-analyst-trainer-part-time-vacancies-roehampton')
def roehampton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Roehampton')

@london.route('/data-analyst-trainer-part-time-vacancies-romford')
def romford_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Romford')

@london.route('/data-analyst-trainer-part-time-vacancies-rotherhithe')
def rotherhithe_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Rotherhithe')

@london.route('/data-analyst-trainer-part-time-vacancies-ruislip')
def ruislip_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Ruislip')

@london.route('/data-analyst-trainer-part-time-vacancies-rush-green')
def rush_green_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Rush Green')

@london.route('/data-analyst-trainer-part-time-vacancies-ruxley')
def ruxley_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Ruxley')

@london.route('/data-analyst-trainer-part-time-vacancies-sanderstead')
def sanderstead_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Sanderstead')

@london.route('/data-analyst-trainer-part-time-vacancies-sands-end')
def sands_end_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Sands End')

@london.route('/data-analyst-trainer-part-time-vacancies-selhurst')
def selhurst_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Selhurst')

@london.route('/data-analyst-trainer-part-time-vacancies-selsdon')
def selsdon_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Selsdon')

@london.route('/data-analyst-trainer-part-time-vacancies-seven-kings')
def seven_kings_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Seven Kings')

@london.route('/data-analyst-trainer-part-time-vacancies-seven-sisters')
def seven_sisters_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Seven Sisters')

@london.route('/data-analyst-trainer-part-time-vacancies-shacklewell')
def shacklewell_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Shacklewell')

@london.route('/data-analyst-trainer-part-time-vacancies-shadwell')
def shadwell_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Shadwell')

@london.route('/data-analyst-trainer-part-time-vacancies-shepherds-bush')
def shepherds_bush_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Shepherds Bush')

@london.route('/data-analyst-trainer-part-time-vacancies-shirley')
def shirley_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Shirley')

@london.route('/data-analyst-trainer-part-time-vacancies-shooters-hill')
def shooters_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Shooters Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-shoreditch')
def shoreditch_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Shoreditch')

@london.route('/data-analyst-trainer-part-time-vacancies-sidcup')
def sidcup_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Sidcup')

@london.route('/data-analyst-trainer-part-time-vacancies-silvertown')
def silvertown_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Silvertown')

@london.route('/data-analyst-trainer-part-time-vacancies-sipson')
def sipson_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Sipson')

@london.route('/data-analyst-trainer-part-time-vacancies-slade-green')
def slade_green_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Slade Green')

@london.route('/data-analyst-trainer-part-time-vacancies-snaresbrook')
def snaresbrook_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Snaresbrook')

@london.route('/data-analyst-trainer-part-time-vacancies-soho')
def soho_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Soho')

@london.route('/data-analyst-trainer-part-time-vacancies-somerstown')
def somerstown_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Somerstown')

@london.route('/data-analyst-trainer-part-time-vacancies-south-croydon')
def south_croydon_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in South Croydon')

@london.route('/data-analyst-trainer-part-time-vacancies-south-hackney')
def south_hackney_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in South Hackney')

@london.route('/data-analyst-trainer-part-time-vacancies-south-harrow')
def south_harrow_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in South Harrow')

@london.route('/data-analyst-trainer-part-time-vacancies-south-hornchurch')
def south_hornchurch_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in South Hornchurch')

@london.route('/data-analyst-trainer-part-time-vacancies-south-kensington')
def south_kensington_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in South Kensington')

@london.route('/data-analyst-trainer-part-time-vacancies-south-norwood')
def south_norwood_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in South Norwood')

@london.route('/data-analyst-trainer-part-time-vacancies-south-ruislip')
def south_ruislip_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in South Ruislip')

@london.route('/data-analyst-trainer-part-time-vacancies-south-wimbledon')
def south_wimbledon_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in South Wimbledon')

@london.route('/data-analyst-trainer-part-time-vacancies-south-woodford')
def south_woodford_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in South Woodford')

@london.route('/data-analyst-trainer-part-time-vacancies-south-tottenham')
def south_tottenham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in South Tottenham')

@london.route('/data-analyst-trainer-part-time-vacancies-southend')
def southend_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Southend')

@london.route('/data-analyst-trainer-part-time-vacancies-southall')
def southall_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Southall')

@london.route('/data-analyst-trainer-part-time-vacancies-southborough')
def southborough_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Southborough')

@london.route('/data-analyst-trainer-part-time-vacancies-southfields')
def southfields_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Southfields')

@london.route('/data-analyst-trainer-part-time-vacancies-southgate')
def southgate_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Southgate')

@london.route('/data-analyst-trainer-part-time-vacancies-spitalfields')
def spitalfields_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Spitalfields')

@london.route('/data-analyst-trainer-part-time-vacancies-st-helier')
def st_helier_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in St Helier')

@london.route('/data-analyst-trainer-part-time-vacancies-st-james')
def st_james_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in St James')

@london.route('/data-analyst-trainer-part-time-vacancies-st-margarets')
def st_margarets_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in St Margarets')

@london.route('/data-analyst-trainer-part-time-vacancies-st-giles')
def st_giles_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in St Giles')

@london.route('/data-analyst-trainer-part-time-vacancies-st-johns')
def st_johns_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in St Johns')

@london.route('/data-analyst-trainer-part-time-vacancies-st-johns-wood')
def st_johns_wood_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in St Johns Wood')

@london.route('/data-analyst-trainer-part-time-vacancies-st-lukes')
def st_lukes_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in St Lukes')

@london.route('/data-analyst-trainer-part-time-vacancies-st-mary-cray')
def st_mary_cray_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in St Mary Cray')

@london.route('/data-analyst-trainer-part-time-vacancies-st-pancras')
def st_pancras_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in St Pancras')

@london.route('/data-analyst-trainer-part-time-vacancies-st-pauls-cray')
def st_pauls_cray_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in St Pauls Cray')

@london.route('/data-analyst-trainer-part-time-vacancies-stamford-hill')
def stamford_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Stamford Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-stanmore')
def stanmore_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Stanmore')

@london.route('/data-analyst-trainer-part-time-vacancies-stepney')
def stepney_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Stepney')

@london.route('/data-analyst-trainer-part-time-vacancies-stockwell')
def stockwell_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Stockwell')

@london.route('/data-analyst-trainer-part-time-vacancies-stoke-newington')
def stoke_newington_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Stoke Newington')

@london.route('/data-analyst-trainer-part-time-vacancies-stratford')
def stratford_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Stratford')

@london.route('/data-analyst-trainer-part-time-vacancies-strawberry-hill')
def strawberry_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Strawberry Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-streatham')
def streatham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Streatham')

@london.route('/data-analyst-trainer-part-time-vacancies-stroud-green')
def stroud_green_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Stroud Green')

@london.route('/data-analyst-trainer-part-time-vacancies-sudbury')
def sudbury_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Sudbury')

@london.route('/data-analyst-trainer-part-time-vacancies-sundridge')
def sundridge_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Sundridge')

@london.route('/data-analyst-trainer-part-time-vacancies-surbiton')
def surbiton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Surbiton')

@london.route('/data-analyst-trainer-part-time-vacancies-surrey-quays')
def surrey_quays_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Surrey Quays')

@london.route('/data-analyst-trainer-part-time-vacancies-sutton')
def sutton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Sutton')

@london.route('/data-analyst-trainer-part-time-vacancies-swiss-cottage')
def swiss_cottage_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Swiss Cottage')

@london.route('/data-analyst-trainer-part-time-vacancies-sydenham')
def sydenham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Sydenham')

@london.route('/data-analyst-trainer-part-time-vacancies-lower-sydenham')
def lower_sydenham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Lower Sydenham')

@london.route('/data-analyst-trainer-part-time-vacancies-upper-sydenham')
def upper_sydenham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Upper Sydenham')

@london.route('/data-analyst-trainer-part-time-vacancies-sydenham-hill')
def sydenham_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Sydenham Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-teddington')
def teddington_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Teddington')

@london.route('/data-analyst-trainer-part-time-vacancies-temple')
def temple_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Temple')

@london.route('/data-analyst-trainer-part-time-vacancies-temple-fortune')
def temple_fortune_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Temple Fortune')

@london.route('/data-analyst-trainer-part-time-vacancies-thamesmead')
def thamesmead_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Thamesmead')

@london.route('/data-analyst-trainer-part-time-vacancies-thornton-heath')
def thornton_heath_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Thornton Heath')

@london.route('/data-analyst-trainer-part-time-vacancies-tokyngton')
def tokyngton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Tokyngton')

@london.route('/data-analyst-trainer-part-time-vacancies-tolworth')
def tolworth_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Tolworth')

@london.route('/data-analyst-trainer-part-time-vacancies-tooting')
def tooting_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Tooting')

@london.route('/data-analyst-trainer-part-time-vacancies-tooting-bec')
def tooting_bec_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Tooting Bec')

@london.route('/data-analyst-trainer-part-time-vacancies-tottenham')
def tottenham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Tottenham')

@london.route('/data-analyst-trainer-part-time-vacancies-tottenham-green')
def tottenham_green_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Tottenham Green')

@london.route('/data-analyst-trainer-part-time-vacancies-tottenham_hale')
def tottenham_hale_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Tottenham Hale')

@london.route('/data-analyst-trainer-part-time-vacancies-totteridge')
def totteridge_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Totteridge')

@london.route('/data-analyst-trainer-part-time-vacancies-tower-hill')
def tower_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Tower Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-tufnell-park')
def tufnell_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Tufnell Park')

@london.route('/data-analyst-trainer-part-time-vacancies-tulse-hill')
def tulse_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Tulse Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-turnpike-lane')
def turnpike_lane_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Turnpike Lane')

@london.route('/data-analyst-trainer-part-time-vacancies-twickenham')
def twickenham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Twickenham')

@london.route('/data-analyst-trainer-part-time-vacancies-upminster')
def upminster_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Upminster')

@london.route('/data-analyst-trainer-part-time-vacancies-upminster-bridge')
def upminster_bridge_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Upminster Bridge')

@london.route('/data-analyst-trainer-part-time-vacancies-upper-clapton')
def upper_clapton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Upper Clapton')

@london.route('/data-analyst-trainer-part-time-vacancies-upper-holloway')
def upper_holloway_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Upper Holloway')

@london.route('/data-analyst-trainer-part-time-vacancies-upper-norwood')
def upper_norwood_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Upper Norwood')

@london.route('/data-analyst-trainer-part-time-vacancies-upper-ruxley')
def upper_ruxley_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Upper Ruxley')

@london.route('/data-analyst-trainer-part-time-vacancies-upper-walthamstow')
def upper_walthamstow_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Upper Walthamstow')

@london.route('/data-analyst-trainer-part-time-vacancies-upton')
def upton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Upton')

@london.route('/data-analyst-trainer-part-time-vacancies-upton-park')
def upton_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Upton Park')

@london.route('/data-analyst-trainer-part-time-vacancies-uxbridge')
def uxbridge_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Uxbridge')

@london.route('/data-analyst-trainer-part-time-vacancies-uauxhall')
def uauxhall_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Vauxhall')

@london.route('/data-analyst-trainer-part-time-vacancies-waddon')
def waddon_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Waddon')

@london.route('/data-analyst-trainer-part-time-vacancies-wallington')
def wallington_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Wallington')

@london.route('/data-analyst-trainer-part-time-vacancies-walthamstow')
def walthamstow_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Walthamstow')

@london.route('/data-analyst-trainer-part-time-vacancies-walthamstow-village')
def walthamstow_village_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Walthamstow Village')

@london.route('/data-analyst-trainer-part-time-vacancies-walworth')
def walworth_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Walworth')

@london.route('/data-analyst-trainer-part-time-vacancies-wandsworth')
def wandsworth_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Wandsworth')

@london.route('/data-analyst-trainer-part-time-vacancies-wanstead')
def wanstead_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Wanstead')

@london.route('/data-analyst-trainer-part-time-vacancies-wapping')
def wapping_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Wapping')

@london.route('/data-analyst-trainer-part-time-vacancies-wealdstone')
def wealdstone_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Wealdstone')

@london.route('/data-analyst-trainer-part-time-vacancies-well-hall')
def well_hall_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Well Hall')

@london.route('/data-analyst-trainer-part-time-vacancies-welling')
def welling_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Welling')

@london.route('/data-analyst-trainer-part-time-vacancies-wembley')
def wembley_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Wembley')

@london.route('/data-analyst-trainer-part-time-vacancies-wembley-park')
def wembley_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Wembley Park')

@london.route('/data-analyst-trainer-part-time-vacancies-wennington')
def wennington_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Wennington')

@london.route('/data-analyst-trainer-part-time-vacancies-west-brompton')
def west_brompton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in West Brompton')

@london.route('/data-analyst-trainer-part-time-vacancies-west-drayton')
def west_drayton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in West Drayton')

@london.route('/data-analyst-trainer-part-time-vacancies-west-ealing')
def west_ealing_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in West Ealing')

@london.route('/data-analyst-trainer-part-time-vacancies-west-green')
def west_green_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in West Green')

@london.route('/data-analyst-trainer-part-time-vacancies-west-ham')
def west_ham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in West Ham')

@london.route('/data-analyst-trainer-part-time-vacancies-west-hampstead')
def west_hampstead_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in West Hampstead')

@london.route('/data-analyst-trainer-part-time-vacancies-west-harrow')
def west_harrow_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in West Harrow')

@london.route('/data-analyst-trainer-part-time-vacancies-west-heath')
def west_heath_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in West Heath')

@london.route('/data-analyst-trainer-part-time-vacancies-west-hendon')
def west_hendon_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in West Hendon')

@london.route('/data-analyst-trainer-part-time-vacancies-west-kensington')
def west_kensington_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in West Kensington')

@london.route('/data-analyst-trainer-part-time-vacancies-west-norwood')
def west_norwood_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in West Norwood')

@london.route('/data-analyst-trainer-part-time-vacancies-west-wickham')
def west_wickham_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in West Wickham')

@london.route('/data-analyst-trainer-part-time-vacancies-westcombe-park')
def westcombe_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Westcombe Park')

@london.route('/data-analyst-trainer-part-time-vacancies-westminster')
def westminster_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Westminster')

@london.route('/data-analyst-trainer-part-time-vacancies-whetstone')
def whetstone_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Whetstone')

@london.route('/data-analyst-trainer-part-time-vacancies-white-city')
def white_city_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in White City')

@london.route('/data-analyst-trainer-part-time-vacancies-whitechapel')
def whitechapel_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Whitechapel')

@london.route('/data-analyst-trainer-part-time-vacancies-widmore')
def widmore_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Widmore')

@london.route('/data-analyst-trainer-part-time-vacancies-widmore-green')
def widmore_green_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Widmore Green')

@london.route('/data-analyst-trainer-part-time-vacancies-whitton')
def whitton_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Whitton')

@london.route('/data-analyst-trainer-part-time-vacancies-willesden')
def willesden_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Willesden')

@london.route('/data-analyst-trainer-part-time-vacancies-wimbledon')
def wimbledon_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Wimbledon')

@london.route('/data-analyst-trainer-part-time-vacancies-winchmore-hill')
def winchmore_hill_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Winchmore Hill')

@london.route('/data-analyst-trainer-part-time-vacancies-wood-green')
def wood_green_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Wood Green')

@london.route('/data-analyst-trainer-part-time-vacancies-woodford')
def woodford_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Woodford')

@london.route('/data-analyst-trainer-part-time-vacancies-woodford-green')
def woodford_green_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Woodford Green')

@london.route('/data-analyst-trainer-part-time-vacancies-woodlands')
def woodlands_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Woodlands')

@london.route('/data-analyst-trainer-part-time-vacancies-woodside')
def woodside_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Woodside')

@london.route('/data-analyst-trainer-part-time-vacancies-woodside-park')
def woodside_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Woodside Park')

@london.route('/data-analyst-trainer-part-time-vacancies-woolwich')
def woolwich_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Woolwich')

@london.route('/data-analyst-trainer-part-time-vacancies-worcester-park')
def worcester_park_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Worcester Park')

@london.route('/data-analyst-trainer-part-time-vacancies-wormwood-scrubs')
def wormwood_scrubs_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Wormwood Scrubs')

@london.route('/data-analyst-trainer-part-time-vacancies-yeading')
def yeading_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Yeading')

@london.route('/data-analyst-trainer-part-time-vacancies-yiewsley')
def yiewsley_data_analyst_trainer_vacancies():
    return render_template('landing_teachers_cities.html', data='data analyst in Yiewsley')




