"""Application configuration.

When using app.config.from_object(obj), Flask will look for all UPPERCASE
attributes on that object and load their values into the app config. Python
modules are objects, so you can use a .py file as your configuration.
"""

import os

# Get the current working directory to place sched.db during development.
# In production, use absolute paths or a database management system.

class BaseConfig(object):
    PWD = os.path.abspath(os.curdir)
    DEBUG = True
    #SQLALCHEMY_DATABASE_URI = 'sqlite:///{}/teachera.db'.format(PWD)
    #SQLALCHEMY_DATABASE_URI = 'sqlite:////home/ziliot/webapps/appname3/sched.db'
    SQLALCHEMY_DATABASE_URI = "postgresql+psycopg2://teachera:jonsnow&harveySpect&merlin&Regina@46.101.195.232:5432/appdb";

class DefaultConfig(BaseConfig):
    SECRET_KEY = 'ndihinbosejsecretkey=-097' # Create your own.
    SESSION_PROTECTION = 'strong'
    SECURITY_PASSWORD_HASH = 'pbkdf2_sha512'
    SECURITY_PASSWORD_SALT = 'ndihinbosejsecretkey=-098'
    SECURITY_LOGIN_URL = '/login'
    SECURITY_LOGOUT_URL = '/logout'
    SECURITY_REGISTER_URL = '/signup'
    SECURITY_RESET_URL = '/reset'
    SECURITY_CONFIRMABLE = False
    SECURITY_REGISTERABLE = True
    SECURITY_SEND_REGISTER_EMAIL = False
    SECURITY_SEND_PASSWORD_CHANGE_EMAIL = True
    #SECURITY_SEND_PASSWORD_RESET_NOTICE_EMAIL = False
    FACEBOOK_LOGIN_APP_ID = '278199585695188'  #Internly's facebook details
    FACEBOOK_LOGIN_APP_SECRET = '0d79c6473c08ce32dbfcac418014e1cc'#Internly's facebook details
    #FACEBOOK_LOGIN_APP_ID = '525684897599050'
    #FACEBOOK_LOGIN_APP_SECRET = '9bf282c5a76bca90b5777094ae35ec2b'
    MAIL_SERVER = 'smtp.mailgun.org'
    MAIL_PORT = 587
    MAIL_USE_SSL = False
    MAIL_USERNAME = 'postmaster@teachera.co.uk'
    MAIL_PASSWORD = '76ae86f0e75841d38a7f26dcb066f1a5'
    SECURITY_RECOVERABLE = True
    SECURITY_SEND_PASSWORD_RESET_NOTICE_EMAIL = True
    SECURITY_RESET_SALT= 'enydM2AJAdcoKwdVaMJWvEsbPLKuQpMjf'
    DEFAULT_MAIL_SENDER = 'postmaster@teachera.co.uk'
    SECURITY_EMAIL_SENDER = 'postmaster@teachera.co.uk'
    MAIL_DEBUG = True
    POSITION_APPERANCE_TIME_IN_DAYS=7
    # Stripe keys
    STRIPE_SECRET_KEY = "sk_live_nGTwZia9sSE8rD75SmB4YMtH"
    STRIPE_PUBLISHABLE_KEY = "pk_live_2Pn32kFhUzRn5hEkXUxYqJV5"

    # Added by James 
    # **********************************************************
    PAYSTACK_SECRET_KEY      =   'sk_live_f29a2a25dd874d44b8b58442298a7409dfefd51c' # Update this with live secret key
    PAYSTACK_PUBLIC_KEY      =   'pk_live_5e742e17841fc6e95bd64c9173bda9152c587501'
    

    



